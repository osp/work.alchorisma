<!DOCTYPE html>
<html lang="en">

  <head>
      <title>Alchorisma — Tu(r)ning into trees</title>
    <meta charset="utf-8" />
    <meta name="generator" content="Pelican" />
    <link rel="stylesheet" href="theme/css/main.css">
    <link rel="stylesheet" href="theme/css/sigils.css">




  </head>

  <body>

    <section id="nav-interface">
          <a id="index-link" href="map.html">&#128610;</a>


  <a href="/home.html">[home or precedent path?]</a>

  <div class="article-cart essay">
    <details>
      <summary>
        <h2>Tu(r)ning into trees</h2>
      </summary>

      <main>
        <p>This text untangles some of the lines that have run through the Alchorisma research. It elaborates on the basis of the knowledge we gathered from meetings with trees that took place during excursions and field trips, and uses these meetings as a pathway to revisit some of Alchorisma’s leading references, concepts and methodologies. The text uses these meetings with trees to unravel practices of attunement, attention, embodied immersion and care for other beings, opening up new ways of engaging with trees, landscapes and other beings from a multispecies perspective.</p>
      </main>

    </details>
    <footer>
      <address class="author">By
        Ils Huygens
      </address>
      <div class="category">
        essay
      </div>
    </footer>

  </div>

    </section>

    <main>
<section class="article essay" class="body">
    <h1>Tu(r)ning into trees:</h1>
<h2>The multiplicity of becoming -root, trunk, branch, leaf</h2>
<p><em>Kinship</em></p>
<p><em>Very slowly burning, the big forest tree</em></p>
<p><em>stands in the slight hollow of the snow</em></p>
<p><em>melted around it by the mild, long</em></p>
<p><em>heat of its being and its will to be</em></p>
<p><em>root, trunk, branch, leaf, and know</em></p>
<p><em>earth dark, sun light, wind touch, bird song.</em></p>
<p><em>Rootless and restless and warmblooded, we</em></p>
<p><em>blaze in the flare that blinds us to that slow,</em></p>
<p><em>tall, fraternal fire of life as strong</em></p>
<p><em>now as in the seedling two centuries ago.</em></p>
<p>Ursula Le Guin<sup id="fnref:1"><a class="footnote-ref" href="#fn:1">1</a></sup></p>
<p>Alchorisma covers multiple threads of living in a multispecies world.
This text untangles some of the lines that have run through this
multilayered project. It touches on some of the ideas that were
developed during the two work sessions, leading up to the collection of
prototypes, texts, dream and fictions presented throughout this online
publication. The lines gathered here try to cover some of the threads
that guided us in the alchemic meetings that occurred between the worlds
of stones, trees, ghosts and algorithms wielding between artistic
practice, <a class="sigils interview" href="#" id="sigil-1">animist knowledge</a> and database technology.</p>
<p class="newpath interview" id="newpath-1">Moreover, NNs do not
possess any <span>prior knowledge</span> of the data being processed, they generate
needed characteristics from the learning material.</p>

<p>The text elaborates from the knowledge we gathered from meetings with
trees that took place during excursions and fieldtrips, and uses these
meetings as a pathway to revisit some of Alchorisma's leading
references, concepts and methodologies. The text uses these meetings
with trees to <a class="sigils dream" href="#" id="sigil-2">unravel practices</a> of attunement, attention, embodied
immersion and care for other beings. They offer possibilities to use
artistic practice and algorithmic technology as a way to open up new
ways of engaging with trees, landscapes and other beings from a
multispecies perspective.</p>
<p class="newpath dream" id="newpath-2">Zuboff calls us to push aside
the algorithms that discipline and colonise our bodies, our futures,
the land we are living on. Zuboff persuades us to join the struggle to
halt <span>disruptive practices</span> that foreclose the future we want. What
happens when they come for my truth my body, my desires, my
indigenous land uninvited and determined to march through my self,
taking the bits and pieces that can nourish their machines to reach
their objectives?</p>

<h2>First meeting: An unadjusted Douglas fir</h2>
<p>Location: Bokrijk, Genk, Castle Garden</p>
<p><img alt="" src="/essay/Tu(r)ning_into_Trees/DouglasFir.jpg"></p>
<p><em>One of the most remarkable trees of the old English Garden around the
Bokrijk castle, located between Hasselt and Genk is an old Douglas fir
with four stems. The English garden was created in the late 1800s, early
1900s together with the castle. Typical for the English park style was
to create small groups of large imposant --often exotic- trees as a way
to show status. In Bokrijk we see small batches of Douglas fir, as well
as a majestic alignment of these firs that mark the wide alleys of the
park. The largest Douglas fir in Belgium is found here, although it's
not even half of the 100ms it can reach in its native home (mainly
America's west coast and British Columbia). The name of the species is
derived from Douglas, the Englishman that first introduced the species
in Europe in 1827. Other exotic trees we encounter in the park are the
Japanese larch, Atlas cedar, or the Pacific red cedar, a majestical tree
that can live up to thousand years (hence it's nickname giant arborvitae
or tree of life). The park also holds the largest collection in the
region of the monumental mammoth tree, of which two large specimens mark
the entrance to the castle.</em></p>
<p>This style of garden is in stark contrast to plantations of trees, where
straight stems are favoured to enhance productivity. When a tree grows
in multiple stems it decreases stability and complicates the flows of
energy and water. In plantation <a class="sigils score" href="#" id="sigil-3">forests meant for wood production</a> trees
need to grow high and fast. 'Unadjusted trees', like this Douglas fir
with its multiple stems would not be retained. However for the joy of
human delight, in the English style abnormal growth patterns and tree
deformations were accepted and even encouraged, turning the trees into
natural 'follies', that can inspire the visitor with feelings of awe and
wonder. Nature as curiosity.</p>
<p class="newpath score" id="newpath-3">The manager of <span>the park and surrounding forests of Bokrijk</span> introduced
us to a huge database of all the trees in the park. In the database
trees are coded as species, numbers, coordinates, they are labelled as
alive, in good, fair or poor condition, removed, unable to locate or
dead, in multiple sublevels where clarity and ambiguity conflate the
real tree and the data of (about?) the tree.</p>

<p>The multistemmed Douglas fir is part of the vast idyllic park
surrounding the castle, fully constructed and manmade. Its origins are
based on heroic travels of the higher classes. For the sake of reliving
the feverish dream of explorers of the wild and exotic, botanical
curiosa were brought back as colonial souvenirs to a private park. The
paradise requires continuous maintenance and meticulous management. This
is why only few of these remarkable exotic trees that were brought from
faraway travels still remain in public parks today and most of those
that remain are found in private gardens. Apart from some exceptions
most of these exotic species require an extremely devoted gardener to
keep them healthy and alive in the alien and often inhospitable grounds
into which they were relocated.</p>
<h2>Second meeting: a dead tree stump</h2>
<p>Location: Bokrijk surrounding forest</p>
<p><img alt="" src="/essay/Tu(r)ning_into_Trees/DeadTreeStump.jpg"></p>
<p><em>"Death does not end the networked nature of trees. As they rot away,
dead logs, branches, and roots become focal points for thousands of
relationships."</em><sup id="fnref:2"><a class="footnote-ref" href="#fn:2">2</a></sup></p>
<p>Dead tree stumps are covered in mosses, lichens, different types of
fungi, and are vibrant with life that in different moments of time
cohabitate with the dead wood, enhancing or thriving on the complex
processes of organic decay. Small and large animals like rodents and
birds use the trees for shelter, whereas the hollows and crevices of
dead trees can serve as drinking pools. In the same water that is
contained in its hollow and the mosses that grow on them, all kinds of
insects and beetles lay their eggs to hatch. Once hatched, the larvae
feed on the dead wood digesting it into fresh and nutrient hummus that
on its turn enables growth and gives life to the surrounding trees and
plants.</p>
<p><em>"Our language does a poor job of recognizing this afterlife of trees.
Rot, decomposition, punk, duff, deadwood: these are slack words for so
vital a process. Rot is detonation of possibility. Decomposition is
renewed composition by living communities. Duff and punk are smelters
for new life. Deadwood is effervescent creativity, regenerating as its
"self" degenerates into the network."</em> <sup id="fnref:3"><a class="footnote-ref" href="#fn:3">3</a></sup></p>
<p>Recent studies point to something even more astonishing: in a forest
environment a dead tree stump can also literally overcome its own death.
Scientists have discovered that tree stumps can be kept alive by
surrounding trees. While the tree stump itself is unable to detract
water from the air through its leaves or roots, it can be provided by a
minimum of water by the trees around it. This suggests that trees have
interconnected root systems, which enables them to exchange nutrients
with each other. A tree, even a dead one is never a single tree but part
of a larger forest organism, interconnecting with other trees as well as
with many other beings.</p>
<p>More and more recent findings in biology are <a class="sigils code" href="#" id="sigil-4">suggesting a shift</a> from the
perception of trees as single individual entities towards the forest as
an entangled ecosystem or a superorganism. In his international
bestseller <em>The Hidden Life of Trees</em> Peter Wohlleben describes how
trees communicate with each other in multiple ways. When infected by
insects, trees send out pheromone signals to the surrounding trees as
warnings, or as a means to attract other insect species that can help
them setting up a counterattack. This suggests that trees are sentient
beings but also that the forest acts as a social system where the older
stronger trees protect the weaker or younger ones.</p>
<p class="newpath code" id="newpath-4">The more controlling machinery is advanced, the more it
turns from tracking to prediction, while the inner principles of its
functioning are overshadowed by a visible complexity. We observe <span>a
temporal shift</span>: contemporary control is defined by pre-processing, that
is by prediction and prevention, while processing becomes fictional.</p>

<p>Even more stunning is what happens below the surface. As the example of
the dead tree stump suggests, roots of different trees are connected
with each other, allowing the exchange of water and minerals, and even
communication. As Peter Wohlleben explains so well in his book, this
communication mostly happens with the aid of other species, most
importantly types of mycorrhizae. These are types of fungi that live
symbiotically between and within plant roots. Hidden under the ground a
rhizomatic world interconnects the individual trees' roots with a vast
network of mycelium, which is the name of the underground threads of
fungal organisms. A single mycelium network can grow over kilometres
wide, interconnecting individual plants and trees together into what is
recently been termed as the 'woodwide web'.</p>
<p>The interrelation between the tree and the mycelium is a form of
symbiosis based on mutual benefit, while the fungal threads allow the
trees to exchange information and nutrients with each other, the
mycelium is 'paid' with the sugar it feeds itself on. Slowly but
steadily these new insights are turning our view on trees upside down,
suggesting that symbiosis, far from being a biological rarity is all
around us. It is a far stretch from the tree as a beautiful yet single
entity, as it was placed in the English garden park. From nature as
curiosity, to a new curiosity for nature, we may need to start
acknowledging that it is precisely the concept of 'nature' itself, which
we need to do away with.</p>
<h2>Third meeting: database ghost trees</h2>
<p>Location: Bokrijk Arboretum database</p>
<p><img alt="" src="/essay/Tu(r)ning_into_Trees/LabelledTree.jpg"></p>
<p><em>The manager of the park and surrounding forests of Bokrijk introduced
us to a huge database of all the trees in the park. In the database
trees are coded as species, numbers, coordinates, they are labelled as
alive, in good, fair or poor condition, removed, unable to locate or
dead, in multiple sublevels where clarity and ambiguity conflate the
real tree and the data of (about?) the tree. Inputting and updating the
information into the database is an exhaustive and time-consuming task
that requires continuous action. Ghosts of dead trees linger. With the
meeting of the dead tree stump in mind we wonder, when is a tree truly
gone?</em></p>
<p>The story turns our attention on how numbers, graphs, calculations,
algorithms are used to describe and map the natural world today. The
concept of nature today is fundamentally intertwined with scientific
observations and research encoded into <a class="sigils visuals" href="#" id="sigil-5">database technology</a>. And just as
much as scientific objectivity turned out to be an unattainable goal,
technology is just as much conflated by those who create the database,
those who manage it, and those who update it.</p>
<p class="newpath visuals" id="newpath-5">The lines gathered here try to cover some of the threads
that guided us in the alchemic meetings that occurred between the worlds
of stones, trees, ghosts and algorithms wielding between artistic
practice, animist knowledge and <span>database technology</span>.</p>

<p>It is precisely thanks to the massive expansion of database technologies
and advances in computer modelling that the concept of the anthropocene
made its way into the current debate on climate change.<sup id="fnref:4"><a class="footnote-ref" href="#fn:4">4</a></sup>
Paradoxically, it is the elevated capacity of processing extensive
amounts of data, which led to (showed?) the acknowledgment of the
desastruous impact of the anthropos on nature. We needed (need?) the
concept of the anthropocene in order to understand how urgent it is to
decenter the human, and move ahead with the 'decolonisation of nature'.
According to cultural theorists like Timothy Morton, Bruno Latour or T.
J. Demos, it is the modernist and dualistic view of 'nature' we need to
get rid of. In re-thinking the anthropocene further into concepts like
the plantationocene, or the capitalocene, cultural theorists argue that
the human-nature divide is not only what caused our problems in the
first place, it is also what is keeping us from acting and dealing with
the current crisis. Their argument is that we need to do away with
'nature' as a concept since it keeps us adhering to a worldview where
humans are placed 'outside of' and subsequently also 'above' nature.</p>
<p>More and more both human and natural sciences are starting to view the
world as an all-encompassing system, where stones, organisms, animals,
plants, bacteria, viruses and humans are part of an interconnected
network. In the network one can never escape or ignore any of the other
presences. If one species overrules the system, the effects of this
imbalance will haunt the earth, and leak into the future like
radioactive waste. The current corona crisis, caused by a microscopic,
tiny entity, is just one more example of these rippling effects. If we
want to survive as a species, and deflect the anthropos' faith of
turning into the ghosts of the anthropocene, we need to fundamentally
change the way we see the world.</p>
<p>With the aid of thinkers like Anna Tsing, Donna Haraway, Bruno Latour
and many others coming from diverse fields like biology (Lynn Margulis,
Margaret McFall-Ngai) or anthropology (Eduardo Kohn, Eduardo Viveiros de
Castro) -to name just a few- a new paradigm is emerging based on a model
of symbiosis and fundamental entanglement. In view of climate change and
its pending disasters a multispecies way of apprehending the world can
offer a way out of negation, pure apocalyptic thinking or just <em>simple</em>
paralysis.</p>
<h2>Fourth meeting: grafted walnut trees</h2>
<p>Location: Vinay, Valley of the Isère, Le Grand Séchoir - Maison du Pays
de la Noix</p>
<p><img alt="" src="/essay/Tu(r)ning_into_Trees/GraftedTree.jpg"></p>
<p><em>Our second field trip took us to the valley of the Isère and the
adjacent Vercors massif, located between Valence and Grenoble in France.
As far as the eye could see, we were surrounded by vast walnut tree
plantations. The walnut trees we learned are grafted, in this technique,
which is different from cross-pollination, two plants are literally
joined into one by creating a wound in one and inserting another type of
plant into it so the tissues can grow together.</em> <em>The walnut trees are
literally pieced together by man in order to enhance economic viability,
mixing positive qualities of one species with those of another.</em></p>
<p>The technique of grafting walnut trees was developed and mastered in the
Vercors and is an important marker in the story of the famous Noix de
Grenoble, one of the first DOC labelled fruits. The story of the walnuts
in the Valley of the Isère is interesting in its own right. Far from
being an original regional product, the walnut business here started out
as the result of a biological disease, the infamous Grape phylloxera, a
type of yellow sap-sucking insects, that feed on the roots and leaves of
grapevines and which started spreading through the more traditional
vineyards of the region in the mid 1850s. The massive impact of this
biological event made more and more farmers turn to walnut production.</p>
<p>The walnut tree in particular thrived in the area, shaded from strong
winds and extreme colds by the massive of the Vercors that is aligned
alongside the river, the ground scattered with pebbles forming an ideal
type of soil for the walnut whose roots dive deep into the earth. The
grafting technique of walnut trees was developed to perfectionism by the
mid 19^th^ century, the increased production and industrialisation tying
in with the opening of a new railway line from Grenoble to Valence,
giving the Isère access to important international markets, pushing even
more farmers to favour the walnut business.</p>
<p>The landscape of walnut trees is based on interrelated stories of
different species, of insects, diseases, bacteria, bifurcating with the
regional geology and the rise of modern plantation technology,
industrial production and global capitalism. <em>"Landscapes enact
more-than-human rhythms. To follow these rhythms, we need new histories
and descriptions, crossing the sciences and humanities."</em><sup id="fnref:5"><a class="footnote-ref" href="#fn:5">5</a></sup> As Anna
Tsing so beautifully pointed out in her book <em>The Mushroom at the End of
the World</em>, the many interrelated encounters that form our contemporary
landscape, depend on soils, mushrooms, disease organisms and people,
whose knotting together at specific points in time can lead to massive
changes in the landscape.</p>
<p>Once we lay down the dominant image of linear modernistic time and it's
ideas of limitless growth and continued progress we become susceptible
to different rhythms that lie hidden in the landscapes of today, opening
up a new curiosity that allows us to follow 'multiple temporalities' and
open up to a 'revitalizing of description and imagination'. The famous
Noix De Grenoble, today one of the most important economical
agricultural products of the region tells a story of many intertwining
histories that gave rise to the plantations we see today, making the
region one of the largest exporters of walnuts in the world, yet at the
same time in a constant struggle of warding of the plagues and pests
that haunt monoculture lands.</p>
<p><strong>Last meeting: the Lady of the Mountain</strong></p>
<p>Location: Forêt de Coulmes, Vercors</p>
<p><em>From the place where we were staying we took a fieldtrip to the parc of
de Forêt de Coulmes, a natural forest high up in the mountaineas area
around the Vercors on an altitude varying between 750 and 1500 meters.
The main species in this dense and bucolic forest are beech trees
together with birch, pine, rowan, oak and ash. The dense shadowy forest
forms a sharp contrast to the open and strictly aligned walnut
plantations in the valley below. The forest is full of history evoked in
curious place names like le Pot de l\'Aigle, Beauregard, Col de Pra
l\'Étang, les Charbonniers. It is scattered with ruins of hammocks,
ancient water sources and old trails that sometimes end up in
mind-blowing panoramic vistas that uncover the little towns that lie
hidden inside the massive. Our guide An, lead us to this age-old forest
in a walk that acquainted us not only with the local species of trees
but also turned our attention to the Celtic values attributed to them:
the wisdom of the oak, the protective capacities of the hazelnut, the
strength and stability of the beech, to the mystical capacities of the
beautiful rowan tree. The rowan, with its alluring red berries is also
called the Lady of the Mountain, because it is a species that thrives in
harsh mountaineas areas like here in the Vercors. The Celts saw the
rowan tree as a holy and highly spiritual tree, only to be approached or
used by those who were initiated to higher forms of knowledge, the druid
or the shaman. Its red berries have healing capacities but can also be
poisonous. The tree can help humans fight against dark forces...</em></p>
<p>The celtic values of trees are based on the animist beliefs and
shamanistic practices of the celts, whose worldview was based on the
forces of nature. In their world non-human entities, animals, rivers,
mountains and trees were endowed with a soul. Looking back at these
ancient forms of knowledge, we learn that spiritual values were often
based on or guiding to material connections between tree and human, such
as types of usages of a tree's wood, or link in with nourishing or
medicinal capacities of its leaves, fruits, nuts or berries. By
acquainting ourselves with these spiritual essences (perspectives?)
humans can (re)- enter into meaningful rapports with more-than-human
beings and re-acquaint ourselves to forms of knowledge that are
forgotten in our modern age but are becoming more and more the subject
to a renewed interest today, in art, science and even popular culture.</p>
<p>As anthropologist Eduardo Viveiros de Castro argues, indigenous
knowledge can offer us insight in understanding the world as
'multiverse'. Ancient forms of 'cosmovisions' conceive of the earth,
man, animals, rivers, plants and stones as an indivisible interdependent
relationship. Anthropology and etnography in particular are ahead in
this multispecies approach and the decolonisation of western modernist
knowledge systems. In his book <em>How Forests Think</em>, ethnographer Eduardo
Kohn immersed himself in the forest culture of the Runa people of Avila,
a village in Ecuador's Upper Amazon, whose lives are fundamentally
intertwined with those of dogs, puma and the forest but just as much
with their relations to nearby postcolonial trading cities. <em>How Forests
Think</em>, argues that if we want to learn to understand and embrace the
multispecies worldview, we need not only to attune ourselves to other
beings, but also tune into the way these other beings see us and learn
to understand their modes of perception and their language, even though
it might not look like language to us. It is only when we see the world
as an 'ecology of selves', where the self dissolves into a larger
ecosystem, that we can move on to what Kohn refers to as an
'anthropology beyond the human'.</p>
<p><em>"We're all -- trees, humans, insects, birds, bacteria -- pluralities.
Life is embodied network. These living networks are not places of
omnibenevolent Oneness. Instead, they are where ecological and
evolutionary tensions between cooperation and conflict are negotiated
and resolved. These struggles often result not in the evolution of
stronger, more disconnected selves but in the dissolution of the self
into relationship."</em><sup id="fnref:6"><a class="footnote-ref" href="#fn:6">6</a></sup></p>
<p>Together with anthropology and ethnography, the contemporary art world
today is turning more and more away from the white walls of the safe
gallery spaces, diving deeper into the messiness of the world.
Fieldtrips, excursions and meetings with people from all kinds of
different domains are an aid in opening up our view, of exercising the
'arts of attunement' and of becoming-other. Speaking to forest managers,
to hunters, to owners of tree plantations, biologists, to local or
indigenous communities, to spiritual healers and farmers helps us
immerse ourselves into the strange worlds of 'more-than-human beings'
with which we share the world. This immersion in the multispecies
understanding of the world is all but a naïve retreat into a pristine
and bygone idea of returning to the wild but takes into account the
history, the formation of landscape, the ferality of life today where
manmade interference and nature can no longer be set apart from each
other. We need to attune, learn and immerse ourselves in order to regain
the feeling of belonging to the earth, which is the first step to
establish new forms of caring.</p>
<p>Multiple projects from the Alchorisma project dive further into these
thoughts, asking the question how we can enable and enhance the creative
potential of these techno-nature intertwinements, setting up new
becomings of roots, trunks, branches and leaves, between spirits of
trees and databased trees.</p>
<p><strong>References:</strong></p>
<p>Ginet, J., 'Contribution historique à l\'étude de la greffe du noyer en
Dauphiné',</p>
<p>Revue de Géographie Alpine, no. 19-1, 1931, pp. 187-198,
<a href="https://www.persee.fr/doc/rga_0035-1121_1931_num_19_1_4564">https://www.persee.fr/doc/rga_0035-1121_1931_num_19_1_4564</a> (accessed 3
March 2021)</p>
<p>Haskell, D. G., <em>The Songs of Trees: Stories from Nature\'s Great
Connectors,</em> New York, Penguin Books, 2017.</p>
<p>Kohn, E., <em>How forests think: Toward an Anthropology Beyond the Human,</em>
Berkeley, University of California Press, 2013.</p>
<p>Morton, T., <em>Being Ecological</em>, London, Penguin Books, 2018.</p>
<p>Press, C., 'A tree stump that should be dead is still alive; here's
why', Phys.org, 7 July 2019,</p>
<p><a href="https://phys.org/news/2019-07-tree-stump-dead-alive.html">https://phys.org/news/2019-07-tree-stump-dead-alive.html</a> (accessed 3
March 2021)</p>
<p>Provinciaal Natuur Centrum, 'Merkwaardige Bomen',
<a href="http://www.provinciaalnatuurcentrum.be/bomenabc">http://www.provinciaalnatuurcentrum.be/bomenabc</a>, (accessed 3 March
2021)</p>
<p>Tsing, Anna L., <em>The Mushroom at the End of the World: On the
Possibility of Life in Capitalist Ruins</em>, Princeton, Princeton
University Press, 2015.</p>
<p>Tsing, A. et al. (eds.), <em>Arts of Living on a Damaged Planet. Ghosts and
Monsters of the Anthropocene</em>, Minneapolis, University of Minnesota
Press, 2017.</p>
<p>Wohlleben, P., <em>The Hidden Life of Trees. What They Feel, How They
Communicate</em>, London, HarperCollins Publishers, 2017.</p>
<div class="footnote">
<hr>
<ol>
<li id="fn:1">
<p>A. Tsing et al. (eds.), Arts of Living on a Damaged Planet, p.
M18.&#160;<a class="footnote-backref" href="#fnref:1" title="Jump back to footnote 1 in the text">&#8617;</a></p>
</li>
<li id="fn:2">
<p>D. G. Haskell, <em>The Songs of Trees: Stories from Nature\'s Great
Connectors,</em> New York, Penguin Books, 2017, p. 84.&#160;<a class="footnote-backref" href="#fnref:2" title="Jump back to footnote 2 in the text">&#8617;</a></p>
</li>
<li id="fn:3">
<p>Ibid., p. 97&#160;<a class="footnote-backref" href="#fnref:3" title="Jump back to footnote 3 in the text">&#8617;</a></p>
</li>
<li id="fn:4">
<p>The Anthropocene is proposed by geologists as a new geological
era, after the pleistocene or holocene where new and purely manmade
materials are showing up in geological layers (eg, plastic or
radioactive waste materials like trinitite).&#160;<a class="footnote-backref" href="#fnref:4" title="Jump back to footnote 4 in the text">&#8617;</a></p>
</li>
<li id="fn:5">
<p>A. Tsing et al. (eds.), <em>Arts of Living on a Damaged Planet</em>, p.
G13.&#160;<a class="footnote-backref" href="#fnref:5" title="Jump back to footnote 5 in the text">&#8617;</a></p>
</li>
<li id="fn:6">
<p>D. G. Haskell, <em>The Songs of Trees: Stories from Nature\'s Great
Connectors,</em> p. viii.&#160;<a class="footnote-backref" href="#fnref:6" title="Jump back to footnote 6 in the text">&#8617;</a></p>
</li>
</ol>
</div>
</section>
    </main>

  </body>

<script>
  let sigils = document.getElementsByClassName("sigils");
  for (let sigil of sigils){
    let index = sigil.id.split("-")[1];
    let newpath = document.getElementById("newpath-"+index);
    sigil.addEventListener("mouseover", function(){
      newpath.classList.toggle("shimmer");
    });
    sigil.addEventListener("mouseout", function(){
      newpath.classList.toggle("shimmer");
    });
  }
</script>

</html>