# Alchorisma

Publication on the [Alchorisma worksession](https://constantvzw.org/site/-Alchorisma,217-.html)


## Conversion to markdown

```
pandoc --extract-media ./ --smart "in.docx" -r docx -t markdown -s -o "out.md"
```

https://stackoverflow.com/questions/39956497/pandoc-convert-docx-to-markdown-with-embedded-images

add required metadata

```
title: first encountered h1
author: make the "author" or "authors" line go at the start of the file
summary: copy the content of the short_intro file
```

rename the `media` folder to `article-name-media`,

then change the images paths from

```
![](.//media/image1.jpg)
```

to

```
!["some alt text"]({attach}out-media/image1.jpg)
```
