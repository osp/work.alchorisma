(function() {
  let button = document.querySelector('[data-role="fullscreen"]'),
      wrapper = document.querySelector('.iframe-wrapper');

  if (button && wrapper) {
    button.addEventListener('click', function (e) {
      e.preventDefault();
    
      if (!(document.fullscreenElement || document.mozFullscreenElement || document.webkitFullscreenElement)) {
        if (wrapper.requestFullscreen) {
          wrapper.requestFullscreen();
        } else if (wrapper.mozRequestFullScreen) {
          wrapper.mozRequestFullScreen();
        } else if (wrapper.webkitRequestFullScreen) {
          wrapper.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
        }
      } else {
        if (document.exitFullscreen) {
          document.exitFullscreen();
        } else if (document.mozExitFullscreen) {
          document.mozExitFullscreen();
        } else if (document.webkitExitFullscreen) {
          document.webkitExitFullscreen();
        }
      }
    });
  }
})();