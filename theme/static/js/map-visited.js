
// https://css-tricks.com/snippets/jquery/shuffle-dom-elements/
// (function($){

//   $.fn.shuffle = function(order) {
//       var allElems = this.get(),
//           getRandom = function(max) {
//               return Math.floor(Math.random() * max);
//           },
//           shuffled = $.map(allElems, function(i){
//               var random = getRandom(allElems.length),
//                   randEl = $(allElems[random]).clone(true)[0];
//               allElems.splice(random, 1);
//               return randEl;
//          });

//       this.each(function(i){
//           $(this).replaceWith($(shuffled[i]));
//       });

//       return $(shuffled);

//   };

// })(jQuery);

function make_order(length) {
  let order = [], shuffled = [],
      getRandom = function(max) {
        return Math.floor(Math.random() * max);
      };
  
  order = new Array(length).fill(null).map(function (v, k) { return k});
  
  while (order.length > 0) {
    shuffled.push(order.splice(getRandom(order.length), 1)[0]);
  }

  return shuffled;
}

function shuffle ($els, order) {
  if (!order) {
    order = make_order($els.length);
  }

  $clones = $els.clone(true);

  $els.map(function (key, $el) {
    $el.replaceWith($clones[order[key]]);
  });

  return order;
}

$(document).ready(function () {

  let file_index = JSON.parse(localStorage.getItem('file_index'));

  // insert explored
  if(file_index){

    for(let [page_name, page] of Object.entries(file_index)){
      if(page["explored"]){
        let id = page_name.split('/')[1].split('.')[0];
        $("#"+id).addClass('explored');
      }
    }
  }

  let map  = JSON.parse(localStorage.getItem('map'));

  if (!map || map.revision_id != window.REVISION_ID) {
    map = {
      revision_id: window.REVISION_ID,
      order: null
    };
  }

  // randomize
  map.order = shuffle($('nav#map > ul > li'), map.order);

  localStorage.setItem('map', JSON.stringify(map))

  $('.map-link').hover(function(){
    console.log(this);
    $(this).closest('.article-cart').toggleClass('hover');
  });

});
