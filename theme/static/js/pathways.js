const LINK_MODE_BIGRAMS = 'bigrams';
const LINK_MODE_SINGLE_WORD = 'single_word_links';

function getRandom(arr){
  // return a random element of an array
  return arr[Math.floor(Math.random() * arr.length)];
}

// ===================
//     HOVER EFFECT
// ===================

function pathwaysHoverIn(sigil){

  let bifurcation = $("#p-"+$(sigil).attr("id"));
  let link = $(sigil).children().first();

  bifurcation.addClass("shimmer");

  let bodyRect = document.querySelector('article').getBoundingClientRect(),
  sigilRect = link.get(0).getBoundingClientRect(),
  bifurcationRect = bifurcation.get(0).getBoundingClientRect(),
  occurenceRect = bifurcation.get(0).querySelector('.occurence').getBoundingClientRect(),
  x = sigilRect.left - bodyRect.left - (occurenceRect.left - bifurcationRect.left),
  y = sigilRect.bottom - bodyRect.top;

  bifurcation.css({
    'top': y.toString() + 'px',
    'left': x.toString() + 'px',
  });

}
function pathwaysHoverOut(sigil){
  let bifurcation = $("#p-"+$(sigil).attr("id"));
  bifurcation.removeClass("shimmer");
}

// ===================
//   PATH CREATION
// ===================

function activateLink(path_start, path_end) {
  // activate the link on the page we are on
  // return the list of remaining possibilities
  // path_start and path_end are triple of [filename, bigram_id, paragraph]

  if ($(document.body).hasClass('hide_inserted_links')) {
    // If the class hide_inserted_links is set on the body
    // simply don't activate the discovered links.
    return;
  }
  
  // -- INSERT LINK IN DOM
  let span;
  let link;
  let category;

  span = $("#"+path_start[1]);
  $(span).wrapInner("<a>");
  link = $(span).children().first("a");
  category = path_end[4]; // path_end[0].split("/")[0];
  $(link).addClass("sigil " + category)
  $(link).attr("href", "/"+path_end[0]+"#"+path_end[1]);

  // -- INSERT BIFURCATION TEXT IN DOM
  let bifurcation = $('<div>').append($('<div>').addClass('bifurcation-content-wrapper').html(path_end[2]));
  let occurence = $(bifurcation).find("#"+path_end[1]);
  occurence.addClass("occurence");
  // bifurcation = $(occurence).closest("p");
  bifurcation.attr("id", "p-"+path_start[1]);
  bifurcation.addClass("bifurcation " + category);

  // insert the bifurcation text
  let article = $("#"+path_start[1]).closest("article");
  article.append(bifurcation);

  // position the bifurcation text
  bifurcation.css({'position': 'absolute', 'top': '0px', 'left': '0px'});

  // -- CREATE HOVER FUNCTION
  let sigil = $("#"+path_start[1]);
  sigil.addClass("activated");
  // using closure function to pass argument
  sigil.hover(
    function(){ return pathwaysHoverIn(sigil); },
    function(){ return pathwaysHoverOut(sigil); }
  );

}

function reactivateLinks(activated_paths, file_name){

  let re_activated_links = 0;
  activated_paths.forEach(function(p){
    if(p[0][0] == file_name){
      // already starting here
      activateLink(p[0], p[1]);
      re_activated_links ++;
    }
    else if (p[1][0] == file_name) {
      // already ending here
      activateLink(p[1], p[0]);
      re_activated_links ++;
    }
  });

  return re_activated_links;
}

function pathHandler(activated_paths, file_index, bigram_index, single_word_index){

  let file_name = window.location.pathname.substring(1);

  if (file_index[file_name]["category"] == 'code-projects' || file_index[file_name]["category"] == 'visuals') {
    // Don't activate paths on code projects or visual projects
    // to avoid it disturbing the layout. But also as these are
    // end points.
    return;
  }

  let max_links = file_index[file_name]["max_links"];
  let explored = false;
  let current_links = file_index[file_name]["current_link_count"];
  // check if we already have the added key for current exploration
  if("explored" in file_index[file_name]){
    explored = file_index[file_name]["explored"];
  }

  console.log("explored", explored);
  console.log("max links on the page", max_links);
  console.log("current links on the page", current_links);

  let re_activated_links = reactivateLinks(activated_paths, file_name);
  console.log("re-activated links", re_activated_links);
  
  // if it has been explored we already have all the links
  if(!explored){
    // number of new links to create
    let create_links = max_links - current_links;
    console.log("new paths to create", create_links);

    let created_links = 0,
        maxTries = 100;

    for (var i = 0; i < maxTries && created_links < create_links; i++) {
      let link_mode;

      if (Math.random() < .5) {
        link_mode = LINK_MODE_SINGLE_WORD;
      }
      else {
        link_mode = LINK_MODE_BIGRAMS;
      }

      // -- CHOOSE PATH START (on current page)
      let startpoint = getRandom(file_index[file_name][link_mode]);
      let id_start = startpoint[0];
      let link_key = startpoint[1];
      let paragraph_start = file_index[file_name]["paragraphs"][startpoint[2]];
      let path_start = [file_name, id_start, paragraph_start, file_index[file_name]["title"], file_index[file_name]["category"]];

      // -- CHOOSE PATH END (on any connected page)
      let path_end_possibilities = (link_mode == LINK_MODE_SINGLE_WORD) ? Object.entries(single_word_index[link_key]) : Object.entries(bigram_index[link_key]);
      // do not go to the same article
      // do not go to an article that has already it's maximum number of path activated
      path_end_possibilities = path_end_possibilities.filter( pe => {
        if (pe[0] == file_name) {
          return false;
        }

        let max = file_index[pe[0]]["max_links"];
        let current = file_index[pe[0]]["current_link_count"];
  
        return current < max;
      });

      // if there is still a possibility
      if(path_end_possibilities.length > 0){

        // -- RANDOMLY CHOOSE ONE
        let path_end = getRandom(path_end_possibilities);
        let id_end = getRandom(path_end[1]);

        let endpoint = file_index[path_end[0]][link_mode].find( b => b[0] == id_end );
        // console.log(file_index[path_end[0]]["bigrams"]);
        // console.log(path_end);

        if (!endpoint) {
          console.log('no endpoint')
        }

        let paragraph_end = file_index[path_end[0]]["paragraphs"][endpoint[2]];
        path_end = [path_end[0], id_end, paragraph_end, file_index[path_end[0]]["title"], file_index[path_end[0]]["category"]];

        // -- ACTIVATE LINK
        activateLink(path_start, path_end);

        // -- ADD TO JSON FOR LOCALSTORAGE
        activated_paths.push([path_start, path_end]);

        // -- CHANGE CURRENT LINK VALUE FOR LOCALSTORAGE
        file_index[path_end[0]]["current_link_count"]++;

        // -- REMOVE EVERY BIGRAM FROM PARAGRAPH START/END
        // OF FILE_INDEX AND BIGRAM_INDEX

        // remove them from starting path possibilities in start file and end file
        let p_start_ids = $(paragraph_start).find(".bigram, .single_word").map(function(){ return this.id; }).get();
        let p_end_ids = $(paragraph_end).find(".bigram, .single_word").map(function(){ return this.id; }).get();
        let forbidden_ids = p_start_ids.concat(p_end_ids)

        file_index[path_start[0]]["bigrams"] = file_index[path_start[0]]["bigrams"].filter( b => !forbidden_ids.includes(b[0]));
        file_index[path_end[0]]["bigrams"] = file_index[path_end[0]]["bigrams"].filter( b => !forbidden_ids.includes(b[0]));
        file_index[path_start[0]]["single_word_links"] = file_index[path_start[0]]["single_word_links"].filter( b => !forbidden_ids.includes(b[0]));
        file_index[path_end[0]]["single_word_links"] = file_index[path_end[0]]["single_word_links"].filter( b => !forbidden_ids.includes(b[0]));

        // remove them from ending path possibilities
        for (let [_, dict] of Object.entries(bigram_index)){
          if (path_start[0] in dict){
            dict[path_start[0]] = dict[path_start[0]].filter(id => !forbidden_ids.includes(id));
            if (!dict[path_start[0]].length) {
              delete dict[path_start[0]];
            }
          }
          if (path_end[0] in dict){
            dict[path_end[0]] = dict[path_end[0]].filter(id => !forbidden_ids.includes(id));
            if (!dict[path_end[0]].length) {
              delete dict[path_end[0]];
            }
          }
        }

        created_links++;
      }
      else{
        console.log("path can not be created");
        // because all end point already have maximum paths count
      }
    }
    console.log("newly created links", created_links);

    // -- CHANGE EXPLORED VALUE FOR LOCALSTORAGE
    file_index[file_name]["current_link_count"] = current_links + created_links;
    file_index[file_name]["explored"] = true;
  }

  // console.log(activated_paths);

  // -- PUSH IN LOCALSTORAGE
  localStorage.setItem('activated_paths', JSON.stringify(activated_paths));
  localStorage.setItem('file_index', JSON.stringify(file_index));
  localStorage.setItem('bigram_index', JSON.stringify(bigram_index));
  localStorage.setItem('single_word_index', JSON.stringify(single_word_index));
  // REVISION_ID is set in the template by pelican and changes
  // every time pelican is ran. Store against which bigram id's
  // the current paths are generated.
  localStorage.setItem('revision_id', window.REVISION_ID);

}

// ===================
//    MOBILE SCROLL
// ===================

function detectMob(){
  // https://stackoverflow.com/questions/11381673/detecting-a-mobile-browser
  let check = false;
  (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
  return check;
};

function isScrolledIntoView(elem) {

  let win_top = $(window).scrollTop();
  let win_bottom = win_top + $(window).height();

  let thres_top = win_top + $(window).height()/4;
  let thres_bottom = win_bottom - $(window).height()/3;

  let elem_top = $(elem).offset().top;
  let elem_bottom = elem_top + $(elem).height();

  return ((elem_bottom <= thres_bottom) && (elem_top >= thres_top));
}


function activateMobileHover(){
  if(detectMob()){

    $(window).scroll(function(){

      $('.bigram.activated').each( function(){
        if (isScrolledIntoView(this) === true) {
          pathwaysHoverIn(this);
        } else{
          pathwaysHoverOut(this);
        }

      });
    });
  }
}


// ===================
//       MAIN
// ===================

$(document).ready(function () {


  // REVISION_ID is set in the template by pelican and changes
  // every time pelican is ran.
  let storage_revision_id = localStorage.getItem('revision_id');
  let file_index = JSON.parse(localStorage.getItem('file_index'));
  let bigram_index = JSON.parse(localStorage.getItem('bigram_index'));
  let single_word_index = JSON.parse(localStorage.getItem('single_word_index'));
  let activated_paths = JSON.parse(localStorage.getItem('activated_paths'));

  // Make sure the database is generated against the same REVISION_ID.
  if ( window.REVISION_ID && storage_revision_id
    && window.REVISION_ID == storage_revision_id
    && file_index && bigram_index && activated_paths ) {
    // we had all necessary in localstorage
    pathHandler(activated_paths, file_index, bigram_index, single_word_index)
    activateMobileHover();
  }
  else{
    // ajax request to load index files
    console.log("ajax request...");
    $.when($.getJSON('/file_linkindex.json'), $.getJSON('/bigram_linkindex.json'), $.getJSON('/single_word_linkindex.json')).done(function(a1, a2, a3){
      // when both async ajax requests done, do:
      console.log("done");
      file_index = a1[0];
      bigram_index = a2[0];
      single_word_index = a3[0];
      activated_paths = [];

      pathHandler(activated_paths, file_index, bigram_index, single_word_index)
      activateMobileHover();
    });
  }

});
