
$(function() {

    let mode = false;

    // footnotes code taken from http://contemporary-home-computing.org/affordance/
    function place_footnotes() {
        if (mode == 'desktop') {

            $('.footnote').addClass('footnote-side');

            $('article').addClass('footnote-processed');

            let articleTop = $('article').get(0).getBoundingClientRect().top;

            let minTop = 0;
            $('.footnote-side li').each( function() {
                let anchor = $(this).attr('id');
                fnAnchor = $('a[href="#'+anchor+'"]');
                let parent = fnAnchor.closest('p, ol, ul, blockquote, h1, h2, h3, h4').get(0);
                let top = parent.getBoundingClientRect().top - articleTop;
                top = Math.max(top, minTop);
                let fnHeight = $(this).css('top', top).height();
                minTop = top+fnHeight;
            });
        }
    }

    // CLOSE ALL THE DETAIL TAG BY DEFAULT ON MOBILE
    function close_details(){
      if( mode == 'mobile' ){
        $("details").removeAttr("open");
      }
    }

    // MOVE LICENSE INSIDE DETAIL TAG ON MOBILE
    function move_licenses(){
      let licenses = $(".licenses").detach();

      if (mode == 'mobile'){
        $("details main").append(licenses);
      }
      else if (mode == 'desktop'){
        $(".article-cart").append(licenses);
      }
    }

    // IMG GO FULLSCREEN ON CLICK
    $('img').click(function(){
      // clone the img
      let img = this,
          clone = document.createElement('img');

      if (img.srcset) {
        clone.src = img.srcset.split(' ', 1)[0];
      } else {
        clone.src = img.src;
      }

      $close = $('<section>').addClass('fullscreen--close-button');
      
      $close.click(function () {
        $(document.body).removeClass('has--fullscreen');
        $(this).closest('.fullscreen').remove()
      });
      
      // wrap it
      // clicking the clone to toggle zoom
      $(clone).click(function(){ $(this).closest('.fullscreen').toggleClass('zoom'); });
      let $fullscreen = $('<div>').addClass('fullscreen');
      $fullscreen.append(clone);
      $fullscreen.append($close);
      
      $('article').first().prepend($fullscreen);
      // append
      $(document.body).addClass('has--fullscreen');
    });

    // UPDATE DISPLAY IF NECESSARY ON RESIZE
    function update_mode(){

      let old_mode = mode;

      if(window.innerWidth < 1152) {
        mode = 'mobile'
      }
      else {
        mode = 'desktop'
      }

      if(old_mode != mode){
        console.log("breakpoint!");
        move_licenses();
        close_details();
      }
      place_footnotes();
    }

    update_mode();

    $(window).bind('resize', update_mode);
    $('img').on('load', update_mode);

    // SMOOTH SCROLL TO TOP BUTTON
    $('#to-top').click(function(){
      $('html, body').animate({
        scrollTop: 0
      }, 500);
    });

    // Let audio bookmarks set time code on closest player.
    $('.audiobookmark[data-time]').click(function () {
      let player = $(this).prevAll('.audio').first().find('audio'),
          allPlayers = $('audio');

      if (allPlayers.length > 1) {
        // Loop through all players on the page.
        // Pause them if they're playing
        allPlayers.each(function () {
          if (!this.paused && player.get(0) !== this) {
            this.pause();
          }
        });
      }

      player.one('seeked', function () {
        if (this.paused) {
          this.play();
        }
      });
      player.get(0).currentTime = this.dataset.time;
    });

});
