
$(document).ready(function () {

  let figuretag = '.scroll-on-click p';
  let imagetag = '.scroll-on-click img';

  // put an id to every image
  let last_index = 0;
  $(figuretag).each( function(index){
    // index = index;
    $(this).attr('id', "figure-"+index);
    last_index = index + 1;
  });

  // smooth-scroll to next id
  $(imagetag).mousedown(function(event) {

    let index = parseInt($(this).closest(figuretag).attr('id').split("-")[1]);
    let next_index = (index + 1) % last_index;
    let prev_index = (((index - 1)%last_index)+last_index)%last_index;
    let next_img = $("#figure-"+next_index);
    let prev_img = $("#figure-"+prev_index);
    let header_height = 0;

    // if mobile display scroll only to top of header
    if(window.screen.width < 992){
      header_height = $(".article-cart").first().height();
      header_height += parseInt($(figuretag).first().css('padding-top'));
    }

    let scrollto;
    switch (event.which) {
      case 1:
        // left click goes to next
        scrollto = $(next_img).offset().top - header_height;
        $('html, body').animate({
          scrollTop: scrollto
        }, 2000);
        break;
      case 2:
        // middle click does nothing
      case 3:
        // right click goes to prev
        scrollto = $(prev_img).offset().top - header_height;
        $('html, body').animate({
          scrollTop: scrollto
        }, 2000);
      break;
      default:
        // imposible does nothing
    }

  });

});
