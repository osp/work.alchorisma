from pelican import signals
from bs4 import BeautifulSoup, NavigableString
from pattern.en import parsetree
from .stopwordsPattern import stopwords
from .single_word_pathways import single_word_pathways

from random import randint

from math import ceil

from string import ascii_letters, digits

import re
import json

from collections import Counter

import csv

import os.path


MODE_CONTENT = '_content'
MODE_SUMMARY = 'summary'

# https://stackoverflow.com/a/1751478
def chunks(l, n):
  n = max(1, n)

  for i in range(0, len(l)-n-1):
    yield l[i:i+n]

# Can be better?
def make_id (length):
  chars = ascii_letters + digits
  charcount = len(chars)-1
  return ''.join([chars[randint(0, charcount)] for _ in range(length)])

def insert_single_span (linkid, match):
  linkid = make_id(6)
  return '{head}<span class="single-word" id="{linkid}">{label}</span>{tail}'.format(linkid=linkid, head=match.group(1), label=match.group(2), tail=match.group(3))

# Function which actually inserts the found match.
def insert_bigram_span (linkid, match):
  return '{head}<span class="bigram" id="{linkid}">{label}</span>{tail}'.format(linkid=linkid, head=match.group(1), label=match.group(2), tail=match.group(3))

def count (key, counter, test = None):
  if test is None or test(key):
    if key not in counter:
      counter[key] = 1
    else:
      counter[key] += 1

def find_parent (node):
  for tag in ['p', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'li', 'blockquote', 'div', 'section']:
    parent = node.find_parent(tag)

    if parent:
      return parent

def find_content_links (article_generator):
  # bigram as key
  # {(lemmaA:str, lemmaB:str): [(filename:str, a:Word, b:Word),]}
  bigrams = {}
  article_index = {}

  output_path = article_generator.settings['OUTPUT_PATH']

  # Counter to make a frequency list
  counter_global = Counter()
  counter_per_article = {}

  single_word_unwound = []
  single_word_index = {}

  for wordId, synonyms in enumerate(single_word_pathways):
    single_word_unwound.extend(synonyms)
    for word in synonyms:
      single_word_index[word] = wordId

  # Generator (find in generators.py) has a property articles with all articles
  for article in article_generator.articles:
    print("Parsing {}".format(article.source_path))

    article_lemma_list = []

    # Article (find in contents.py)
    extract_mode = MODE_SUMMARY if article.category.name in ['visuals', 'code-projects'] else MODE_CONTENT
    # Don't touch article.content as then it'll be memoized

    if extract_mode == MODE_SUMMARY and 'summary' in article.metadata and article.metadata['summary']:
      article_soup = BeautifulSoup(article.metadata['summary'], 'html.parser')
    else:
      article_soup = BeautifulSoup(article._content, 'html.parser')

    footnotes = article_soup.find('div', class_='footnote')
    if footnotes:
      # There are footnotes in this articles, remove from the tree
      # and store html in index
      footnotes = str(footnotes.extract())
    
    # Construct index for later in processing
    article_index[article.save_as] = [article, article_soup, footnotes, extract_mode]

    raw_content = ' '.join(list(article_soup.stripped_strings))
    raw_content = raw_content.replace('’', '\'').replace('‘', '\'')
    parsed = parsetree(raw_content, tokenize=True, tags=True, chunks=False, relations=False, lemmata=True)

    for sentence in parsed.sentences:
      # Transform into standard list and filter out interpunction
      word_list = list(filter(lambda w: w.tag not in ['O', ',', 'SYM', '(', ')', ':', '', '#', '"'] \
                                  and w.lemma not in ['^', '’', '‘', '\'', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.', '?'] \
                                  and not w.lemma.startswith('http'), list(sentence.words)))

      for a, b in chunks(word_list, 2):
        # Drop if b is in stopwords, or if both a and b are
        # in stopwords. If b is in stopwords, we don't care
        # about a
        if a.lemma in stopwords and b.lemma in stopwords:
          continue

        if a.lemma in single_word_unwound:
          # As this word is is in the single_word_links
          # the bigram will not be considered
          # Add the way it occured to the patways to make sure it'll be replaced later
          if a.string.lower()  not in single_word_pathways[single_word_index[a.lemma]]:
            single_word_pathways[single_word_index[a.lemma]].append(a.string.lower())
          continue

        if a.lemma not in stopwords:
          article_lemma_list.append(a.lemma)

        if b.lemma not in stopwords:
          article_lemma_list.append(b.lemma)

        bigram = "{}--{}".format(a.lemma, b.lemma) # (a.lemma, b.lemma)

        if bigram not in bigrams:
          bigrams[bigram] = { article.save_as: [] }
        elif article.save_as not in bigrams[bigram]:
          bigrams[bigram][article.save_as] = []

        # (filename:str, a:Word, b:Word)
        bigrams[bigram][article.save_as].append((a.string, b.string))


    counter_local = Counter(article_lemma_list)
    counter_per_article[article.save_as] = counter_local.most_common()
    counter_global += counter_local


  # Filter bigrams so we treat only bigrams occuring in two or more files
  print("Filtering bigrams")
  bigrams = { bigram: occurences for bigram, occurences in bigrams.items() if len(occurences.keys()) > 2 }

  print("Replacing single words in text.")

  single_word_file_linkindex = { article.save_as: [] for article in article_generator.articles }
  single_word_linkindex = { k: {} for k in range(len(single_word_pathways)) }
  single_word_id_index = {}

  for save_as in article_index:
    for wordId, synonyms in enumerate(single_word_pathways):
      search_regex = re.compile('(\W|^)({})(\W|$)'.format('|'.join(map(re.escape, synonyms))), flags=re.I)
      for fragment in article_index[save_as][1].find_all(text=search_regex):
        # Make sure this is within a allowed tag
        # to avoid matching spans, but also script or style tags
        if fragment.parent.name in ['p', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'li', 'blockquote', 'div', 'section']:
          head = NavigableString('') # Reference to know where to insert fragments
          fragment.insert_before(head)
          tail = str(fragment.extract()) # Remove fragment from tree, convert to string
          
          m = re.search(search_regex, tail)
          
          # Loop while matches are found in the fragment split on each match
          # Insert head, before the match back into the tree
          # wrap the match with a span and insert into the tree.
          #
          # Continue on remaining fragment
          while m:
            start = m.start(2)
            end = m.end(2)
            linkid = make_id(6)
            single_word_file_linkindex[save_as].append((wordId, linkid))
            single_word_id_index[linkid] = wordId

            if not save_as in single_word_linkindex[wordId]:
              single_word_linkindex[wordId][save_as] = []

            single_word_linkindex[wordId][save_as].append(linkid)
        
            if start > 0:
              fragment_head = NavigableString(tail[:start])
              head.insert_after(fragment_head)
              head = fragment_head

            single_word = article_index[save_as][1].new_tag('span')
            single_word['class'] = 'single_word'
            single_word['id'] = linkid
            single_word.append(NavigableString(m.group(2)))

            head.insert_after(single_word)
            head = single_word
            tail = tail[end:]

            m = re.search(search_regex, tail)

          if tail:
            head.insert_after(NavigableString(tail))

      # article_index[save_as][1] = re.sub('({})'.format('|'.join(map(re.escape, synonyms))), regex_callback, article_index[save_as][1], flags=re.I)



  for wordId, occurences in list(single_word_linkindex.items()):
    occured_in = list(occurences.keys())
    if len(occured_in) == 1:
      print('Word occured only in one file, it will be removed, ', single_word_pathways[wordId])
      single_word_file_linkindex[occured_in[0]] = list(filter(lambda r: r[0] not in occurences[occured_in[0]], single_word_file_linkindex[save_as]))

      for linkid in single_word_linkindex[wordId][occured_in[0]]:
        del single_word_id_index[linkid]

      del single_word_linkindex[wordId]
  
  print("Replacing bigrams in text.")

  bigram_file_linkindex = { article.save_as: [] for article in article_generator.articles }
  bigram_linkindex = {}
  bigram_id_index = {}
  # { str:bigram : { str:save_path : [ str:id, ... ], ... }, ... }

  # Loop through sorted bigrams, by amount of files it occurs in, ascending
  # Hoping we go from most precise to least precise
  # See if they occure in at least two texts
  for bigram, occurences in sorted(bigrams.items(), key=lambda r: len(r[1].keys())):
    bigram_linkindex[bigram] = {}

    for save_as, search_pairs in occurences.items():
      # Search_pairs is a list of tuples with the bigrams [(A1, B1), (A2, B2)]
      # transform to two lists [A1, A2] [B1, B2]
      occurences_a, occurences_b = zip(*search_pairs)

      search_regex = re.compile('(\W|^)((?:{})\W?\s+\W?(?:{}))(\W|$)'.format('|'.join(map(re.escape, set(occurences_a))), '|'.join(map(re.escape, set(occurences_b)))), flags=re.I) 
      for fragment in article_index[save_as][1].find_all(text=search_regex):
        if fragment.parent.name in ['p', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'li', 'blockquote', 'div', 'section']:
          head = NavigableString('')
          fragment.insert_before(head)
          tail = str(fragment.extract())
          
          m = re.search(search_regex, tail)
          
          while m:
            start = m.start(2)
            end = m.end(2)
            linkid = make_id(6)
            bigram_file_linkindex[save_as].append((bigram, linkid))
            bigram_id_index[linkid] = bigram
        
            if save_as not in bigram_linkindex[bigram]:
              bigram_linkindex[bigram][save_as] = []

            bigram_linkindex[bigram][save_as].append(linkid)
        
            if start > 0:
              fragment_head = NavigableString(tail[:start])
              head.insert_after(fragment_head)
              head = fragment_head

            single_word = article_index[save_as][1].new_tag('span')
            single_word['class'] = 'bigram'
            single_word['id'] = linkid
            single_word.append(NavigableString(m.group(2)))

            head.insert_after(single_word)
            head = single_word
            tail = tail[end:]

            m = re.search(search_regex, tail)

          if tail:
            head.insert_after(NavigableString(tail))

  # { str:id : str:bigram, ... }
  # Filter the index
  for bigram, occurences in list(bigram_linkindex.items()):
    # Occurences: { str:filename: [ str:id, ... ], ... }
    occured_in = list(occurences.keys())

    if len(occured_in) < 2:
      print('Bigram `{}` occurs only in one file. Deleting.'.format(bigram))

      if len(occured_in) == 1:
        bigram_file_linkindex[occured_in[0]] = list(filter(lambda r: r[0] is not bigram, bigram_file_linkindex[occured_in[0]]))

        for link_id in bigram_linkindex[bigram][occured_in[0]]:
          del bigram_id_index[link_id]
      
      del bigram_linkindex[bigram]

  file_linkindex = {}

  print("Extracting content windows")

  # Go through all articles to extract windows per bigram and single word link
  # And to measure the maximum amount of links
  # Find all valid bigrams and unwind invalid ones
  for path, (article, soup, footnotes, extract_mode) in article_index.items():
    bigrams = []
    valid_bigram_ids = bigram_id_index.keys()
    for bigram in soup.find_all("span", attrs={ 'class': 'bigram' }):
      if bigram.get('id') in valid_bigram_ids:
        bigrams.append(bigram)
      else:
        bigram.unwrap()

    valid_single_word_ids = single_word_id_index.keys()
    single_words = []
    # Find all valid single words and unwind invalid ones
    for single_word in soup.find_all("span", attrs={ 'class': 'single_word' }):
      if single_word.get('id') in valid_single_word_ids:
        single_words.append(single_word)
      else:
        single_word.unwrap()

    parents = list(set([ find_parent(node) for node in (bigrams + single_words) ]))

    max_links = ceil(len(parents) / 5)

    file_linkindex[path] = {
      'title': article.title,
      'category': article.category.name,
      'max_links': max_links,
      'current_link_count': 0,
      'paragraphs': list(map(str, parents)),
      'bigrams': [ (bigram.get('id'), bigram_id_index[bigram.get('id')], parents.index(find_parent(bigram))) for bigram in bigrams ], 
      'single_word_links': [ (single_word.get('id'), single_word_id_index[single_word.get('id')], parents.index(find_parent(single_word))) for single_word in single_words ]
    }

    if extract_mode == MODE_SUMMARY:
      article.metadata['summary'] = article._summary = str(soup)
    else:
      article._content = str(soup) + footnotes if footnotes else str(soup)

  print("Exporting indexes")

  json.dump(file_linkindex, open(os.path.join(output_path, 'file_linkindex.json'), 'w'))
  print(os.path.join(output_path, 'file_linkindex.json'))
  json.dump(bigram_linkindex, open(os.path.join(output_path, 'bigram_linkindex.json'), 'w'))
  print(os.path.join(output_path, 'bigram_linkindex.json'))
  json.dump(single_word_linkindex, open(os.path.join(output_path, 'single_word_linkindex.json'), 'w'))
  print(os.path.join(output_path, 'single_word_linkindex.json'))

  json.dump(counter_global.most_common(), open(os.path.join(output_path, 'word_frequency_global.json'), 'w'))
  print(os.path.join(output_path, 'word_frequency_global.json'))
  json.dump(counter_per_article, open(os.path.join(output_path, 'word_frequency_per_artcle.json'), 'w'))
  print(os.path.join(output_path, 'word_frequency_per_artcle.json'))

  print("Exporting counters")
  with open(os.path.join(output_path, 'word_frequency_global.csv'), 'w', newline='') as csvfile:
    print(os.path.join(output_path, 'word_frequency_global.csv'))
    fieldnames = ['lemma', 'count']
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

    writer.writeheader()
    for lemma, count in counter_global.most_common():
      writer.writerow({'lemma': lemma, 'count': count})

  for filename, counter in counter_per_article.items():
    filename, _ = os.path.splitext(os.path.basename(filename))
    with open(os.path.join(output_path, 'word_frequency_{}.csv'.format(filename)), 'w', newline='') as csvfile:
      print(os.path.join(output_path, 'word_frequency_{}.csv'.format(filename)))
      fieldnames = ['lemma', 'count']
      writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

      writer.writeheader()
      for lemma, count in counter:
        writer.writerow({'lemma': lemma, 'count': count})

# Set Revision id, so the js can invalidate local storage when
# the website is regenerated
def set_revision_id (pelican):
  pelican.settings['REVISION_ID'] = make_id(8)

def register_glossary(pelican):
  pelican.settings['GLOSSARY'] = single_word_pathways

def register():
  signals.article_generator_pretaxonomy.connect(find_content_links)
  signals.initialized.connect(set_revision_id)
  signals.initialized.connect(register_glossary)
