#!/usr/bin/env python
# -*- coding: utf-8 -*- #

AUTHOR = 'CONSTANT'
SITENAME = 'Alchorisma'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Pelican', 'https://getpelican.com/'),
         ('Python.org', 'https://www.python.org/'),
         ('Jinja2', 'https://palletsprojects.com/p/jinja/'),
         ('You can modify those links in your config file', '#'),)

# Social widget
SOCIAL = (('You can add links in your config file', '#'),
          ('Another social link', '#'),)

DEFAULT_PAGINATION = False

# Uncomment following line if you want document-relative URLs when developing
# RELATIVE_URLS = True


# ____ CUSTOM PARAMETERS _____

LOAD_CONTENT_CACHE = False


# --- METADATA

# read the date in the filestamp
DEFAULT_DATE = 'fs'

USE_FOLDER_AS_CATEGORY = True


# --- STRUCTURAL

# default slug is the title
ARTICLE_SAVE_AS = '{category}/{slug}.html'
ARTICLE_URL = '{category}/{slug}.html'

# disable the generation of those html files
PAGE_SAVE_AS = ''
AUTHOR_SAVE_AS = ''
TAG_SAVE_AS = ''
CATEGORY_SAVE_AS = ''

# list of singular page template
DIRECT_TEMPLATES = ['index', 'map']
PLUGIN_PATHS = ['plugins']
PLUGINS = ['insert_audio_bookmarks', 'find_content_links']

ARTICLE_EXCLUDES = ['code-projects/code', 'dreams-fictions/static', 'scores/static']
PAGE_EXCLUDES = ['code-projects/code', 'dreams-fictions/static', 'scores/static']
STATIC_PATHS = ['code-projects/code', 'dreams-fictions/static', 'scores/static', 'scores/i-lost-my-training-of-thought-media']

# --- THEME

# theme path
THEME = 'theme'

# Should be set by the find_content_links plugin
REVISION_ID = None

MARKDOWN = {
    'extension_configs': {
        'markdown.extensions.extra': {},
        'markdown.extensions.meta': {},
        'mdx_figcaption': {}
    },
    'output_format': 'html5'
}
