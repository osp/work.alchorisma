title: H(a)untology
authors: Artyom Kolganov, Arthur Gouillart, Gaspard Bébié-Valérian
summary: One of the local witch stories of Hasselt includes ‘the fish stone’, which was used for cutting fresh fish during city markets. While the good parts of the fish were sold, the bad ones were thrown onto a dirt heap that was visited by cats searching for food. Cats had a negative connotation as they were thought of as signalling the presence of witches, and they were therefore expelled from the market. ‘H(a)untology’ is a ritual that was performed in Hasselt to reverse this historical inequity and restore the reputation of the witches. The text presented here was developed for the ritual and was read aloud during the performance.
license: fal

# H(a)untology

Hasselt has been associated historically with witches. One of the local
witch stories concerns 'the fish stone', which was used for cutting
fresh fish during city markets. While the good parts of the fish were
sold, the bad ones were thrown onto a dirt heap that was visited by cats
searching for food. Cats had a negative connotation as they were thought
of as signalling the presence of witches, and they were therefore
expelled from the market.

The ritual, which took place in the garden of the Municipal Museum,
consisted in cutting fish on the fish stone, leaving the best part for
stray cats. The ritual intended to reverse a historical inequity and to
restore the reputation of the witches. Below is the text that
accompanied the ritual and was read aloud during the performance.

<section class="side-to-side" markdown="1">
![]({attach}hauntology-media/image1.jpg){.image-process-illustration--inline}
![]({attach}hauntology-media/image2.jpg){.image-process-illustration--inline}
</section>

1 stone  
3 fish  
3 knives

Visible and invisible forces

Potential cats

A spectre haunts Europe,
A spectre hunts witches.

Capitalist sorcery doesn't need sorcerers. The logic of acquisition is
the logic of extinction.

Using the knife,

-   Cut the caudal fin,

then

-   Cut the dorsal fin,

We Cats

then

-   Cut the pelvic fin,

We Stones

then

-   Cut the pectoral fin,

We Fish

then

-   Cut the adipose fin,

We Witches

then

-   Cut the anal fin,

On this stone, the authority of the market held the knife.

The knife slashed fishes.
The knife separated lives.
The knife wounded non-humans.
The knife nourishes.

If all the fins are cut:

-   Process,
-   Take a long knife,

No more witches. Is that a question?

-   Maintain the fish firmly on the table,
-   Scrub the scales on one side,
-   Scrub the scales on the other,

If successful:

-   Rotate the fish,
-   Present its belly,

Cats as the continuation of the witches. Is that a question?

Cats as signalling the presence of witches, really?

The market as a context, remnants of a context,  
a history, a place.  
The erosion of a stone,  
the stone being eroded,  
the burning of a witch,  
witches being burnt.  
The eviction of a cat,  
cats being evicted  
from the market.  
The erosion of memory,  
the burning of the past,  
the removal of a minor narrative.  
From the anus, cut alongside towards the fish tail.

Then

-   Remove the guts,

Marginal lives are being given marginal values.  
Excluded cats are being given discarded resources.  
Do they know they’ve been excluded?  
I think they do.  
Actually.

Then

-   Remove the gills,

both sides.

If successful,

-   Cut 1 cm away from the gills, then push your knife into the fish
    spine,

Maintain the fish firmly on the head with your hand, pull and cut the
fish going towards the tail.

Otherwise:

Let’s reassemble the past to get access to the present. Let’s infect
the present with the past. An offering for reparation, an offering for
contamination.

Void function fillet (side)

-   Raise the first fish fillet,

then

-   Cut lower and dark part,

Hauntology is our ontology.

Then

-   Reverse and repeat the previous fillet operation.

While humans are able:

Wait until the cats arrive.

Hauntology is our ontology.

Otherwise:

To haunt is to hunt.

Wait until the rain stops.

Go away.
