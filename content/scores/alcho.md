title: Alcho
author: Annie Abrahams
summary: In Alcho Annie Abrahams asks: Is it possible to create an algorithm that includes human behaviour, that evolves over time and in the process integrates aspects of the personalities of all the participating individuals? Because there are humans involved Annie thinks the project will probably turn out to be an erratic, iterative, evolving, experiential happening. She is interested in the written traces; in the written instruction sets, as markers of the evolving Alcho processed through humans by writing and performing.
license: fal

# Alcho

# An online experiment of Performing Alchoritms / Performing Processes using algorithms [^1]

![]({attach}alcho-media/image1.jpeg){.image-process-illustration--inline}

**Invitation**: Please read the Alchoritm and make an appointment with someone on
<https://meet.jit.si/PerformingAlchoritms>. Send this person the
meta-instructions. Follow the Alchoritm as best you can. Don’t discuss it, just go ahead
with what you understand.

**Annie afterwards**: It seems as if such a performance can keep its essence, at least during
5 iterations. I love the way the performance instruction sets evolve and
show how differently you can write down instruction sets. It makes me
long for a longer series.

## Alchoritm

### Variables:

<div class="variables" markdown="1">
* meta-instructions
* performance instructions (**never to be talked about**)
* internet connexion
* webcam
</div>

### Meta-instructions:

<div class="instructions" markdown="1">
* Connect to the meeting at the right time.
* Watch the performance attentively and leave the meeting immediately afterwards (don't discuss intentions).
    * Discover/determine what is **essential** for you in what you saw
* Write the performance down as an instruction set
    * A translation of \[what you saw\] to \[what to do, so others can **experience** something similar through a new iteration of the performance\]
    * Keep in mind this is not about being precise, but about intentionality.
* Send an email invitation plus the meta-instructions to another person and ask him/her to join the project at **a time set by you**.

Before the set time:

* Re-read attentively the performance instructions you wrote yourself.
    * Discover what you think is **essential** in this instruction set.

At the set time:

* Connect to the meeting.
* Ask the person you invited if she is ready to watch your performance.
Perform the instruction set.
    * Be very clear about when you start.
    * Follow the instructions and pay attention to what you think is* **essential.** Keep in mind this is not about being precise, but about intentionality.
    * Be very clear about when you end the performance.
* Leave the meeting without discussing.
* Send the instruction set you wrote to Annie Abrahams <bram.org@gmail.com>.
</div>

### Possible performance instructions for a first iteration:

<div class="framed" markdown="1">
* watch the other person attentively for around 30 sec.
* slowly bring your hand to your webcam
* caress the webcam (watch the image you produce, try to give this  
quality)
* slowly bring back your hand, quickly caress your face
* watch the other person attentively for around 30 sec.
</div>

<div class="framed" markdown="1">
Needs: an alarm

* watch the other person attentively for around 30 s
* set an alarm for 5 minutes and show this.
* close your eyes
* open your eyes when the alarm goes
* watch the other person attentively for around 30 s
</div>

<div class="framed" markdown="1">
Before: choose a short sound or video you want to share

* watch the other person attentively for around 30 sec.
    * If video: screenshare the video.
    * If audio: cut your videostream and ask the other to listen to the audio (URL in chat space)
* listen or watch yourself too

When finished

* watch the other person attentively for around 30 sec.
</div>

## Alcho Beta test v1

February--March 2021

Initiated by Annie Abrahams

Participants: An Mertens, Ils Huygens, Adva Zakai, RYBN.ORG, Karen
Dermineur and Dominique Albouy

### Performance instructions set 1 <br> (executed twice by Annie → a and b series)

<div class="framed" markdown="1">
Sit back and stare at the person for a few secs  
Then slowly get closer to the webcam until you touch it with your face  
Continue caressing the webcam with your face  
After 1 min. your hands take over, first from very close, they caress,  
they rub, they dance  
Return slowly to the original position  
And say goodbye
</div>

### Series a

#### Performance instruction set 2a

<div class="framed" markdown="1">
Sit still at approx. 1 m from the screen  
Look straight at the camera for a few seconds  
Move your body very slowly towards the camera, while keeping your face
central  
Caress the camera gently with your face  
Move back a few centimetres so there is a little space  
In this space, cover your face with both hands as close as possible to
the camera  
Caress the screen with hands over your face  
Interlace the fingers of both hands while still holding them over your
face  
Continue caressing the camera with interlaced hands  
Point your interlaced fingers slowly towards the camera  
Rub your hands while slowly moving your face back  
Continue rubbing your hands while moving your body back into your seat  
When fully seated, move your hands towards your heart  
Hold your hands in a prayer position before your heart  
move your hands next to your body  
Look straight at the camera  
Smile  
Say goodbye  
</div>

#### Performance instructions set 3a

<div class="framed" markdown="1">

Sit in front of the computer camera  
Look at the person in front of you on the screen  
Bring your face as close as possible to the camera.  
Think of the person in front of you on the screen while you do it  
Tell yourself at heart: I wish this person could feel that they are
entering my body through my skin  
Move your face slowly, try to show the camera every little detail of
your face  
Stay close enough to the camera to feel the warmth emanating from the
working computer  
Let this warmth influence the quality of your movement  
Keep this quality throughout the rest of the performance  
Lift both your elbows in front of your face, pointing towards the camera  
Slide your hands up through the torso until they cover your face  
Let your hands touch each other  
Slowly distance yourself from the camera, while covering your face with
your hands  
Sit back in a straight position  
Slide your hands down your face and let them come together in front of
your heart, palms touching  
Look at the person in front of you on the screen  
Smile  
Wave goodbye

</div>

### Series b

#### Performance instructions set 2b

<div class="framed" markdown="1">
Sit at a distance  
Watch the screen with a friendly look  
Come closer to the screen  
Show your nose, eyes, ears, skin in close-up  
Show your hands in close-up  
Rub your hands and fingers  
Move back to the chair  
Smile at the camera
</div>

#### Performance instructions set 3b

<div class="framed" markdown="1">

La position initiale c'est être assez loin de la caméra pour être vu de
plain pied, faire face à la caméra.

Annoncer que la performance a commencé;

1. prendre un temps pour soi sans bouger et se concentrer sur ce qui va
arriver
2. se lever doucement et se rapprocher, en regardant fixement un coin
de l'écran, attentivement,
3. rapprocher tout doucement son oeil de la caméra, le rapprocher au
point que la camera filme l'oeil en gros plan, puis au point de le
faire disparaître.
4. faire glisser la camera (en se déplacant soi-même) doucement sur la
peau du visage, pendant un temps
5. approcher la main, recouvrir la caméra de la main et cacher son
visage de la caméra derrière sa main, filmer la main, toujours en gros
plan, pendant un temps.
6. faire venir la deuxième main et former une grille en entrelaçant les
doigts devant la caméra, comme pour attraper la camera (ou l'image)
avec
ses mains.
7. une fois l'image attrapée dans les mains, faire mine de la
caresser,
envoyer l'image au creux de ses mains, longtemps.
8. interstices, écarter les doigts, multiplier les plans, montrer la
paume en arrière plan à travers les doigts, faire apparaître des bouts
de mains
9. joindre les doigts, les frotter, caresser ses mains
10. s'éloigner de la camera, s'asseoir, retourner à la place initiale.

</div>

#### Performance instructions set 3c

<div class="framed" markdown="1">

Tu es là, debout devant moi, à quelques pas de moi. Tu me regardes. Fixement.
Mais je ne peux lire sur ton visage aucune expression particulière. Tu me regardes, c'est tout.
Et puis tu t'avances vers moi, doucement.
Tu te rapproches tout près de moi, jusqu'à ce que je ne vois plus que
ton œil, ton œil droit.
Ton œil en gros plan me regarde, puis se cherche dans cet espace
réduit.
Puis se ferme.
Et ensuite, c'est la peau.
Ta peau en gros plan, tes mains, je peux même lire tes lignes de la
main.
Tes doigts se froissent, se frottent, se mélangent, en macro.
C'est flou et c'est rose, c'est doux et c'est chaud.
Puis l'étreinte se termine.
Tu es seule, face à moi, tu me regardes fixement, et toujours en
silence, tu recules doucement jusqu'à ta place initiale.
Fin.

</div>

#### Performance instructions set 3d

<div class="framed" markdown="1">

*La personne est debout face à moi/caméra à quelques mètres*

Elle s'approche doucement à pas réguliers, jusqu'à venir devant
l'objectif, regarde à droite et à gauche comme pour vérifier si on la
regardait, puis fixe son oeil gauche sur l'objectif, comme pour voir à
travers, puis ferme les yeux.

je ne vois plus que du blanc, c'est curieux je ne comprends pas tout de
suite, mais ensuite je vois ses mains qui se frottent l'une à l'autre
puis s'éloignent de l'objectif. Peut être une caresse à un animal ?
dans tous les cas c'est bienveillant

Elle reprend sa position debout, puis recule à pas lents comme elle
était venue.

</div>

[^1]: Alcho ( online v1 beta ) is a project by Annie Abrahams. It is a
      follow-up to *Testing Performing Processes using Algorithms / Performing
      Secret Alchoritms* performed by RYBN and Annie Abrahams during the
      Alchorisma worksession organized by Constant and Z33 in Dec. 2018 in
      Hasselt. More info: [Secret Alcho Performing]({filename}secret-alcho.md).


<style>
  :is(.instructions, .variables){
    font-family: monospace;
  }
  :is(.instructions, .variables) ul{
    margin-top: 0;
  }
  .instructions li{
    list-style: none;
  }
  .instructions li li{
    font-style: italic;
  }
  .framed{
    border: calc(var(--vine-width-thin) / 2) dotted var(--vine-color);
    padding: 0 var(--pad);
    margin: var(--em-space) 0;
  }
</style>
