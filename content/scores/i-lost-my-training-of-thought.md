title: I lost my training of thought
authors: Adva Zakai, Femke Snelting & Isabella Aurora
summary: Adva, Femke and Isabella perform loops over multiple modes of training. Their spell moves through drawings, texts and hyperlinks to ask: how to be responsible with that which disappears without a trace? If yes: allow magic to emerge.
license: fal
hide_inserted_links: true

<h1>I lost my training<br>of thought</h1>

<div class="loop">
    <p>How to train a tree?</p>
</div>
<img src="i-lost-my-training-of-thought-media/01.jpg">

<div class="loop">
    <p>Wake up early, together with the sun.</p>
    <p>Imagine you see the world from a bird's eye.</p>
    <p>From this perspective, nothing will be overlooked. </p>
    <p>Detect any undisciplined trees.</p>
    <p>Tie their branches horizontally to something…</p>
    <p>and remember to lower your head in front of nature and humbly thank it.</p>
</div>
<img src="i-lost-my-training-of-thought-media/02.jpg">
<div class="loop">

    <p> <span id="overlooked">Overlooked? → Yes → <a href="#what">Ask what</span></a> </p>
    <p> ↓ </p>
    <p> No </p>
    <p> ↓ </p>
    <p><a href="#bird">Ask how</span></a></p>

</div>

<img src="i-lost-my-training-of-thought-media/03.jpg">
<div class="loop">
  <p>to include</p>
</div>
<div class="loop">
  <img src="i-lost-my-training-of-thought-media/04.jpg">
</div>

<p><a href="#disappear"><span id="emerge">the imperceptible</span>,</a></p>
<p>the unresponsive,</p>
<p>the too slow, the too fast,</p>
<p>dormant, latent, dead, inorganic,</p>
<p>nonagential, nonthings.</p>

<div class="loop">
  <img class="scale" src="i-lost-my-training-of-thought-media/05.jpg">
</div>

<div class="loop">
  <p>How to be responsible to that which disappears without a trace?</p>
</div>

<p>track a tree</p>
<p>train a trace to dissapear</p>
<p>trace that which disappears</p>
<p>trace that witch</p>
<div class="loop">
  <img src="i-lost-my-training-of-thought-media/06.jpg">
</div>
<p>which witch</p>
<p>spelling what</p>
<p>which spell to train</p>

<div class="loop">
    <p>To cast a spell and to spell a word are actually the same thing. A spell is the bringing together of elements that don't have agency on their own, but when put in relation, magic happens. Spelling a word is the same as making magic. Separate letters interact, and meaning emerges.</p>
</div>


<div class="loop">
    <p><a href="#calibrate"><span id="uncalibrate">Start</span></a>.</p>
</div>

<img src="i-lost-my-training-of-thought-media/07.jpg">
<img src="i-lost-my-training-of-thought-media/08.jpg">
<img src="i-lost-my-training-of-thought-media/09.jpg">
<img src="i-lost-my-training-of-thought-media/10.jpg">

<div class="loop">
  <p>We are all witches.</p>
  <img src="i-lost-my-training-of-thought-media/11.jpg">
</div>

<div class="loop">
  <p>Relax before continuing the training.</p>
</div>

<div class="loop">
  <p><span id="bird">How to relate</span></p>
  <p>to write</p>
  <p>to sense</p>
  <p>that which is beyond me?</p>
</div>
<img class="scale" src="i-lost-my-training-of-thought-media/12.jpg">


<div class="loop">
  <p>Is the fear of death stronger than the desire to touch? → No → Keep your hands to yourself.</p>
  <p>↓</p>
  <p>Yes</p>
  <p>↓</p>
  <p><a href="#uncalibrate"><span id="calibrate">freeze</span></a> training.</p>
</div>

<div class="loop">
  <p>The <a href="#emerge"><span id="disappear">emergence</span></a> of a spell.</p>
</div>

<img class="scale" src="i-lost-my-training-of-thought-media/13.jpg">
<img class="scale" src="i-lost-my-training-of-thought-media/14.jpg">

<div class="loop">
  <p>How</p>
</div>
<div class="loop">
  <p>to</p>
</div>
<div class="loop">
  <p>take</p>
</div>
<div class="loop">
  <p>sides</p>
</div>
<div class="loop">
  <p>without</p>
</div>
<div class="loop">
  <p>relation?</p>
</div>

<div class="loop">
    <img src="i-lost-my-training-of-thought-media/15.jpg">
</div>
<div class="loop">
    <p>What <span id="what"><a href="#overlooked">overlooked</span></a>, non-participative, untimely lifeworlds are apart from me, but also a part of me?</p>
</div>
<div class="footer">
    Words: Adva Zakai, Femke Snelting<br/>
    Reading: Kathryn Yusoff, <em>Insensible Worlds: Postrelational Ethics, Indeterminacy and the (k)Nots of Relating</em>.<br/>
    Drawings: Isabella Aurora<br/>
    Picture: Arboretum, Domein Bokrijk (Genk)
</div>

<style>
  .loop {margin: 2em 0 2em 0;}
  img {width: 80%;}
  .scale {width: 60%}
  .small {margin-left: 40%;}
  .footer {font-size: 0.75em; margin-top: 40em;}
</style>
