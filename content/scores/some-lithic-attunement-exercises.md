title: Some lithic attunement exercises
authors: Maja Kuzmanovic & Nik Gaffney, FoAM
summary: As part of Alchorisma, Maja Kuzmanovic & Nik Gaffney of FoAM worked with the geological substrates of digital technology in a series of lithic attunement exercises. These exercises are an invitation to reestablish relationships with local geological entities and glacial timescales by algorithmically re-confusing binary distinctions between nature and culture, human and non-human, life and death.
license: FoAM (CC-Attribution-Share-Alike)

# Some lithic attunement exercises

![]({attach}some-lithic-attunement-exercises-media/image1.jpeg){.image-process-illustration--inline}

*<span class="alchemy-symbol">&#128851;</span> To begin with, find a small stone and hold it in your hand while you
read or listen.*

## Attuning to stones

When we talk about stones and rock, we talk about the thin planetary
crust of minerals. Some are formed from recycled bodies of plants,
animals and insects. Some are the Earth’s volcanic and sedimentary
regurgitations. Some are as old as the planet, others as old as the
stars that gave birth to it. While you might see yourself as a very
different being from the stone in your hand, minerals circulate through
your body, shape your bones and support your digestion. When you die,
you will return to the mineral realm. The organic dissipated into the
inorganic. On a geological timescale human bodies are as much in a state
of transition as wind, weather or lava. Flowing and temporary. We endure
continuous erosion and our inevitable entropy. Geology and biology,
geology and technology, our inner and outer topographies are
inextricably entangled.

Alchemists understood this entanglement as a working hypothesis. In
alchemy the states and shapes of matter can be transduced,
transsubstantiated. Minerals into knowledge, in the creation of the
Philosopher’s Stone, or the long, careful series of transformations
required to create a portable computer. On this mineral hardware,
mathematical models and operational algorithms metamorphose into mythic
signifiers with capacities to reshape cultures.

If we reconsider alchemy as a contemporary proposition, what could
become possible that isn’t otherwise? The alchemical union between
matter and spirit suggests different relationships between hardware and
software, between man-made and earth-grown technologies. In contrast to
contemporary geo-engineering, which tends to see itself as separate from
the world, an alchemist understands the world as a web of delicate
connections and convoluted relationships. In an alchemical worldview,
technology is less about control and more about negotiation. Alchemical
technologies are more akin to grains of sand gauging their place among
their neighbours, gradually settling in response to internal and
external pressures, and over time fusing into rock. A solidified pattern
of stone emerging from the noise of sand dunes.

According to Arabic geomancers, the masters of "science of the sand",
patterns are always present in the noise, one just needs attune to their
inherent dynamics. Geomancy reads the landscape from a relational point
of view, where the practitioner and their surroundings are inextricably
linked. As much as we sense the presence of a stone, the stone can be
subtly affected by our presence. This elemental scrying can uncover
latent possibilities for mutual influence. Geomancy does not offer
answers but rather suggestions for possible relationships between human
lives and an indifferent universe. An algorithmic pattern finding.

> "Through a process of attrition, nature has fashioned stones as strange offerings to the human imagination. With their landscapes and cloudscapes, and their nebulous writing. These figures in stone bind an arc between our proclivity for pattern recognition and our mythological imagination. They commune with the deep memory bank of human experience and expose this connection as a higher intention — a gift to us from the Earth itself."
>
>— Paul Prudence

![]({attach}some-lithic-attunement-exercises-media/image2.jpeg){.image-process-illustration--inline}

Rocks, stones and other lithic entities contain traces of charismatic
and algorithmic patterns, as they function, break down, or diverge from
human timescales. By attuning to these entities, you can directly engage
with the mineral substrate of digital technologies.

Attunement can be understood as a particular sensitivity characterised
by a careful, receptive, open awareness. It requires an active
observation and adjustment to entities or situations. It assumes a
willingness to be touched by external circumstances; to be lured,
affected and changed by them. A deliberately hesitant engagement with
other entities on their own terms, from quivering butterfly wings to
earth-shattering volcanoes. Attuning relies on the human capacity to
notice and participate in the asymmetrically reciprocal effects that
occur in direct experiences of the world. A kind of "ecological
intimacy".

Although the capacity for attunement is innate for some people, it can
also be developed, enhanced and refined through practice. In a state of
attunement, the world can be experienced teeming with life. It’s a
mutualistic or symbiotic state of being, one that accepts human
interdependence with the planet as a given. We are immersed and
implicated.

Attunement practices are common in animist cultures, and can be found
throughout human societies. There are some obvious resonances with
current philosophical tendencies toward speculative realism or the long
winding trails of panpsychism. Yet attunement is a concept that is often
difficult to grasp in words, especially words that make sense outside of
philosophical, ritual, performative or meditative discourses. It can
most certainly not be abstracted as a set of algorithms. It is exactly
this slippery quality of attunement that makes it interesting to work
with.

![]({attach}some-lithic-attunement-exercises-media/image3.jpeg){.image-process-illustration--inline}

> "Whether in the forest or in the clinic, noticing means a full sensory
engagement with the sounds, images, feeling and general atmosphere of an
encounter. It also means paying attention to, and trying to make sense
of, what is not typical, capturing what might at first seem to be
inconsequential details. These skills differ markedly from algorithmic
pattern recognition — something we ought to appreciate if we're not to
lose them, and if humans and machines are to collaborate in fruitful
ways."
>
>— [Anna Harris](https://psyche.co/ideas/what-might-mushroom-hunters-teach-the-doctors-of-tomorrow)

## Exercises

The following exercises are an invitation to attune to the lithic
entities in your surroundings. The emphasis is on the practice itself
rather than working toward a specific goal. The practice is comprised of
direct experience and reflection on the experience. You can repeat the
process as often as you like, in order to refine and deepen the
practice. The exercises were designed for small groups, however they are
also suitable for individual explorations.

![]({attach}some-lithic-attunement-exercises-media/image4.jpeg){.image-process-illustration--inline}

### Exercise: A pebble in my pocket

#### Duration: from hours to weeks.

*How does your relationship with a stone change when you keep it close
for an extended period?*

A simple exercise to observe how your relationship to a specific stone
changes over time. You don't necessarily need to do anything with the
stone, just be with it, notice and attune to its presence. Do this
exercise on your own. If you are engaging in this exercise with a group
of people, share your insights at the conclusion.

<span class="alchemy-symbol">&#128851;</span> Find a small pebble, stone, crystal or other mineral entity in your
surroundings. Observe the stone using all your senses. Take a few
minutes to notice any responses that might arise in your body or mind.
Does it feel appropriate to keep this stone with you for a while? If
not, find another stone and repeat the process.

<span class="alchemy-symbol">&#128851;</span> Decide how you are going to engage with this stone and for how long.
For example, you might hold the stone in your hand for a minute every
hour or once every day; meditate on its shapes and textures; put it in
your pocket and carry it around with you; place it on your desk or under
your pillow. Whatever you decide, the stone should remain in close
proximity to you for the duration of the exercise.

<span class="alchemy-symbol">&#128851;</span> Be with the stone. Notice if and how your response to the stone
shifts. You may periodically record your observations in a medium of
your choice if necessary.

<span class="alchemy-symbol">&#128851;</span> At the end of the chosen period, return to the place where you found
the stone. Hold it in your hand and observe, just like the beginning of
the exercise. Notice what emerges — sensations, thoughts, feelings,
impressions, etc. When you are ready, return the stone to the place
where you found it. Observe your reaction to this act of separation
between you and the stone, and the act of re-integration between the
stone and environment.

<span class="alchemy-symbol">&#128851;</span> Reflect on your experience. Did being with the stone change over time?
If so, how did your experience change? How did you relate to the stone
at the beginning, middle and end of the experience? How do you feel
about being more distant from the stone?

![]({attach}some-lithic-attunement-exercises-media/image5.jpeg){.image-process-illustration--inline}

### Exercise: Psychogeological drift

#### Duration: an hour or longer

*How do you experience your surroundings when guided by lithic
entities?*

The exercise is based on the psychogeographic drift, a form of
exploration of urban environments (usually) that emphasizes associative
drifting or *dérive.* In a psychogeographic drift you let yourself be
drawn by particular aspects of your surroundings, while observing how
they affect your emotional, embodied state of orientation or
disorientation. In this psychogeological meander, focus on the lithic
entities you encounter such as stones, rocks, bricks, or sand. Conduct
the exercise individually and in silence. You can record or document
your experience if you're so inclined.

<span class="alchemy-symbol">&#128851;</span> Look around you.

<span class="alchemy-symbol">&#128851;</span> When your attention is drawn to a particular lithic entity, slowly
walk towards it. Pause at a distance that feels appropriate and focus
all your senses on this entity. Notice their presence, their
phenomenological qualities, their patterns and relationships to their
surroundings. Notice what arises in you as a response -- sensations,
emotions, thoughts, ideas. Let these responses pass through you without
interpreting them. When you feel your attention beginning to drift, or
get pulled towards another charismatic stone or geological pattern, move
on. Repeat. Continue to drift for at least half an hour, or longer if
you prefer.

<span class="alchemy-symbol">&#128851;</span> Return to your point of origin and reflect on what you uncovered. What
did you experience? What did you notice? What do your surroundings look
like from the perspective of geological patterns? What algorithms could
produce these patterns? Note down your reflections. If you are doing
this as a collective exercise, share your individual insights within the
group.

![]({attach}some-lithic-attunement-exercises-media/image6.jpeg){.image-process-illustration--inline}

### Exercise: Written in stone

#### Duration: an hour or two.

*What do your surroundings look like from lithic and algorithmic
perspectives?*

You can conduct this exercise individually, or in groups of 3\~5 people.

<span class="alchemy-symbol">&#128851;</span> Describe your surroundings from a lithic point of view. What might a
building be like for bricks? a road for cobble stones? a monument for
marble? a beach for pebbles? How do the lithic entities relate to each
other, to humans and others in their surroundings? Imagine how the
surroundings appear from the vantage point of the lithic entities you
encounter.

<span class="alchemy-symbol">&#128851;</span> Translate your impressions into a tangible form. For example, an
illuminated story, a map, a lapidarium, a divination system for
geological pattern recognition or something else entirely.

<span class="alchemy-symbol">&#128851;</span> Find the algorithms. How would you abstract algorithms from the
descriptions in the previous steps? Describe one of these algorithms as
succinctly as possible. For example, as a list of instructions, a set of
constraints, a grammar, a group of operations or transformations.

<span class="alchemy-symbol">&#128851;</span> Apply a different algorithm. How might your lithic impression change
if another algorithm would be incorporated? If you are practising on
your own, create an additional algorithm. If you are doing this exercise
collectively, exchange algorithms between individuals or groups.

<span class="alchemy-symbol">&#128851;</span> Reflection. Discuss and note down your insights. What was easy to
describe algorithmically? What fell outside of the algorithmic approach?
How do your surroundings change from the lithic and algorithmic
perspective? How would you transpose these perspectives in different
contexts? How has your relationship to your surroundings changed when
seen from lithic and algorithmic perspectives?

![]({attach}some-lithic-attunement-exercises-media/image7.jpeg){.image-process-illustration--inline}

> "There was a word inside a stone.  
> I tried to pry it clear,  
> mallet and chisel, pick and gad,  
> until the stone was dropping blood,  
> but still I could not hear  
> the word the stone had said.  
> I threw it down beside the road  
> among a thousand stones  
> and as I turned away it cried  
> the word aloud within my ear  
> and the marrow of my bones  
> heard, and replied."  
>
> — Ursula Leguin

## Related reading

-   Amato, Joseph A. Dust: a history of the small and the invisible
-   Bjornerund, Marcia. Timefulness
-   Blohm, H., Beer, S. Suzuki, D. Pebbles to Computers: The Thread
-   Caillois, Roger. The Writing of Stones
-   Cohen, Jeffrey. Stone: An Ecology of the Inhuman
-   Calvino, Italo. Collection of sand
-   FoAM (eds.). Dust and Shadow Reader \#2
-   Harris, P.A. Turner, R., Nocek, A.J. Rock Records. In SubStance
    Volume 47, No 2
-   Harris, Anna. What might mushroom hunters teach the doctors of
    tomorrow?
-   Howse, Martin. Earthcode
-   Jemisin, N.K. The Broken Earth Trilogy
-   Leguin, Ursula. Deep in Admiration. In The Arts of Living on a
    Damaged Planet
-   MacFarlane, Robert. Underland
-   Morton, Tim. Attune. In Veer Ecology
-   Nova, Nicholas & Disnovation.org. A Bestiary of the Anthropocene
-   Prudence, Paul. Scholar's Rocks. In Reliquiae Vol 8 No 1
-   Ogden, James Gordon. The Kingdom of Dust
-   Thacker, Eugene. In the dust of this planet
-   Shepherd, Nan. The living mountain
-   SOLU: Field_Notes -- EOS 2018.
-   Sonic Acts. Living Earth
-   Sonic Acts. The Geologic Imagination
-   <https://www.flickr.com/photos/foam/albums/72157703407322714>


<style>
  @font-face {
    font-family: "Symbola";
    src: url("static/Symbola.otf");
  }

  article span.alchemy-symbol {
    font-family: "Symbola";
  }
</style>
