title: Execute
author: RYBN.ORG
summary: This paper describes the workshop ‘Execute’, an exercise on algorithms conducted by RYBN.ORG at Alchorisma’s first worksession at Z33 in Hasselt. It can be taken as a tutorial and be reproduced or re-enacted. In the context of the rise of algorithms as magical techno-solutions for every problem or situation, RYBN.ORG initiated a workshop that would give everyone the basics to understand the logic behind algorithms, their biases and their complex relation to the world.
license: fal

# Algorithm \| Execute

![]({attach}execute-media/image1.png){.image-process-illustration--inline}
:   Fig 1. Execute Workshop Flowchart, RYBN.ORG, 2021

## I. Foreword

In 2011 a TED Talk video narrated by Kevin Slavin and entitled *How
Algorithms Shape our World* went viral.[^1] The craze marked the start
of a wave of mainstream cultural interest in algorithms. Translated and
dubbed into many different languages, the 15-minute talk asserted that
any product, any service, any event and any desire of our modern world
is designed and shaped by algorithms.

At that precise moment, the word algorithm gained a 'magical aura':
algorithms became the *sesame* of platform capitalism, fuelled by some
kind of secret and mystical force with the potential to decipher and
predict our most intimate desires and behaviours – insights yet unknown
to our own selves.[^2] Algorithms rekindled the old cybernetic fantasy
of replacing humans at any position: algorithms were said to be ready to
outperform humans in the most complex cognitive tasks and to be able to
take the fairest decisions. Even if they remained secret and opaque --
*blackboxed* due to the commercial advantage they were supposed to offer
companies – algorithms were identified as the key ingredient that would
turn any start-up into a future unicorn platform.

Since long before the publication of the video, artists, activists,
thinkers and academics[^3] were discussing within the field of
algorithmic studies to enlarge, nuance and deepen our collective
understanding of algorithms. Nevertheless, the merciless mechanics of
social media created a dystopian imaginary, where algorithms, emptied of
their very substance, would embed passions, trigger investments and
embody the anxieties of our times towards technology.

In this context, we initiated the workshop *Algorithm \| Execute* to
offer a simple platform for anyone to understand the logic behind
algorithms, their biases and the complexity and ambivalence of their
relation to the world, not using any digital computer, but paper and pen
only.

This article describes the *Algorithm \| Execute* workshop procedure in
the form of a tutorial so that anyone can reproduce it or re-enact it.

## II. Workshop Description

The workshop is open to anyone, with or without prior knowledge of
algorithms and computing logic.

Participants are asked to write simple programmes on paper that will be
executed by one or several of the other participants.

The workshop progresses through the simplification of writing and
instruction sets as well as deeper into learning, using a small,
prepared vocabulary of operators and functions (recursive loops,
conditions, variables and functions, etc.).

By observing the execution of their own algorithms, participants can see
the gap between optimization logic at work and the real world as well as
all the biases infused through this gap.

Other participants may try to decipher the instructions of algorithms
written by others, by observing the execution, as a deduction game and
as an introduction to reverse-engineering practice.

The workshop, by only using pen and paper and the participants' bodies
as core elements, provides a platform for a more intimate understanding
of algorithms as well as a collective space for critique and discussion.

Participants can use any kind of notation, but the use of logical
flowcharts and universal indicators is encouraged.

Optional processes can be layered upon the core procedure to make it
more complex and more playful, such as: write two algorithms that
attempt to nullify each other (adversarial logic); change the
environment so as to incapacitate programmes, etc.

## III. Prerequisites

<div class="roman" markdown="1">
1. Pen and paper.
2. A large room full of objects, doors, windows, pieces of furniture.
3. Participants. They will change position throughout the workshop: algorithm 'designers', algorithm 'executors', 'observers'.
</div>

## IV. Workshop Introduction

Participants to the workshop are given several possible genealogies and
definitions of algorithms. The analogy with cooking recipes offers a
very mainstream activator for universal understanding.

A useful preliminary definition has been proposed by Gérard Berry[^4]:

> An algorithm is simply a way of describing in great detail how to do
something. It turns out that many mechanical actions, probably all of
them, are open to such a deconstruction. The ultimate goal is to take
the thinking out of the calculation, so it can be executed by any
digital machine.

When we advance to the execution phase of the workshop, the denial of
any subjectivity for the executors is constantly emphasized.

In 1954 Andrey Andreyevich Markov[^5] proposed another useful definition
of algorithms which we can build upon with the workshop participants:

> In mathematics, 'algorithm' is commonly understood to be an exact
prescription, defining a computational process, leading from various
initial data to the desired result. (…) The following three features
are characteristic of algorithms and determine their role in
mathematics:
>
> a\) the precision of the prescription, leaving no place to
arbitrariness, and its universal comprehensibility – the definiteness
of the algorithm;
>
> b\) the possibility of starting out with initial data, which may vary
within given limits – the generality of the algorithm;
>
> c\) the orientation of the algorithm toward obtaining some desired
result, which is indeed obtained in the end with proper initial data –
the conclusiveness of the algorithm.

These key elements – *definiteness*, *generality*, *conclusiveness* –
are the basic parameters on which we continually evaluate the
participants' instruction sets throughout the workshop.

## V. Workshop Steps

### Step 1 – A Simple Action

![]({attach}execute-media/image2.jpeg){.image-process-illustration--inline}
:   Fig 2. Execute Workshop Flowchart, step 1, RYBN.ORG, 2021

Instructions: 'write an action that has to be performed by one another'.

The action shall:

-   be feasible in time and space.
-   modify the surrounding space by acting upon any available element: tables, chairs, furniture or persons, for example.
-   not hurt anyone.

After the execution:

-   designer / observer: was the action well executed or not?
-   others: retro-engineer the instruction set.

### Step 2 – Discretization

![]({attach}execute-media/image3.jpeg){.image-process-illustration--inline}
:   Fig 3. Execute Workshop Flowchart, step 2, RYBN.ORG, 2021

Instructions: 'write an action that has to be performed by one another.
Deconstruct the action as much as possible in small steps'.

The action shall:

-   be feasible in time and space.
-   modify the surrounding space by acting upon any available element:
    tables, chairs, furniture or persons.
-   not hurt anyone.

Additional criteria:

-   each movement has to be discretized as much as possible.
-   erase any subjectivity of interpretation, leave no room for
    arbitrariness.
-   each action has to be written using the basic formal flowchart
    notation.

After the execution:

-   designer / observer: was the action well executed or not?
-   others: retro-engineer the instruction set.

### Step 3 – Conditions

![]({attach}execute-media/image4.jpeg){.image-process-illustration--inline}
:   Fig 4. Execute Workshop Flowchart, step 3, RYBN.ORG, 2021

Instructions: 'write an action that has to be performed by one another,
continue to discretize and simplify the writing. Add Boolean operators,
conditions and/or programming syntax elements'.

The action shall:

-   be feasible in time and space.
-   modify the surrounding space by acting upon any available element:
    tables, chairs, furniture or persons.
-   not hurt anyone.
-   each movement has to be discretized as much as possible.
-   erase any subjectivity of interpretation, leave no room for
    arbitrariness.
-   each action has to be written using the formal flowchart notation.

Additional instruction:

-   use programming semantics such as: conditions, variables, loops,
    functions.

After the execution:

-   designer / observer: was the action well executed or not?
-   others: retro-engineer the instruction.

## VI. Basic Semantics

The whole workshop process is defined by a succession of phases (based
on Markov's definitions), including: initial state; stimuli (something
that triggers an action); action; control (conditioning an event, an
execution, creating paths); transition (an infinite loop of actions
determined by an elementary state, also called deterministic automaton,
for example: open if close, close if open); final desire; final state.

The vocabulary available to describe these phases is inspired by the
basics of programming language, such as:

*Mathematical Operators*, which can be very helpful (+ - \> \< : \*) --
do not see these as emoticons; *Conditions* (if/else/then) to fork the
scheme; *Loops* (repeat/break, do while, copy, etc) very useful to
shorten the code; *Boolean Logic Operators* (not, and, or, true, false);
etc.

Before the start, a precious vocabulary of *arguments* (such as
*variable*, *list* and *set)* helps to declare all the involved objects
and subjects that are acting or interacting, and the parameters of all
those elements (remember the initial stage directions in theatre).

*Functions* (the name given to a reusable set of instructions) and
*Routines* can be very useful to encapsulate sub-actions, portions of
processes, instead of overloading the main script with too many details.
It also helps to describe, besides the main action, the behaviours (or
responsibilities) of an object (or subject, in our case); it
encapsulates single tasks that, combined later in the main chain of
instructions, will recompose a complex action.

One can also describe elementary states or composite states (complex
states combining several elementary states). *Environment Variables*,
for example, can be written to set an initial state of the environment
before executing the programme.

Of course this list is not exhaustive, and it is possible (and
recommended) to combine it with other kinds of *jargon*. The more you
combine families of symbols, the more the resulting diagram will look
like a Japanese garden.

## VII. Optional / Variations

<div class="roman" markdown="1">
1.   *Environment*: It is possible to change the environment settings
before each execution (change position of furniture, open/close windows,
lights on/off, etc). Having evolving surroundings at your disposal
raises the possibility of errors and underlines the fact that the
environment resists predictability, etc.

2.   *Adversarial Group Work*: algorithm designers work in groups; each
algorithm goes through a trial phase. Thanks to fine observation during
the observation phase, teams can try to disrupt the opposite side
algorithm, push it into infinite loop, or even DDoS[^6] it during the
execution phase.
</div>

## VIII. Workshop remarks

The workshop's simplicity of means allows people from any background to
take part and engage in a collective discussion on the pros and cons of
algorithms, usually reserved to experts.

The workshop is usually more interesting when infused with concrete
examples of working algorithms, such as: HFT trading algorithms[^7],
PredPol crime-predicting algorithms[^8], or any GAFAM and Silicon
Valley-related procedure.[^9] A meticulous dissection of their ways of
operating is highly recommended, as is encouraging participants to
relate their personal experiences with algorithms (e.g. CAPTCHAs, Amazon
recommendations, etc.).

Participants are usually impressed by the degree of precision needed to
write a simple action, for it to be correctly executed, and by the
infinite possibilities of misinterpretation, resulting in unexpected
bugs, errors or infinite loops. These errors magnify the inner
resistance of human beings and of the environment to dry instructions
and standardized procedures.

By providing analytical tools and observation methods, the workshop may
be a suitable approach to reverse-engineering techniques and offers
possible ways of self-protection against intrusive algorithms.

## IX. Conclusions

In 2015 the hype around algorithms faded a bit, and the mainstream
attention turned to AI and Deep Neural Networks, with the release of
*Google DeepDream*[^10] and the victory of *AlphaGo*[^11] against Lee
Se-dol.

In 2015 we developed another workshop, *Human Perceptron*[^12], which
can work in combination with the *Algorithm \| Execute* workshop to
expand the workshop focus to Neural Networks and AI inner mechanisms.

All the new critical points that emerged after 2015 – from
infrastructures, algorithmic biases or digital labour studies – can
continue to infuse the workshop and offer new developments.

The workshop is a stable platform and a friendly introduction for
critical discussions around computational logic. It remains flexible
enough for any further adaptations to any new critical entries.

## X. Workshop iterations

ALBA, Beirut, 2013.

IMPACT Festival, PACT, Essen, 2016.

Open Codes exhibition, ZKM, Karlsruhe, 2017.

ALCHORISMA, Z33, Hasselt, 2018.

Paris 8 University, St Denis, 2018.

ISI, Yogyakarta, 2019.

FRAC Alsace, Selestat, 2019.

## XI. References

-   Benque, David,
    [Almanac.computer](https://davidbenque.com/projects/almanac/), 2018
    
-   Cardon, Dominique, *À quoi rêvent les algorithmes*, Paris, Seuil, La
    République des idées, 2015

-   Casilli, Antonio, *En attendant les robots. Enquête sur le travail
    du clic*, Paris, Seuil, 2019

-   Chazard, Alexis, [*KOD
    (Kunst'O'Digit)*](http://incident.net/upgradeparis/economie0/atelier1.htm),
    Economie 0, Paris, 2008; see also the related pedagogical
    methodologies on programming and object-oriented modeling, ULM
    (Unified Modeling Language) and 'activity diagrams' also applied as
    managerial methodologies : 'Activity diagrams are graphical
    representations of workflows of step-wise activities and actions
    with support for choice, iteration and concurrency.' (Wikipedia)

-   Constant, Cqrrelations, Brussels, 2015
    <https://cqrrelations.constantvzw.org/0x0/>

-   Domingos, Pedro, *The Master Algorithm*, Basic Books, 2015

-   Laumonier, Alexandre, *6*, Zones Sensibles, Bruxelles, 2013

-   O'Neil, Cathy, *Weapons of Maths Destruction: How big data increases
    inequality and threatens democracy*, Crown Books, 2016

-   Mac Quillan, Dan, *Algorithmic State of Exception*, 2015

-   Rouvroy, Antoinette, 'Gouvernementalité algorithmique et
    perspectives d’émancipation'. Avec Thomas Berns, in *Réseaux*,
    2013/1 (n° 177)

-   RYBN.ORG, *ADM8*, 2011 [http://www.rybn.org](http://www.rybn.org/)

-   RYBN.ORG, *Algorithmic Trading Freak Show*, 2013
    [http://www.rybn.org](http://www.rybn.org/)

-   RYBN.ORG, *ADMXI*, 2015 [http://www.rybn.org](http://www.rybn.org/)

-   de Vries, Patricia, *Algorithmic Anxiety*, Amsterdam, INC, 2020

[^1]: *How Algorithms Shape Our World,* Kevin Slavin, 366 920 views, 25
    Nov 2012 <https://www.youtube.com/watch?v=ENWVRcMGDoU>; text
    transcript:
    <https://www.carrollfletcher.com/usr/library/documents/michael-najjar-texts/cf_michael-najjar_kevin-slavin_algorithms.pdf>

[^2]: *Amazon Patents 'Anticipatory' Shipping — To Start Sending Stuff
    Before You've Bought It,* Natasha Lomas, Jan 18, 2014
    <https://techcrunch.com/2014/01/18/amazon-pre-ships/>; or *How
    Target Figured Out A Teen Girl Was Pregnant Before Her Father Did*,
    Kashmir Hill, Feb 16, 2012
    <https://www.forbes.com/sites/kashmirhill/2012/02/16/how-target-figured-out-a-teen-girl-was-pregnant-before-her-father-did/>

[^3]: See, for example, the works and research of Antoinette Rouvroy,
    Dannah Boyd, Kate Crawford, Luciana Parisi, Matteo Pasquinelli,
    Alexandre Laumonnier, Cathy O'Neil, Dominique Cardon, Antonio
    Casilli, Patricia de Vries, David Benque, Constant and Algolit.

[^4]: Gérard Berry, *Penser, modéliser et maîtriser le calcul
    informatique*, coll. Leçons inaugurales du Collège de France, Paris,
    Collège de France / Fayard, 2009.

[^5]: A. A. Markov. *Theory of Algorithms*. Imprint Moscow, Academy of
    Sciences of the USSR, 1954. Original title: *Teoriya algerifmov*.

[^6]: Distributed Denial-of-Service attack: a cyber-attack from many
    different sources in which the perpetrators seek to make a machine
    or network resource unavailable to its users by temporarily or
    indefinitely disrupting services of a host connected to the
    Internet.

[^7]: RYBN's ADM X, Alrogithmic Trading Freakshow offers a panel of
    reference [http://www.rybn.org](http://www.rybn.org/) ; See also
    Nanex [http://www.nanext.net](http://www.nanext.net/) ; Or Sniper in
    Mahwah <https://sniperinmahwah.wordpress.com/>

[^8]: 'PredPol Digital dragnet : How data became a cop’s best weapon',
    Patrick MarshallNov 29, 2011
    <https://gcn.com/articles/2011/12/05/predictive-policing-tech-feature.aspx>
    ; first to experiment it, the city of Santa Cruz was also the first
    to ban the use of PredPol 'Santa Cruz becomes the first U.S. city to
    ban predictive policing', Kristi Sturgill, June 26, 2020
    <https://www.latimes.com/california/story/2020-06-26/santa-cruz-becomes-first-u-s-city-to-ban-predictive-policing>
    ; numerous critics have been emitted towards PredPol (systemic
    racism, etc.), see, for example, 'Technology Can't Predict Crime, It
    Can Only Weaponize Proximity to Policing', Matthew Guariglia,
    September 3, 2020, EFF,
    <https://www.eff.org/fr/deeplinks/2020/09/technology-cant-predict-crime-it-can-only-weaponize-proximity-policing>

[^9]: See the work of Antonio A. Casilli, Dominique Cardon, Cathy
    O'Neil, Kate Crawford, Vladan Jolen, among many others.

[^10]: *Inceptionism: Going Deeper into Neural Networks,* Google AI
    Blog, June 17, 2015, Alexander Mordvintsev, Christopher Olah and
    Mike Tyka
    <https://ai.googleblog.com/2015/06/inceptionism-going-deeper-into-neural.html>

[^11]: *Artificial Intelligence: Google’s AlphaGo beats Go master Lee
    Se-dol*, BBC News, 12 March 2016
    <https://www.bbc.com/news/technology-35785875>

[^12]: RYBN.ORG, Human Perceptron, 2015/2020
    <http://rybn.org/human_computers/humanperceptron.php>
