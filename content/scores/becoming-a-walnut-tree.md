title: Becoming a walnut tree
author: Malgorzata Zurada
summary: The following is a set of instructions on how to shapeshift into a walnut tree. It was exercised twice as a guided meditation by the participants of the Alchorisma summer camp in Beaulieu in August 2020. The exercise was devised as an attempt to gain insight into the body-mind state of the Grenoble walnut trees cultivated in the Isère region of France. The trees growing in the local orchards are grafted – they are hybrids of two walnut varieties: sturdy rootstocks of *Juglans regia L.*, the common walnut tree, and more fragile scions such as *La Franquette, La Mayette and La Parisienne*.

# Becoming a walnut tree

> This text is based on a shapeshifting exercise from *Visual Magick* by
> Jan Fries[^1] and was rewritten and adapted for the Alchorisma summer
> camp in Beaulieu, France from 23 to 29 August 2020.




Stand with your feet together, your hands hanging at your sides.

Take a couple of deep breaths, then relax and close your eyes.

Now slowly start to imagine that you are becoming a walnut tree.

Feel your body as a pillar, a single line, a solid stem. Layer upon
layer of solid wood.

Where there was your skin, you can now feel the bark. The bark covers
you from head to toe. It covers you completely and hides your face.

You have no face, no eyes, no ears, no nose, no mouth.

The bark completely surrounds and covers the core of your being, your
innermost self. It allows you to rest. You can rest deep within the
silence, within yourself.

Now slowly move your attention to your feet. Feel how they touch the
ground. Feel the roots growing out of your feet. Long roots that reach
down, deep down into the soil. They reach into the earth and through the
humus into the deeper realms. They meander through pebbles and stones
into the deep – and deeper still, clinging to solid rock, firmly
anchored, drinking the rich dark waters that flow deep below the
surface.

You can sense as these waters nourish you. Deep within you, you feel the
sap rising; it moves and circulates the nourishment in an upward
movement through your entire body.

Feel how the stem transports the juices.

With this upward movement you also slowly move your attention up on
high; you begin to sway as the tree sways. You can now feel the branches
as they fold out and grow from your shoulders and from your head. You
feel how they extend, reaching out, and reaching up to the sunlight.
Firm branches, elastic twigs and lush foliage. Leaves that bend and
dance and sway in the wind. You’re swaying under the wide sky, in the
cycles of nature. Feel the heaviness of walnuts – these are the most
precious parts of your being, they carry the information of your whole
lineage. You feel the juices transporting all the nourishment to the
walnuts. Feel how much of your life force is being invested in creating
and maintaining them.

<section class="right-aligned" markdown="1">
Now put your attention to the graft, the place of division, the witness
of your history. Can you feel it? How does it feel? Do you feel any
distinction or separation between the upper and lower part of your body,
or none at all?

How is life for you? Do you feel trapped? Or are you perhaps
comfortable? Taken care of? Do you feel any aches, or rather pleasure?
Do you feel safe?

Now pay attention to your community – the other walnuts standing in line
on your left, right, back and front. Become aware of them. They share
your history, they have similar experiences. Are they your companions,
or are they perhaps an extension of you? Feel the connection with
everything and everyone surrounding you.

Stay in this experience as long as you like.

When it starts to lose its freshness, take a couple of deep breaths and
slowly return to your usual self.
</section>

[^1]: Fries, Jan, *Visual Magick*, p. 139, Oxford: Mandrake of
    Oxford, 1992, ISBN 978-1-869928-57-5


<style>
  .right-aligned{
    text-align: right;
  }
</style>
