title: Secret Alcho
author: Annie Abrahams, RYBN.ORG
summary: Would it be possible to create an algorithm that includes human behaviour, that could evolve over time and in the process could integrate aspects of the personalities of all the participating individuals? During the Alchorisma residency in Z33 in Hasselt, Annie Abrahams and RYBN.ORG ran a test.
license: fal, Photos by Peter Westenberg

# Performing Secret Alchoritms

## Performing processes using algorithms

Would it be possible to create an algorithm that includes human
behaviour, that could evolve over time and in the process could
integrate aspects of the personalities of all the participating
individuals? During the Alchorisma residency in Z33 in Hasselt, Annie
Abrahams and RYBN.ORG ran a test.

![]({attach}secret-alcho-media/image1.jpeg){.image-process-illustration--inline}

They decided to try and develop what they called alchos (alchorisma
algorithms) based on German dancers Deufert and Plischke’s methodology
for *shared performance writing through transmission and reformulation*,
which in itself is an adaption of the game of
[consequences](https://en.wikipedia.org/wiki/Consequences_(game)) for the production of movement descriptions. (D&F used it for instance
in [Anarchiv\#2: second hand](https://deufertandplischke.net/anarchiv2-second-hand) (2012).

>"Our work processes are circular processes of continuously passing on
and transmitting materials without discussion. We clearly position these
practices as an aesthetic and political alternative to processes based
on discussion and consensus.' …* *'It is a way of working together
that is not based on consensus, but on passing on material, spending
time with it individually, making the necessary decisions with it,
transforming and reformulating it without the need of immediate
negotiation or justification. This is when a plurality of voices comes
into the work and personal competences for an activity can transform
into shared responsibilities for the material: handing over material,
taking it seriously, spending time with it, contributing to it."
— Deufert & Plischke

**RYBN.ORG and Annie started their alchos with a simple flow chart of an
iterative process. In the process they wanted to propagate a performance
from one person to the next.**

It took them a lot of time to optimize their ideas and they decided to
initiate two different tests. Alcho 1 and Alcho 2 had different
meta-instructions and were started in secret in order not to disturb the
other processes going on during the Alchorisma week. The initial
performance instructions were also different for both alcho-sets. This
made for variation, but in hindsight wasn’t such a good idea since it
made comparison impossible.

#### error error error

## Alcho 1:

### Variables:

<div class="variables" markdown="1">
* meta-instructions printed
* instructions on a piece a paper (**never to be talked about**)
* a virgin piece of paper
* a pen
* stone(s)
</div>

### Meta-instructions written on a piece of paper:

<div class="instructions" markdown="1">
* I read the meta-instructions (on this paper).
* I (a) read the instructions to perform.
* I keep these instructions secret -- I don’t discuss the instructions in any way, nor my intentions during the execution
* I find someone (b).
* I ask (b) to watch me attentively.
* I perform these instructions.
* I ask (b) to write down what she/he saw as a new set of instructions.
* I ask (b) to pass the new set of instructions + these meta instructions to another person (a').

\* If it is difficult to find a new person immediately, try again.
</div>

## Alcho 2:

### Variables:

<div class="variables" markdown="1">
* meta-instructions printed
* instructions on a piece a paper (**never to be talked about**)
* a virgin piece of paper
* a pen
* stone(s)
</div>

### Meta-instructions written on a piece of paper:

**Recto**:
<div class="instructions" markdown="1">
* Watch my performance
* Write it down as an instruction set
* Find someone else:
* Give him/her your instruction set
* \+ this paper and ask him/her to read the verso
</div>

**Verso**:
<div class="instructions" markdown="1">
* Read my instruction set, and keep it secret
* Find someone alone:
* Give him/her this paper and ask him/her to read the recto
* Perform my instruction set
</div>

Here are the two initial performance instruction sets used during the
test.

![]({attach}secret-alcho-media/image2.jpeg){.image-process-illustration--inline}

After starting the process, RYBN.ORG and Annie quickly lost track of
what was happening.
(no cookies, no trackers, no surveillance system)

#### error error error

*'It was also shocking and amazing that in our happiness at succeeding
in making something that could work, we had quite naturally left out
essential relational and qualitative elements.'*

**Left out**: The intention to have the performance transformed through the
subjectivity and interpretation of the writer AND the performer.
Care for the conditions of the transmission.

**Lesson**: before implementing anything, always check what is left out.

<h3 class="line-through" markdown="1">Optimization *has pitfalls.*</h3>

A new test has to be set up:

![]({attach}secret-alcho-media/image3.jpeg){.image-process-illustration--inline}

## Alcho V3 (beta)

### Variables:

<div class="variables" markdown="1">
* meta-instructions printed
* instructions set on a piece a paper (**never to be talked about**)
* a virgin piece of paper
* a pen
* stone(s)
</div>

### Meta-instructions written on a piece of paper:

**Recto**:

<div class="instructions" markdown="1">
* Watch the performance attentively
    * Discover/determine what is **essential** for you in what you saw\
* Write it down as an instruction set
    * a translation of \[what you saw\] to \[what you **want** others to **experience** when she/he executes it\]
* Find someone else
* Give him/her your instruction set + this paper and ask him/her to read the verso
</div>

**Verso**:
<div class="instructions" markdown="1">
* Read the instructions, and keep them secret
    * Discover what you think is **essential** in this instruction set
* Find someone alone
* Find a quiet place together
* Give him/her this paper and ask him/her to read the recto
* Perform the instruction set
    * Follow the instructions with attention for what you think is **essential**
</div>

### Possible performance instructions:

<div class="framed" markdown="1">
* go somewhere you have space to move
* stand relaxed on both feet
* after some time, close your eyes
* think of a particular object/entity you like
* imagine dancing with this object
* dance till satisfied
* open your eyes
</div>

What did we forget this time?

![]({attach}secret-alcho-media/image4.jpeg){.image-process-illustration--inline}

![]({attach}secret-alcho-media/image5.jpeg){.image-process-illustration--inline}
:   Performing different interpretations of Alcho1 and Alcho2
by its participants

<style>
  .line-through{
    text-decoration: line-through;
  }

  :is(.instructions, .variables){
    font-family: monospace;
  }
  :is(.instructions, .variables) ul{
    margin-top: 0;
  }
  .instructions li{
    list-style: none;
  }
  .instructions li li{
    font-style: italic;
  }
  .framed{
    border: calc(var(--vine-width-thin) / 2) dotted var(--vine-color);
    padding: 0 var(--pad);
    margin: var(--em-space) 0;
  }

</style>
