title: Exile
author: Isabella Aurora
summary: The practice of grafting observed extensively in the walnut trees surrounding the Beaulieu estate was the starting point for an online meditation script sewn together by the images of scar tissue in the bodies of participants of the Alchorisma summer camp. Tree and human bodies alike are here seen through the borderlines within their flesh that connect memory of times past and times to come.
license: fal

<section class="texture" markdown="1">

  <div id="invert-scroll" markdown="1">

  ![tree texture]({attach}Exile-media/texture/texture6.JPG){.image-process-illustration--visuals}

  ![tree texture]({attach}Exile-media/texture/texture7.JPG){.image-process-illustration--visuals}
  
  ![tree texture]({attach}Exile-media/texture/texture4.JPG){.image-process-illustration--visuals}

  ![tree texture]({attach}Exile-media/texture/texture2.JPG){.image-process-illustration--visuals}

  <!-- ![rock and wood texture]({attach}Exile-media/texture/texture6.JPG){.image-process-illustration--visuals}

  ![rock texture]({attach}Exile-media/texture/texture7.JPG){.image-process-illustration--visuals}

  ![wood texture]({attach}Exile-media/texture/texture8.JPG){.image-process-illustration--visuals} -->

  </div>

</section>

<section class="exile scroll-on-click" markdown="1">

![grafted tree]({attach}Exile-media/exile/Exil2.JPG){.image-process-illustration--visuals}

![human scar]({attach}Exile-media/exile/Exil5.jpg){.image-process-illustration--visuals}

![human scar]({attach}Exile-media/exile/Exil6.jpg){.image-process-illustration--visuals}

<!-- ![grafted tree]({attach}Exile-media/exile/Exil7.jpg){.image-process-illustration--visuals} -->

![grafted tree]({attach}Exile-media/exile/Exil8.jpg){.image-process-illustration--visuals}

![human scar]({attach}Exile-media/exile/Exil9.jpg){.image-process-illustration--visuals}

![human scar]({attach}Exile-media/exile/Exil10.jpg){.image-process-illustration--visuals}

<!-- ![grafted tree]({attach}Exile-media/exile/Exil11.jpg){.image-process-illustration--visuals} -->

![grafted tree]({attach}Exile-media/exile/Exil12.JPG){.image-process-illustration--visuals}

![human scar]({attach}Exile-media/exile/Exil13.jpg){.image-process-illustration--visuals}

![human scar]({attach}Exile-media/exile/Exil14.jpg){.image-process-illustration--visuals}

<!-- ![grafted tree]({attach}Exile-media/exile/Exil15.jpg){.image-process-illustration--visuals} -->

![tree hole]({attach}Exile-media/exile/pbn.jpg){.image-process-illustration--visuals}

</section>


<script>
  $(window).on("scroll", function() {

    let screenHeight = $(window).height();

    // the global scroll
    let currentScroll = $(window).scrollTop();
    let ExileHeight = $(document).height() - screenHeight;

    let percentScroll = currentScroll/ExileHeight;
    console.log("percentScroll", percentScroll);

    // the texture opacity mouvement
    // let exileImagesNumber = $("section.exile").children().length;
    let textureImagesNumber = $("section.texture").children().first().children().length;

    let currentTextureImage =  percentScroll * textureImagesNumber;
    console.log("currentTextureImage", currentTextureImage);

    let toTranslate = Math.floor(currentTextureImage) * screenHeight;
    let toOpacify = ((Math.abs((currentTextureImage % 1) - 0.5)*-1)+0.5)*2;

    $("#invert-scroll").css("transform", "translate3d(0, "+toTranslate+"px, 0)");
    $("#invert-scroll").css("opacity", toOpacify);

  });
</script>
