title: Alchorismyxias
author: Peter Westenberg
summary: 'Alchorismyxias' are digital collages of the photo documentation of the worksession at Z33. I made them in an attempt to let new meanings emerge. Making -myxias has an anti-asphyxia effect, it is a remedy for absoluteness-anxiety, my outlet for distrust against belief in efficiency- and solution-oriented technological charisma. Human fiddling, cutting and pasting, shifting and trying without a direct goal. The result: something that doesn’t have to exist, but that brings a dose of valuable visual undefinedness into the world. 'Alchorismyxias' are visual formulas, spillings, cantings and question marks. They are called and spell. Mineralmatter, Swarmal, Ritual activist, Vogue operators, Mauro machine, Gestendings. They cast and name. Turn plane polarized, Kompelator, Topospectre. They merge, ripple, disrupt, filter. Rythmiks, Kapitaeel, Green powder monkeys. They are mixings, mikings, blends, moksies that erode, extract and interpolate.
License: fal

<section class="exile scroll-on-click" markdown="1">

![mineralmatter]({attach}Alchorismyxias-media/01-mineralmatter.jpeg){.image-process-illustration--visuals}

![the mauro machine]({attach}Alchorismyxias-media/02-the mauro machine.jpeg){.image-process-illustration--visuals}

![gestendings]({attach}Alchorismyxias-media/03-gestendings.jpeg){.image-process-illustration--visuals}

![vogue operators]({attach}Alchorismyxias-media/04-vogue_operators.jpeg){.image-process-illustration--visuals}

![turn,plane,polarized]({attach}Alchorismyxias-media/05-turn,plane,polarized.jpeg){.image-process-illustration--visuals}

![kompelator]({attach}Alchorismyxias-media/06-kompelator.jpeg){.image-process-illustration--visuals}

![rituactivist]({attach}Alchorismyxias-media/07-rituactivist.jpeg){.image-process-illustration--visuals}

![swarmal]({attach}Alchorismyxias-media/08-swarmal.jpeg){.image-process-illustration--visuals}

![qreenpowdermonkeys]({attach}Alchorismyxias-media/09-qreenpowdermonkeys.jpeg){.image-process-illustration--visuals}

![topospectre]({attach}Alchorismyxias-media/10-topospectre.jpeg){.image-process-illustration--visuals}

![rythmiks]({attach}Alchorismyxias-media/11-rythmiks.jpeg){.image-process-illustration--visuals}

![kapiteel]({attach}Alchorismyxias-media/12-kapiteel.jpeg){.image-process-illustration--visuals}

</section>
