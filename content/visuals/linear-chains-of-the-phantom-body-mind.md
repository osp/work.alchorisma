title: Linear chains of the phantom body-mind
Author: Malgorzata Zurada
summary: Tracing the invisible presences subjectively observed during the Alchorisma worksession in Beaulieu. Mapping the energy flow with the use of diagrams, symbols and notes. Quantification of prana in entities, thought-forms, objects, and locations. Things that are no more, but their energetic imprint is still present. Phantom ontologies and ghost trees, grafted entities with dual-energy matrix. Human and other-than-human apparitions and etheric doubles. Intangible structures, invisible codes, subtle anatomies of entities and their habitats. Ghostly algorithms, orphaned data, lingering syntaxes; traces left by the programmer. Mapping the energetic pathways, body-mind reflex chains and cause and effect relations. Detecting vibrational residues, recognizing transfers between the physical and non-physical. The energy body, the subtle body, the etheric body, the phantom body, body without organs.
License: © Malgorzata Zurada

<section class="exile scroll-on-click" markdown="1">

![altar for the past present and future]({attach}linear-chains-of-the-phantom-body-mind-media/altar_for_the_past_present_and_future.gif){.image-process-illustration--visuals}

![etheric doubles]({attach}linear-chains-of-the-phantom-body-mind-media/etheric_doubles.jpg){.image-process-illustration--visuals}

![night shift]({attach}linear-chains-of-the-phantom-body-mind-media/night_shift.jpg){.image-process-illustration--visuals}

![ontology of vitality]({attach}linear-chains-of-the-phantom-body-mind-media/ontology_of_vitality.gif){.image-process-illustration--visuals}

![phantom tree]({attach}linear-chains-of-the-phantom-body-mind-media/phantom_tree.gif){.image-process-illustration--visuals}

![terraforming]({attach}linear-chains-of-the-phantom-body-mind-media/terraforming.gif){.image-process-illustration--visuals}

</section>
