title: about
authors: [Z33, House for Contemporary Art](https://www.z33.be/), [Constant](https://constantvzw.org/)

This platform emerged out of the collective energies during two
Alchorisma worksessions organised by <a href="https://www.z33.be/" target="_blank">Z33</a> and <a href="https://constantvzw.org/" target="_blank">Constant</a>. It
gathers ideas, tests and prototypes developed during the
worksessions. Worksessions are Constant's intensive
trans-disciplinary situations to which participants from all over the
world contribute. They function as a temporary research lab; a
collective working environment where different types of expertise come
into contact with each other. During work sessions we develop ideas and
prototypes. The first worksession took place from *2 till 8 December 2018*
at *Z33 in Hasselt, Belgium*. The second worksession was organised from
*24 to 29 August 2020* in the family house of Anne-Laure Buisson
in *Beaulieu, Vercors, France*.
