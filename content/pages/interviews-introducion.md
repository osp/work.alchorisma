title: Interviews introduction
authors: Isabella Aurora, Malgorzata Zurada, An Mertens

We notice a rising practise of shamanism, magic and spirituality in the
West since the late nineties. Moreover, terms such as technomagic,
technoshamanism and technopaganism have been surfacing to designate a
rising movement between art, spirituality and technology and still
create confusion to this day. But the intertwining of the notions of
“technology” and “magic(s)” could be seen as evidential when one
operates spiritual practice as a technique to be studied, or when one
faces the many ghosts animating technology.

This compilation of interviews engages different practitioners from
Brazil, Belgium and Lithuania across multiple fields whose work is
embedded within this subject to create a composition of points-of-view
in relation to this movement.
