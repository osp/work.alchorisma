title: definition
authors: [Z33, House for Contemporary Art](https://www.z33.be/), [Constant](https://constantvzw.org/)

Alchorisma alludes to the relationships between algorithms, charisma,
rhythm, alchemy and karma. It looks at integrating cosmogenetic views
with the charisma surrounding technology, at ways to infect existing
algorithmic models with ideologies that acknowledge the importance of
co-existence with non-human entities.
