title: pathways
authors: OSP

## Pathways

Visiting this website is like wandering in a forest, slowly discovering its many paths.

Algorithms have read the contents of this publication and have discovered thematic pathways within the content.
These pathways are marked as colored links in the site. The pathways allow you to jump to a different place in the 
publication.

<ul>
  <li><div class="essays category">essays</div></li>
  <li><div class="interviews category">interviews</div></li>
  <li><div class="dreams-fictions category">dreams-fictions</div></li>
  <li><div class="scores category">scores</div></li>
  <li><div class="code-projects category">code-projects</div></li>
  <li><div class="visuals category">visuals</div></li>
</ul>

If ever you get lost ‘Points of Interest’ is a great start to find your way back.

Only a part of the pathways is shown: after clearing your cache different pathways will be revealed, taking you on a different walk.