title: definition short
authors: [Z33, House for Contemporary Art](https://www.z33.be/), [Constant](https://constantvzw.org/)

Alchorisma alludes to the relationships between algorithms, charisma,
rhythm, alchemy and karma.