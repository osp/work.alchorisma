Title: 🄸🄽🄲🄰🄽🅃🄰🅃🄸🄾🄽🅂 - 🅃🄰🄻🄴🅂 - 🅂🄿🄴🄻🄻🅂 🄰🄽🄳 🅁🄴🄼🄴🄳🄸🄴🅂 🄵🄾🅁 🄳🄰🅃🄰🄱🄰🅂🄴 🄰🄽🅇🄸🄴🅃🄸🄴🅂
Author: Anne-Laure Buisson, Axel Meunier, Isabella Aurora, Vesna Manojlovic, Sumugan Sivanesan
Summary: A spiral of concerns about representing faithfully or addressing respectfully the world out there, the world beneath and the world within. Knowledge retrieved from the computer. Database queried to access numbers. Numbers entered in the database. Trees identified by numbers. Labels nailed onto trees.
license: fal

# 🄸🄽🄲🄰🄽🅃🄰🅃🄸🄾🄽🅂 - 🅃🄰🄻🄴🅂 - 🅂🄿🄴🄻🄻🅂 🄰🄽🄳 🅁🄴🄼🄴🄳🄸🄴🅂 🄵🄾🅁 🄳🄰🅃🄰🄱🄰🅂🄴 🄰🄽🅇🄸🄴🅃🄸🄴🅂

## OPENING

Labels nailed onto trees.

Trees identified by numbers in the green collection.

Numbers entered in the database.

Database queried to access numbers.

Knowledge retrieved from the computer.

Illness of the green collection manager.

Papers accumulating in red boxes in the office. Illness infecting the
database as threatening blanks blurring the aliveness and deathness of
trees unaccounted for.

alive

alive: excellent condition

alive: good condition

alive: fair condition

alive: poor condition

alive: questionable status

alive: indistinguishable mass

dead

dead: deaccessioned/removed

dead: unable to locate

dead: deaccessioned/removed

dead

alive: indistinguishable mass

alive: questionable status

alive: poor condition

alive: fair condition

alive: good condition

alive: excellent condition

alive

Life

Domain

Kingdom

Phylum

Class

Order

Family

Genus

Species

Subspecies

Hybrids

Taxonomy. Nomenclature.

Names to control them.

Code. Changing code. New code.

A vibration. A fleeting feeling of discomfort.

Uncertainty.

About the health of the database.

About the health of the collection.

About the health of the green collection manager.

About the sameness of the calculated world and the experienced world.

A database anxiety.

That radiates from the green collection manager's computer and passes
through each of us.

That only the rain can cure when the office of the green collection
manager becomes a shelter.

Speaking the words of the database. Questions. Answers. Questions.
Answers. Questions. Repeat. Embodying the database. The database in one
computer in the office. Knowledge from botany, taxonomy, books, delivery
manifests, other places and other worlds, condensed in the office.
Humans repeating the words of the database.

‘A worldwide social engineering apparatus penetrating every community’,
as Suzanne Treister's ironic tarot card told me in answer to my
question.

Database anxiety as an unbalance.

Repair in the form of an IF, an algorithmic function and an ontological
speculation.

Which entities compose the community?

Are unaccounted for in the database?

A spiral of concerns about representing faithfully or addressing
respectfully the world out there, the world beneath and the world
within.

Knowledge retrieved from the computer.

Database queried to access numbers.

Numbers entered in the database.

Trees identified by numbers.

Labels nailed onto trees.

## THE BLIND BOTANIST

Yeah, I met the developer at the beginning of this year, and he’s just
very — because he’s almost blind — so he just knows. He was just like:
‘OK, just go to that field’ — he was talking to the person with the
\[indecipherable\]. ‘Just go to that field’, because he knew in his head
how it all worked. But he – yeah – at this point he’s becoming — he only
had — he says: ‘I’m starting to fall downstairs’ and stuff like that. So
he’s just — but because he couldn’t see anymore, started losing his
sight, he said: ‘I want to do something else for botany than what I
would like to do’ — which is working with microscopes and working with
flowers and very in-depth. And so, that’s why he changed to software
development. For – to try and collect data for botanical gardens, to be
sure that there is continuity and longevity in the data in those
collections. So that’s, yeah.

That’s why he developed that?

That’s why he started, or that’s why he started working in databases,
for databases, because there are a lot of different databases that you
can use for plant collection requirements, but this one is just so
extensive. And the next one, the next update will be even more extensive
because, as I said, you have new regulations about the transfer of wild
materials so you need certain certificates that you have to be able to
sign. The people of the country of origin have to sign it because – by
signing it they say that if you propagate it and get a profit from it,
we don’t want any part of that profit. So, in the new software you will
also be able to produce these types of certificates. Also, if you are
exchanging material between gardens, for example, if the material
crosses borders you will have to take, for example — and also add that
information and certificates and everything. And so, those transfer
agreements, everything will be important in that.

## INTENTION-PROPOSAL Market universalism

‘Market Fundamentalism’ is an overarching and universalizing condition
in which efforts to remove obstacles to commodification and the movement
of capital, to open up goods and services to markets and the development
of financial products are accepted as being ‘good’. It is conceivable
that all forms of life can somehow be fashioned into financial products
and traded on markets, but value predominantly returns to an elite few.
What are the implications for forms of life that hold different points
of view and practise alternative methods of ‘worlding’? Under these
conditions, is it possible to simply ‘keep the market at bay’?

## Risky business

One proposal is that such groups might be able to develop ‘New Economic
Spaces’ (NES) on blockchain-secured platforms such as Ethereum.
Communities would effectively develop financial products based on their
lifestyles and beliefs, issue coins and trade, ultimately returning
value to themselves and the groups they are in solidarity with.

Within ‘Rights of Nature’ discourses, there has been a discussion about
how Indigenous people living ‘on Country’ are helping to slow down
global warming. Hypothetically, this ‘contribution’ to managing climate
change could be financed and traded (for example, as an offset).
Nevertheless, this is a risky business given that such forms of life
would significantly increase their exposure to market fluctuations and
in particular to takeovers, ‘devaluation’ and the biases of the
investors who currently dominate markets. This emphasizes the need for
strategic cultural programmes to accompany such developments in NES and
‘prepare’ potential investors to act in accordance with, for example,
Indigenous interests.

## Code(x)

The proposal is to develop a suite of protocols and a syntax — an
algorithmic book of spells — that can be used to support alternative
modes of living via the proliferation of ‘spirits’. These programmes
facilitate the proliferation of specific desirable entities by
fashioning those that are subject to their algorithmic processes into
‘vessels’, suitable for incorporation.

![]({attach}database-anxieties-media/image1.jpeg){.image-process-illustration--inline}

## DATASCIENTIST’S REMEDY I

-   add the field private / public in the first page of encoding of the
    new items (default = public)
-   go on with the encoding (back log + excel files)
-   check private / public status of the old plants, and correct the one
    that should not be published
-   add a query to insert new data in batch in the database (from an
    excel file or CSV, in a given structure)
-   add a query to export the data directly in CSV, with a clean layout,
    and the referential
-   publish the public data on the website (with a not too precise
    location )
-   change the licence of the software
-   create reporting, for example with the number of new
    occurrences/taxa between two dates
-   then analyse data :
    -   mapping of species / places
    -   exploratory analysis of data

## DATASCIENTIST’S REMEDY II

Go in the garden

Look at the trees

What aspects of the trees are missing in the database (add pictures, add
locations, add smells)

Make a graveyard zone in the database, with all the dead trees and their
obituaries

Find a way for the trees to access the database (what does a tree know
about the place where it is? its surroundings?)

Find a way for the database to access the trees and exchange information
with them (when they change status, when there is a mistake in the
encoded information)

How to include the forest in the collection (delegation of trees?)

How to allow the trees to change place in the domain, via the database?

Via a RfC? (Request for Change)

Add comments from people and messages to a particular tree in the
database, via the website (form)

Write the GDPR of the trees

Help a tree find the address of another tree via the database? (if they
want to keep in touch with old friends from the nursery, for example)

## MAGIC SPELL AGAINST AROUSAL

Too much passion may cause anger and anxiety in the person who is
working the spell, or else an unbalanced sexual desire may prevent the
sorcerer (or sorceress) from winning over the beloved´s heart. Will is
at the root of magic, but many times we jeopardize our chances by
cultivating an unconscious fear of achieving our goals. The image that
came to me is that when one works a love spell, one should keep
unbalanced sexual desire, impatience and anger at bay as much as
possible. It is necessary to keep the flame of love burning steady,
happy and free instead of fearful of success or failure.

Arousal is coming upon me like a wild bull,

It keeps springing at me like a dog,

Like a lion it is fierce in the coming

Like a wolf it is full of fury

Stay! I pass over you like a threshold, I walk right through you like a
flimsy door, I span you like a doorway.

I turn back your approach like a hobble, I drive out the fieriness of
your heart.

αe  
∂ ⚘ ◊ + Æ { DB, T, H, Tr } = ∫ (DB +T) a H  
Tr ♞ © + ↻♥ (H)  
∫ = \^ + ∫∫ Tr Ω (H) + Tr a H  
αK

## THE OLDEST RECORDED TREE IN THE WORLD

Someone was doing his PhD or research on that — and he just went — he
bought a brand-new corer to try and count the rings. And he went to the
park with the forest guard and everything, and he tried to take a core
and his corer got stuck, which can happen because there is a different
type of tension in the trees and everything. And he asked the forest
guard: ‘OK, my corer got stuck, but what do I do?’ And the guard said:
‘We have a lot of those trees here, just cut it down. It’s no problem,
you can just take — take the part of the tree and you can count it in
the lab.’ And he took it with him and already on his first day of
counting, he was already — it was already older than the oldest recorded
tree in the world. So he found the oldest tree in the world, but he cut
it down. So it’s actually a really tragic story. I don’t know what
happened to his PhD afterwards, but it is just one of those stories that
you never forget, because it would just be horrible if that ever
happened to me.

## CLOSING

It was a pleasure to be in the domain you are so lovingly taking care
of!

Thank you for making an effort to welcome us to share both the gardens &
forests with us and the database!

Celebrate and promote the knowledge stored in your data.

Share it with the hackers, software developers and datascientists.

May they too help you in your struggles by either giving you advice,
volunteering to write or change some code, analysing the data. I would
like to hear what other ways people can contribute and how can we help
each other through more cooperation.
