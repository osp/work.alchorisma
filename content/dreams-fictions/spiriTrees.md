Title: spiriTrees
author: Annie Abrahams
summary: spiriTrees is a short text on trees and spirits by Annie Abrahams that was written in the Irish Tree Alphabet. The Alphabet was developed by Irish artist Katie Holten in a project that explores language ecosystems and the importance of our words and the stories that we tell. You can download the font and write your own text in the Irish Tree Alphabet here <http://treealphabet.ie>.
License: <a href="http://treealphabet.ie">Irish Tree Alphabet</a> by Katie Holten

a spirit is not a thing, nor is it of the imagination, it’s all encompassing, everywhere, it relates things, trees, humans, stones and whatever … it is a consciousness of entanglement, of interdependence – if you experience it, everything becomes related – it is a mystical feeling and I, Annie, can call this feeling by touching beautiful old “dying” trees.
{: .treeified }

a spirit is not a thing, nor is it of the imagination, it’s all encompassing, everywhere, it relates things, trees, humans, stones and whatever … it is a consciousness of entanglement, of interdependence – if you experience it, everything becomes related – it is a mystical feeling and I, Annie, can call this feeling by touching beautiful old “dying” trees.

<style>
  @font-face {
    font-family: "Irish Tree Alphabet";
    src: url("static/itav6.ttf") format("truetype");
  }

  article .treeified {
    font-family: "Irish Tree Alphabet";
    font-size: 11vh;
    line-height: 11vh;
  }
</style>
