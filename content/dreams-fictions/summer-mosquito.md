Title: 3 a.m summer mosquito
Author: Elodie Mugrefya
Summary: ‘3 a.m. summer mosquito’ is a conversation between two unidentified human beings, or maybe just one. They wonder whether they should dismiss altogether the notion of ‘worthy life’ and if so, how to escape the human-made hierarchies determining who/what deserves to be saved and protected and who/what doesn’t.

# 3 a.m summer mosquito

What follows is a conversation between two unidentified human beings, or
maybe just one. They wonder whether they should dismiss altogether the
notion of ‘worthy life’ and if so, how to escape the human-made
hierarchies determining who/what deserves to be saved and protected and
who/what doesn’t. The being(s) having this discussion understand(s) that
the systems and processes constructed in relation to these hierarchies
(such as genus classification or human nations delimitation apparatuses)
interest only a very limited strand of the earth’s population. And that
this limited strand seems, on the one hand, to have a hard time coping
with the impossibility to discipline and predict earthly beings, and on
the other, to struggle with the paradox of discarding hierarchies and
notions of worth while leading an existence based on violence and
subjugation. An existence that needs these hierarchies so to make its
brutality not only justifiable but also bearable.

I can not sleep.
{: .left }

I understand.
{: .right}

It is unnerving.
{: .left }

I know.
{: .right}

I want to kill it.
{: .left }

You know it’s a she, right?
{: .right}

Yeah… I know. But saying “I want to kill her” feels a bit too much.
{: .left }

But why? That’s what you want to do, isn’t?
{: .right}

C’mon…
{: .left }

Sorry.
{: .right}

It’s not like it’s just me you know.
{: .left }

Hmm hum.
{: .right}

There’s people dying.
{: .left }

Yes, it is daunting.
{: .right}

Children dying even!
{: .left }

Children are people.
{: .right}

Yeah yeah, you know what I mean.
{: .left }

You think this particular one is the one killing the children?
{: .right}

Of course not, but it’s not like she…or they would be missed.
{: .left }

Well, you don’t know that. I reckon some guppy[^1] would fight you on this.
{: .right}

Okay, maybe depending on who, but I wouldn’t miss them
{: .left }

I wouldn’t be sure of that either. I know an Hispaniolan crossbill[^2] that could tell you quite a story; at least for now.
{: .right}

…
{: .left }

But it’s not only about sleep is it? There’s something else that is bothering you. That is bothering us. Right?
{: .right}

Yes. The audacity.
{: .left }

Meaning?
{: .right}

The irreverence for boundaries.
{: .left }

Oh…Yes. They do tend not to mind our boundaries.
{: .right}

Nationalised fleshes and expensive repellents produced in shiny laboratories.
{: .left }

Yes
{: .right}

All kinds of papers that don’t matter.
{: .left }

Maybe this is why we make up other very serious papers.
{: .right}

One says some are Culex[^3]
{: .left }

And another says some are Aedes[^3]
{: .right}

I guess it reassures us. But of course that makes no difference to them.
{: .left }

Many things made the difference over time though
{: .right}

The so many cattles
{: .left }

The tires
{: .left }

The slave ships
{: .right}

The increasing temperatures everywhere [^4]
{: .left }

The increasing temperatures
{: .right}

I  
wonder… Guilty as carrier or guilty as a doer?
{: .left }

The emergence there or here[^5]
{: .right}

Ouch.
{: .left }

‘Guilty’… as in conscious and informed?
{: .right}

But what about the 750 000 deaths per year?
{: .left }

…
{: .right}

I don’t know
{: .right}

What about those we don’t count? Because we can’t, but also because we don’t want to.
{: .right}

What about getting to decide?
{: .right}

That this doesn’t make sense?
{: .left }

What about the us vs them?
{: .right}

We should think about this more often.
{: .left }

I don’t know, I’m just asking.
{: .right}

Yes
{: .left }

We should think about this all the time.
{: .right}

Well…
{: .left }

Yes
{: .right}

Yeah, I’ll try.
{: .left }

Good night?
{: .right}


[^1]: A Guppy is a tropical fish originating from South America but
that has been introduced in many other regions of the world. The wild
guppy feeds on various types of food including aquatic insect larvae and
notably, the mosquitoes larvae. The guppy was introduced in many
ecosystems as a mosquito control but ended up damaging the balance of
the ecosystems it was brought in. *Des moustiques et des hommes*,
podcast on France Culture,
<https://www.franceculture.fr/emissions/la-methode-scientifique/des-moustiques-et-des-hommes>

[^2]: The Hispaniolan crossbill is a crossbill endemic to the island
of Hispaniola which is the island shared between Haïti and the Dominican
Republic. The Hispaniolan crossbil is listed as an endangered species.
Malcom Ferdinand tells a story about the Haitian anti-colonial fight in
relation to mosquitoes in his book *Une écologie Décoloniale. Penser
l'écologie depuis le monde caribéen*. “John McNeill shows that the
mosquito, as a vector of yellow fever (Aedes aegypti) and malaria
(Anopheles quadrimaculatus), played an important role in the failure of
colonial projects in the Americas. This political alliance was
spectacular during the Haitian revolution. Proliferating through the
plantation landscapes of the colony, mosquitoes bite the entire
population. However, different groups of the population were affected
according to their different immunities to yellow fever. Toussaint
Louverture, Jean-Jacques Dessalines and the former slaves were able to
use this difference in immunity to their advantage. Having been born in
Santo Domingo/Haiti or coming from the regions of Africa where yellow
fever exists, they had developed a "natural" immunity. Recently arrived
white Europeans, such as General Leclerc's troops in 1802, did not enjoy
the same immunity as the blacks of Haiti. Faced with the French troops
sent by Bonaparte to quell the revolt, Louverture and Dessalines decided
to stay in the mountains - places less infested with mosquitoes - and
practiced guerilla warfare in anticipation of the rainy season which,
because of the proliferation of mosquitoes in the plains, was
catastrophic for Leclerc's men. Of the 60,000 to 65,000 soldiers brought
to Santo Domingo, 35,000 to 45,000 died from yellow fever. Through their
military strategies, anti-slavery insurgents and mosquitoes allied
themselves for a time to repel the colonial and slave holding forces.
This alliance lives on in the popular legend that Mackandal, the Brown
Negro who terrorised the planters of the colony fifty years earlier with
his poisonings, escaped death on the stake by turning into a mosquito.”
(translation by Elodie Mugrefya)

[^3]: Culex and Aedes are two different genus of mosquito

[^4]: <http://postgrowth.art/managing-extremity-En.html>

[^5]: According to Erik Orsenna (*Géopolitique du moustique*) an
emergent disease is not a disease that recently appeared but instead a
disease that starts affecting wealthy nations. Jean-François Saluzzo,
Pierre Vidal and Jean-Paul Gonzalez in *Les virus émergents* explain
“The example of AIDS \[…\]: this disease has long been ignored in its
natural African home. It was only after it was introduced into the
United States and Europe that the disease and its viruses were described
(and so becomes an emergent disease, A/N). Likewise, epidemic
surveillance networks were only able to detect the coronavirus
responsible for SARS after it had spread outside its natural home. The
example of SARS highlights the sensitivity of our surveillance system
for new diseases, a system that only becomes effective when the germ
penetrates the hyper-medicalised societies of the North, whereas it may
go unnoticed in its initial outbreak.” (translation by Elodie Mugrefya)

<style>
  .left {
    margin-right: 50%;
  }

  .right {
    margin-left: 50%;
  }

  :is(.left, .right){
    font-style: italic;
  }
  :is(.left, .right)::before{
    content:"“";
  }
  :is(.left, .right)::after{
    content:"”";
  }
</style>
