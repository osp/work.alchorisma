title: Tree Dream
author: Tiina Prittinen
summary: The map to get out of ‘the cover of the forest’ and the text of a dream are elements to remediate. They are healing from entropy, environmental and personal grief, intuitively using material from one tree, the work of the Alchorisma group and community, and Tiina’s own life experience and interactions since meeting the tree. In Finnish folklore, the cover of the forest – ‘metsänpeitto’ – is a state of invisibility or entropy for the lost one. People would search for you in the forest but not see you. There were simple spells you could perform to get out.
license: Images under CC rights BY, NC, ND.

# Tree Dream

<section class="side-to-side" markdown="1">
![]({attach}Tree-dream-media/image1.jpg){.image-process-illustration--inline}
![]({attach}Tree-dream-media/image2.jpg){.image-process-illustration--inline}
</section>

In my dream I collect leaves. The tree of Heksenberg has seen history,
times when witches and healers were hunted, also the rise of women from
patriarchy, social change, wars, conflicts, the price of freedom.
Sorrow, happiness, the flow of existence, stars, only some rays of light
from the past and from the future. Even when the trees are felled, the
roots stay in the past, the gardens become mirrors of love, human forms
stay as statues, space fills with the same love as it was around the
people who used to live.

I meet three ladies. One is the one who escorts people, from eternity
to birth and from life back to eternity. She is the one who at the very
last turn gives you a farewell or welcome hug. I also meet a lady who
paints -- to forget and to remember. Grief is a connection, and it
never fades. Triangles, squares, following rhythms that constitute
abstract trees, endless streams where all you can grasp is a thread.


The last lady, I can't remember. I try, but I can't. I am not wise
enough or I lack some important ingredient. Maybe I passed by her too
quickly, then missed the whole point. I begin to doubt myself -- was she
the one I met first, and were there three of them after all? I just know
there was something I should have learned. I loop back, start from the
beginning. I collect leaves.
