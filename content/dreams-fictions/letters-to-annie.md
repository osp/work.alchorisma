title: Letters - on the violence of naming
authors: Annie Abrahams & Ils Huygens
summary: The second worksession organized in Beaulieu, Vercors (FR) took place during the corona crisis. ‘Letters’ were a means of entering into a dialogue with those who were unable to travel but joined us from other places. The letters are a mix of travelogue, notes, readings and thoughts exchanged during that week.  
license: fal

# Letters - on the violence of naming

![some trees]({attach}letters-to-annie-media/image1.jpeg){.image-process-illustration--inline}

*Monday 24 August*
------------------

Maybe out of sync, but something I wanted to share:  
the violence of naming  
what is in a name  
what is your name?  
names names  
naming  
naming  
is identifying  
distinguishing  
placing  
ordering  
is categorizing  
you don't need my name to relate  
why name me?  
just feel me  
I am beneath, beside, underneath, with and above  
I can hold you  
you can hold me  
I am a tree  
forget Linnaeus  
eat me  
touch me feel me  
have a nice day!!!!  

<em>Annie</em>

 *Tuesday 25 August*
--------------------

Dear Annie,

I hope you’re well. It’s been a while since we were all working together
in Hasselt. In the meantime, many things have changed. While we tried to
stay connected as a group by discussing and reading together, the world
abruptly got disconnected.

Living in a multispecies world is also this, being entangled with beings
we don’t like to become entangled with, a virus, moving invisibly from
one body to the next, creating a global and hostile army that grows and
feeds on human togetherness. Being immersed in connections with other
beings can bring lucid dreams of stones, trees and spirits; it also
brings realities that are violent, harsh and ugly, like a lioness
killing its own cubs. The key to the multispecies world is balance,
balancing out the too many or the too many of the same, like the endless
walnut plantations we are surrounded by here in the area and the way
they are prone to disease.

Today we explored the surroundings by walking, sharing and learning. The
geological processes that formed the ancient plateaus of the Vercors
rose straight up from the bottom of the sea. The glacier that moved
through the landscape left pebbles and stones everywhere. These pebbles
and stones are used to build walls, borders and houses. Here, in this
turbulent landscape, formed by movements still ongoing, as Anne-Laure
put it so nicely, ‘the earth speaks a lot’.

When walking you need to keep your eyes on the ground or you’ll trip or
fall down one of the many holes and cracks in the surface.

Loveliest wishes,

<em>Ils</em>

PS: I will come back to your poem in my next letter. 

 *Tuesday 25 August*
--------------------

Thank you so much, Ils, for your touching letter!  
Looking forward to reading ‘today’.

<em>Annie</em>

PS: tomorrow I will use English again, sorry.

> oui, oui le virus fait partie de l'enchevêtrement des choses
> comme ce que nous en disons,  
> du covid,  
> comme de nos angoisses  

> l'état néerlandais a conseillé aux gens qui viennent de l'Hérault
> d'aller en quarantaine à leur retour  
> conseillé  

> trop et pas assez  
> ou  

> as if we would know  

> des conseils, des interdictions, des règles, des incitations  
> magic  

> touching trusting feeling  
> si les spells me kunnen afleiden, ja dan graag  
> en/chantons-nous  

> cobri-ari-bellails sumwegval ikkenie  
> cal-celt vlier jetetouch  

> psspieloeboulou troeloelou  
> tsjak  

> (à haute voix - fort - libre)

![]({attach}letters-to-annie-media/20200824_122027.jpg){.image-process-illustration--inline}

 *Wednesday 26 August*
----------------------

Dear Annie,

Thank you so much for sending this beautiful poem about the trees. It
resonated a lot with us and also made us laugh, especially after there
being so much confusion about names of trees in different languages
during our first day exploring the forest. Beech, birch, beuk, berk,
buche, hêtre, bouleau, lijsterbes or vlierbes … You’re right, we need to
get acquainted with the trees in ‘other’ ways, in order to understand
what they tell us – feeling, holding, listening, eating. Unfortunately
the *lijsterbes*, the only tree we met that is bearing fruit right now,
apparently is not very tasty, and An advised us to eat it only if we
were in acute need of food.

The walnut, the main fruit of the region, also is still hiding inside
its green shell, enjoying the last warm summer days for its final
moments of growth. In the Vercors, they would say that we are in the
time ‘just before the nuts’, as the passing of the year is expressed
here as either being before, during or after the nuts. I thought it was
a nice example of different lives and temporalities intertwining.

We also learned that in Celtic mythology the walnut tree is associated
with sensitivity, which is key to developing new ways of living with
other life forms. Sensitivity and attention are needed to find other
ways of listening, for attuning ourselves to other types of language and
thinking that are completely unfamiliar to us. A language that perhaps
doesn’t even communicate anything but only perceives, as suggested by
the President of the Association of Therolinguistics in Ursula K. Le
Guin’s short story, ‘The Author of the Acacia Seeds’.

Since we reread this story yesterday morning as an inspiration for our
first day of work, I would like to share the fragment with you as well.

When pondering the idea that some day we will discover the art of
plants, the President writes:

> Can we in fact know it? Can we ever understand it? It will be
> extremely difficult. That is clear. But we should not despair.
> Remember that so late as the mid-twentieth century, most scientists
> and many artists, did not believe that Dolphin would ever be
> comprehensible to the human brain – or worth comprehending! Let
> another century pass, and we may seem equally laughable. ‘Do you
> realise,’ the phytolinguist will say to the aesthetic critic, ‘that
> they couldn’t even read Eggplant?’ And they will smile at our
> ignorance as they will pick up their rucksacks and hike on up to read
> the newly deciphered lyrics of the lichen on the north face of Pike’s
> Peak.
>
> And with them, or after them, may there not come that even bolder
> adventurer – the first geolinguist, who ignoring the delicate,
> transient lyrics of the lichen, will read beneath it the still less
> communicative, still more passive, wholly atemporal, cold, volcanic
> poetry of the rocks: each one a word spoken, how long ago by the earth
> itself, in the immense solitude, the immenser community, of space.

Loveliest wishes,

<em>Ils</em>

 *Wednesday 26 August*
----------------------

Dear Ils,

Thank you for your beautifully crafted letters, thank you for making me
part of the Alchorisma process at a distance.

I send this early today and might not have time to write something
tomorrow, but will expect a call around 10 – if someone wants to.

I will try to keep up with the work in the pads.

love

<em>Annie</em>

> |I am a graft|a misfit|a hybrid|not good enough for what I am|I am
> usefulI have to be usefulusefulusefulusefulusefulusefuluseful
> |usefulusefulusefulusefulusefulusefulusefulusefulusefulusefulusefulusefulbut
> not alone|grafted in society|I need you|
>
> what if we stopped talking about communication and of reading
> non-humans, of trying to understand ‘them’, what if we were ‘with’,
> yes, indeed only perceiving what we can perceive … which is not much
>
> (btw who is this ‘we’?)
>
> how can I see my chakra?
>
> "Participation is not a design fix for machine learning. Ignoring
> patterns of systemic oppression and privilege leads to unaccountable
> machine-learning systems that are deeply opaque and unfair. These
> patterns have permeated the field for the last 30 years. Meanwhile,
> the world has watched the exponential growth of wealth inequality and
> fossil-fuel-driven climate change. These problems are rooted in a key
> dynamic of capitalism: extraction. Participation, too, is often based
> on the same extractive logic, especially when it comes to machine
> learning."[^1]
>
> What happens if we look at algorithmic capitalism as a magical process
> – In fact it feels like it is one – Is there magic to heal the
> process? Or do we, again, have to be ‘with’? … magical understanding
> might also be a way to control …
>
> what could be an interspecies opera? and could Fibonacci figure in it
> - how to turn this into something multi-perceivable?

![]({attach}letters-to-annie-media/IMG_1725.jpeg){.image-process-illustration--inline}

*Thursday 27 August*
--------------------

Dear Annie,

I hope you are well. I hope you are safe.

Who is ‘we’, you ask? Good question. It includes those who want to be
included, even if, in the end, it is only me, myself and I. It’s not
politically correct and part of the problem to say this, I know that. I
simply like ‘we’ better than ‘I’ and I certainly like it better than
‘they’.

Today another difficult but necessary conversation about ‘we’ and ‘I’
came up. No answers, only questions, clouds, mist.

We are suspended.  
Floating in thin air.  
We follow the rules and wait.  
What is natural, human?  
What is normal?  
What is needed?  
What do we miss?  
I’m sorry but,  
what to do?  
Training trees, training bodies,  
training dreams.  
Please bear with us.  
Loveliest wishes,

<em>Ils</em>

 *Friday 28 August*
-------------------

back home, I have been reading

thanks Ils, thanks Elodie, thanks the pads, thanks all

What do we lose and gain by calling ourselves witches?

Would accepting we can't communicate with plants, trees, stones be an
option and humble us and help us to be interdependent and with all.

With love,

<em>Annie</em>

![]({attach}letters-to-annie-media/20200821_152855.jpg){.image-process-illustration--inline}

*Tuesday 1 September*
---------------------

Dear Annie,

This is my final letter from Vercors. It was raining when we left. The
Vercors plateau was covered in a heavy and impenetrable layer of mist,
obscuring it from sight. It was nice since we hadn’t had the chance to
experience the mountains in this kind of light before. I’m on the train
now watching clouds pass by while the landscape is slowly getting
flatter. On one side of the train they look bright, on the other side
grey and heavy.

What’s there that we took home? Ideas, new plans, two different recipes
for tarte Tatin. Not to forget the remainder of a large bag of walnuts I
bought at the local Nut Museum which is called Le Grand Séchoir. I
wasn’t planning on taking them but after tasting some last night, I
decided I would.

The Grenoble nuts tasted different from the ones I’m used to. They
weren’t fresh, so it was impossible to peel off the last thin layer of
skin (my personal requisite for eating a walnut). Yet still they were
very tasty, not dry, shrivelled and bitter like the walnuts from last
year I still have lying around and will probably never eat. It’s amazing
to see the variety of walnut species that exist around the world. The
Nut Museum, as is expected, holds a nice collection of them, all types
of different walnuts (round, oval, thick, long, flat, light, dark, black
… ). Perhaps you saw this in the pictures we sent.

In order to form a group, you need to travel together – these words from
Ursula K. Le Guin often came back to us during the week. So we
travelled, walked and ate with each other but also with the snails
crawling up wet tents, the sugar-riven wasps, the curious spiders, the
uninvited mosquitoes, the sexually confused flies, the refugee beetles,
the wild horses in the camping shop as well as the trees, rocks, moss,
lichen, and all the strange creatures in-between.

The ones we met, the ones we missed and the many that remained unseen.

Watching trees  
Training trees  
Performing trees  
Becoming a walnut  
Do we feel grafted or merged?

Speak soon,

<em>Ils</em>

[^1]: Quoted from: ‘Participation-washing could be the next dangerous fad in machine learning: Many people already participate in the field’s work without recognition or pay. by M. Sloane, *MIT Technology Review*, 25 August 2020, <https://www.technologyreview.com/2020/08/25/1007589/participation-washing-ai-trends-opinion-machine-learning/> (accessed 25 August 2020)
