Title: Tarot Reading
Author: Malgorzata Zurada
Summary: This Tarot reading was conducted in the early afternoon on December 6th, 2018 by Malgorzata Zurada, with the use of Hexen 2.0 deck by Suzanne Treister. It took place during the Alchorisma worksession, in the former beguinage that now comprises part of Z33 – House for Contemporary Art in Hasselt. The modified three-card spread was used to gain insight into the nature of Alchorisma, its past, present, and future.
license: fal

# Tarot Reading
# 6/12/2018, Hasselt

<!-- ![]({attach}tarot-reading-media/alchorisma_tarot_reading.jpg){.image-process-illustration--inline} -->

## 1. AN OVERALL SITUATION. PAST THAT INFORMS THE PRESENT

![]({attach}tarot-reading-media/tarot_1.png){: .out .image-process-out}

Q: What is our work about? Where does it originate from?

A: Six of Swords (air)

> “Thus questioning, we bear witness to the crisis that in our sheer
> preoccupation with technology we do not yet experience the coming to
> presence of technology, that in our sheer aesthetic-mindedness we no
> longer guard and preserve the coming to presence of art. Yet the more
> questioningly we ponder the essence of technology, the more mysterious
> the essence of art becomes.”
>
> *― Martin Heidegger*, The Question Concerning Technology

Recognizing problems and moving towards solutions. Transition. Being in
transit, travelling, inner journey. Stars, transcendental realms, lofty
ideals and the New World Order. Non-human personhood, universal
*Dasein*. Intellectual discipline, gaining spiritual insight through the
depth of one’s thought. The danger of technology lies not in what it can
do, but in what it essentially is: its coming to presence conceals the
truth. Technology is ambiguous and this ambiguity points to the mystery
of all revealing. The key to see technology for what it is, is art.
Black forest, being in time. Illumination vs. enlightenment.

## 2. ADVICE FOR THE PRESENT MOMENT

![]({attach}tarot-reading-media/tarot_2.png){: .out .image-process-out}

Q: What specifically should we focus on during the worksession?

A: Knave of Pentacles (earth)

> “Techno-gaianism: belief in the power of technology to restore Earth’s
> environment.”

The bearer of good news in earthly matters. Earthly matters need to
finally be put in the context of Earth. Laying foundations for the
future by education and re-education. Redefining techno-gaianism.
Learning to recognize the consequences of one's actions. Juvenile faith
in one’s omniscience being matured and transformed through the
alchemical process. Alembic for the eternal light of life. Distilling,
purifying, transmuting, splitting the atom, opening portals. Closed
biosphere experiments on Earth & space colonies, creating reality
bubbles. Terraforming other planets, manipulating the *prima materia,
*genetic engineering, tinkering with the mystery of creation. Infinite,
fractal matryoshka of interconnected living beings. Earth’s inner Sun &
the galactic central Sun.

## 3. THE OUTCOME: PRESENT MOMENT INFORMING THE FUTURE

![]({attach}tarot-reading-media/tarot_3.png){: .out .image-process-out}

Q: What will be the outcome of our work?

A: Four of Pentacles (earth)

> “The mission of the WWWF is to advance the Web to empower humanity. We
> aim to do this by launching transformative programs that build local
> capacity to leverage the Web as a medium for positive change.”
>
> *― World Wide Web Foundation*

A web of relations. A network of interlinked people, machines, thoughts
and agendas. We’re all going online. Hypermedia, metadata, DARPA, EU,
CERN, Web 3.0, Humanity+, transnational bodies. Hidden agents, occult
algorithms and distributed causes create and affect the network. Being
held hostage in a holographic fiction. Control technologies, smart grid,
the myth of net neutrality. Casting nets under the guise of free access
to the network. Itemizing, monetizing, securitizing, systematizing,
assimilating of the living beings. The web = spiderweb, created by/for
AI spider. The internet of human-things. The dark side of the Aquarian
Age is about collectivism and technology. The Borg. What appears to be
decentralization is in fact the ultimate bonding and assimilation to the
hive mind. The gaseous state of matter, bonding of atoms. The need to
obstruct progress by infecting the existing algorithms. Holding on to
the analog past to protect the self and others. Conservationism in the
information age. Pouring sand in the gears of digital expansion.
Subversive cog in the machine, the violet flame.

<style>

article img.out{
  max-height: 50vh !important;
}

</style>
