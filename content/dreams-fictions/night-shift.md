title: Night Shift
author: Alchorisma participants, edited by Malgorzata Zurada
summary: ‘Night Shift’ is a collective dreaming practice that was held during the summer of 2020. At first it was exercised remotely once a week, then daily – during the Alchorisma summer camp in Beaulieu. Some of the dream sessions had a thematic focus on Beaulieu, other were more broadly referencing various Alchorisma threads. The aim was to simply explore the collective practice of dreaming and investigate the mutual relation – or feedback loop – between research strategies of the conscious and unconscious mind.

# Night Shift

Having a lot of bad dreams lately.

Can dreams touch? Can chestnuts feel?

We are simultaneously in a physical space and on an astral plane. These
two dimensions -- the physical and the subtle one -- are interwoven and
we conduct our research in both at once. Our research threads are
symbolized by sigils floating horizontally in the air. When no one is
working on a given thread, its sigil is black and still, as if it was
dormant. Whenever the thread is activated by our focus, its sigil
switches on and starts flickering in prismatic colours.

I keep seeing complex, geometric shapes resembling the reversed
honeycomb (the hexagons are 'filled' and the spaces in-between are
'empty'). These hexagonal shapes are made of wood and are morphing and
pulsating, their edges becoming rounder and smoother, then sharper
again. The wooden honeycomb structure somehow seems to be alive.

We are at the lake to measure its dimensions. After a while it becomes
apparent that by measuring it, we are actually attempting to heal the
lake with our attention.

I was hiking in the mountains and got stuck in a small village far from
anywhere when it became dark.

I visit a place where people are healing their illnesses and wounds.
They show their injuries to one another and share their experiences.
It’s not a hospital, there’s no medical staff. The convalescents are
joyful and at peace, resting and self-healing in harmony.

A group of people working together (online, offline, not clear). I
wasn't there, but I lived close to one of the members and proposed to
meet her. She was in Turnhout, and apparently I was at my birthplace in
the Netherlands and not in France, where the others were. When we had
contact with the group they told me they were all going to Turnhout -- I
hadn’t told anyone, it was just an idea in my head, this going to
Turnhout. I asked them what they were going to do there. No one knew.

Other participant: I am from Turnhout! In the very beginning we
considered organizing the worksession in the beguinage there, where my
mum is a guide.

My consciousness has divided into three parts and inhabits three people
now -- a woman, a man and a child, all at the same time. The three of us
get on a ship and sail away to an island.

We walk through the forest of young trees and collect data.

We make envelopes with seeds and a letter, write down the destinations.
We talk about ecofeminism. Someone holds an object and proposes to put
it in the trash. Oh no, we need it, says someone else. It is a
corkscrew.

In the post office I queue in the company of a group of computer
scientists.

I am having lunch on a café balcony overlooking a valley with some
Italian acquaintances. The waiter brings us focaccia and pizza, but my
friends are allergic to tomatoes. I eat hungrily, but my friends brush
their plates aside and drink aperitifs in the afternoon sun. Later we go
indoors, which is a very dark 'black cube' gallery space. My colleagues
are photographers and we are trying to solve the problem of documenting
a very subtle sculptural/light intervention in almost complete darkness.
The gallery photographer comes up with a solution by attaching a long
cardboard -- a bit like a shoebox -- to his lens.

I recall I was taken to a party with a group of photographers. It was a
kink party in a very stylish apartment overlooking a river (the Spree?).
One of the photographers was talking about how he intuitively fell in
love and began a relationship with a woman who was a manager in a
shipping or air freight company. This enabled him to get closer to his
subjects, who were employees of this company. His portraits used
conventional aesthetics to present a critique of labour relations. This
made me think of the economic subtext of relationships and made me
wonder how to value them in a society in which the market has captured
everything.

Each of us swims in a different swimming pool, in a different place, but
we are mentally linked with each other. The pools are made from clay,
are round, and form concentric patterns. I see us from up high, as if I
was hanging in the air.

We work in an underground base. The base is half flooded, damp and dimly
lit; moss grows everywhere. The work is organized in such a way that
each person is dependent on help from others -- to complete any task we
have to ask one another for assistance.

I am hovering above the fields and forests and see that there is a
geometric grid that underlies all life, a sort of invisible chessboard.
Somehow I know that the grid is not natural but is rather a synthetic
enhancement of life on Earth.

We are on a hike in nature, paying attention to the vegetation around.
We notice the first signs of autumn coming. We spot a snake.

I am in front of the cinema which is playing a movie that I had
previously lent them on DVD. Later they return it to me -- but instead
of a DVD it is now in the form of a soft drink. I drink the movie
through a straw, contemplating the artificial taste and the overall
strangeness of the situation.

I visit a friend and ask for a tarot session. He uses the Thoth deck and
places the cards on my back, so I don’t see them. There is another
fortune teller there, she has a broken leg and is limping.

We are shapeshifting into various animals and fighting with each other.

I see everything in an isometric view, as if life was a game or a
simulation. All choices seem to be reversible. They don’t carry the
same weight as in ‘normal’ life. A lover from my past wants to
re-establish contact.

I am gardening with my brother. We first start to argue about how to do
it, then discuss and go about it in a more relaxed manner.

We are sitting around a long, wooden table inside a glass dome. There is
a Brazilian artist-friend who found us through a network called 'Algae
(something)'. There is also a host who presents himself but I cannot
hear what he says.

I am in a square made of stone, with a small rock on which I can stand.
I stand on the edge and hold another rock. This action is called
'Exile'; holding the rock is analogous to the weight of migration. The
stone square is filled with water. I might be in Croatia?

I visit two houses in which I find relics of my families. I feel
goosebumps in my dream. A phantasmagoria. We are on the edge of a big
forest.

We are filling our backpacks with stones. Stones are watching over us.

We are in the centre of the city, on a big square that is blocked due to
the demonstration. I get separated from the group and end up in Ikea.
Merchandise is scattered over the floor as if in the wake of a hurricane
or an act of vandalism.

The house in Beaulieu is turned inside out (or is a mirror image of
itself?): what’s left is right, and what’s right is left, familiar but
not the same.

I meet a group of pilgrims from Brussels who have been walking around
the world for two years. They all look very happy but very dusty -- in
need of a shower. We have drinks together. In the same bar I meet more
friends. We have more drinks. One of them is a little drunk. She shows
me a pink earring in the form of a strawberry. She cannot wear it, she
says. It is too heavy. I take it in my hand: it is heavy indeed.

I am in a multistorey building. People are lying on big, edible beds;
some beds are made from nuts.

We make then eat a cake.

Repetitive packing a backpack, moving in circles, swimming in circles.

I take a stroll across town, it’s a beautiful sunny day; people are
eating, drinking wine, enjoying themselves. Suddenly I get my period and
notice that the blood is green.

We are in a big, lush orchard-forest filled with many different trees.
The gentle sunlight sifts through the canopy, it’s calm and green
around. The gathering is very official, we are representatives of
various areas and countries. The agenda is that each one of us -- in the
course of five days -- will study one tree specimen and gather data
regarding this tree -- botanical, historical, symbolic, spiritual, etc.
-- and then present it as a comprehensive paper. I have acorns in my
hand, so I gather I ought to study the oak. Everyone takes their job
very seriously; the atmosphere is formal and dignified. We are aware of
the importance of what we are doing.

We guard a pavilion in which the gorillas live. All visitors are dressed
in the same colour scheme: khaki, coral red, ochre, and white; the
colours recall warm, sunny days and tropical lands.

We drive around the city in a car donated to us by a sponsor. We feel
obliged to create a new Alchorisma thread dealing with the automotive
themes.

<style>

  article p{
    margin: calc(var(--em-space) * 6) 0;
  }
  article p:nth-of-type(odd){
    margin-left: calc(var(--pad) * -2);
    margin-right: calc(var(--pad) * 2);
  }
  article p:nth-of-type(even){
    margin-left: calc(var(--pad) * 2);
    margin-right: calc(var(--pad) * -2);
  }

</style>
