Title: Stone Poem
Author: Annie Abrahams
Summary: A short poem on being stony.

# Stone Poem

How does it feel to be broken?  
to be moulded?  
to be round?  
to be baked?   
to be industrial?  
to lie down?  
to be carved?  
to be pushed?  
to be in a border?  
to be the border?  
to have a hole?  
to be one of many?  
to be made into?  
to be among others?  
close to another material?  
to be erected?  
to be square?  
how does it feel to be of stone?  
to be a **stone?**  
to be stony?


03/12/2018, Hasselt
