Title: Daily Reflections
Author: FoAM Earth
Summary: FoAM Earth's involvement in Alchorisma’s worksession in Z33 consisted of designing daily reflections using the words and ideas proposed by the participants. They designed the meditative and poetic reflections by working with online documentation posted online daily by the workshop participants. The participants used the reflection as part of their 'morning practice'. It was an experiment in remote participation and facilitation, purely based on content generated during the workshop, without personal interactions with the group.

<div class="iframe-wrapper">
<iframe src="https://libarynth.org/alchorisma_daily_reflections" ></iframe>
<a class="vines-button" href="https://libarynth.org/alchorisma_daily_reflections" target="_blank" data-role="fullscreen">Daily Reflections</a>
</div>
