title: Writing Speculation Exercise
authors: Artyom Kolganov & participants to the worksession in Beaulieu
summary: The summer camp in Beaulieu took place in the realm of pandemic ambiguity: while several countries reopened their borders, certain places, like Moscow, were still in a strict lockdown. The idea of an online speculative writing exercise arose from different regimes of presence in the camp as well as from the intention to document the multifaceted identity of Alchorisma through fictions. The intention of the exercise was to describe both the site-specific and de-spatialized experience of the collective self distributed through different forms of affect. Prior to the exercise, the participants were told to describe places around them and think of key subjects of Alchorisma, so that the fragments of the text would be centred around spaces and entities – forests, spirits, cities, trees – at once documenting the events of the camp and creating a multitude of collective experiences.
license: fal

# Writing Speculation Exercise

Beaulieu is a safe haven surrounded by hundreds of thousands of walnut
trees. I am sitting in a place where in the old days there was an
entrance gate. To my left there is a huge ghost lime tree – a tree that
is no more, but still is. Close to the ghost there is another lime tree
that still exists physically. In front of me there is a 35-year-old
Himalayan cedar that Anne-Laure has known since it arrived at the house
as an unwanted gift. Also in front of me, a little bit to the right,
there is the vegetable garden which used to be tended by Anne-Laure's
father. To my upper right there is a terrace where I feed the spirits of
the house – every day four small plates are placed as an offering to all
invisible and intangible inhabitants of the place, the ones often
forgotten, unacknowledged or overlooked. The terrace is a place of
gathering for them and for us, twice a day in a meditative ceremony.

Yesterday for a short while I became a walnut tree; I was more than
surprised to feel the world as a tree may feel. All of the walnuts here
are grafted – the roots are from one tree, the top from a younger, more
fragile one, stitched together to form one entity. I clearly felt the
physical division at the level of my knees and the trauma stored in the
cellular memory of the tree-body. The other day we plunged into the
ice-cold river; its waters carry the information we could decipher
through our skin. The whole area is full of pebbles, scattered around in
heaps and piles; they were carried here by the glacier during the last
Ice Age. Carried from where? Where do they originate? When did they
originate?

I found a wounded pebble and brought it to the house for rest and
healing. During the walk in the forest I heard the jay. I asked it to
kindly give me one of its blue feathers, it gave me a grey one. The
other day I found another jay feather, this time with a slight tinge of
blue. Yesterday we looked at Jupiter, Saturn and the Moon, almost in one
line across the night sky. Then we drank the walnut wine.

![]({attach}speculation-exercise-media/20200821_143851.jpg){.image-process-illustration--inline}

I'm now looking at the window that looks at the alley surrounded by
trees. The alley is a quiet piece of space between two monumental
buildings whose shadows look into each other. Looking out at the trees
in the morning feels like a vivid connection in the middle of an inhuman
place right in the central part of the city. I'm thinking of a
collective tree spirit, the universal in the particular – a forest near
my grandmother's small house, giant pines in Bokrijk, Scandinavian
landscapes that I last saw before the borders closed. Do trees have
borders? A weird feeling: while looking at the window from a distance,
the whole space is occupied by stones which surround the building, but I
can clearly feel that the trees are here. Stones and trees make me
remember about Hasselt – where now is the stone we were friends with
then? I still care about its spirit but I still haven't learnt to
communicate with it in an interlocutory way. I've been thinking a lot
about communication recently – how should we interact equally, being
different substances? The sun is finally here. We started this dialogue
by talking about rainy days – is it a performative possibility of
communication or do the spirits maybe understand the words? I'm looking
again at the tree in the alley. It's impossible to talk about/with trees
without talking about/with spirits. And through a universal tree spirit,
I can now feel the connection with the trees out there.

The bird sings when there is a gap in the text. Was it an invitation?
Does birdsong wait for its slot of air, its due invitation, or was it my
awareness that expanded?

 

I plunge into a void each time I close my eyes: again and again, that
welcoming, warm and full darkness that I know from every night and
morning, afternoon even, meditation, whenever. I think of the yanomami
shamans that embraced the missionary religion: they took in the GOD, but
god does not answer their prayers, it is a silent god. They become
enraged, being accustomed to receiving the spirits and seeing their
dance. I think about this void a lot, of a culture of replacement and
externalization of belief. The void bothers me, too, but it doesn’t
bother me either. It just is.

![]({attach}speculation-exercise-media/image1.jpeg){.image-process-illustration--inline}

It is difficult to sleep and costly to wake up as all the sand must be
shaken from my eyes while I re-enter this body. Vigil comes with a harsh
feeling of passing a threshold, a familiar sensation sometimes
experienced when battling with Hypnos to gain the waking world. Many
times I lose, often taking pleasure in the dive.

Yesterday I documented tree individuals, up close, very personally.
Their man-made genesis impresses me, and I can't help but notice how
particular they are after learning of the human intervention that takes
place in their growth. The slit, the graft, merges and infuses, a tree
onto another, formation around a rupture, disform filling, deforming
form, informing deformation. They leave growth marks, produce scars,
create a new hybrid. It is like looking at a tree guest that bursts from
within a trunk host, but the guest and host are the same. Upon
observation, I feel the eeriness of shapes in these fields, like a
discrete note that is out of tune in the back of the mind's eye. But I
have scars also, I am also a product of hybridization and relate my
becoming to external, laboratorial intervention. Even though my coming
into the world is different from these trees, I also have my slits, my
frontiers, and in this I feel a communion. While exercising walnut
training, can we train our becomings? Or do we train our being?

For the first time this week I had a communication with a grafted tree.
We are surrounded by walnut trees here. They are cultivated in
plantations, row by row. Each tree combines a stem of one type of robust
walnut tree (like *Juglans nigra*) and one type of more fragile walnut
tree that gives rich fruit. The tree felt peaceful but it also carries a
wound that still causes pain. The idea of being artificially composed by
elements – branches – of two different individuals is still puzzling to
me somehow.

![]({attach}speculation-exercise-media/IMG_1719.jpeg){.image-process-illustration--inline}

We also met the tuff, a stone made of moss and limestone. We came across
the tuff in the making on our walk throughout the Gorges de Nan. It is a
natural process in which the moss absorbs the water that drips from the
rocks. But the moss is fooled somehow, because the water it drinks
contains so much limestone that it is killed by it. The rocks in this
area can be very impressive, like huge walls. They make me stand still
and want to listen to them. Unfortunately, I haven’t developed the
methods yet to listen to stones. It might be a next stage, especially
here where the stones date back to long before the last Ice Age.

Gosia aliments the spirits daily. It is becoming clear that everything
and everyone carries spirits with them. So it is getting difficult to
talk about trees and stones without talking about their spirits. Even
the printer showed its spirit yesterday as we printed a page with a
guided meditation on 'becoming a walnut tree'. The printer only printed
half a page, cutting the text at exactly the paragraph where the text
started talking about the graft. Not only was this an invitation to
connect more to the specific walnut trees here, but the guiding also
became a graft between the version of Gosia – who had prepared the text
– and my improvisation.
