Title: Apple Soup
Author: Rares Craiut
summary: As a chef in residency during the Alcorisma summer of 2020, Rares Craiut was impressed by the landscape around. He was reminded of foraging trips during his socialist past. Participants in the residency foraged in a very small quantity, since the house garden and orchard were very generous. Rares thought about teas and drinks from plants in the area that he recognised, and he thought of cooking with three bark. The one semi-foraged recipe that he did have time to cook was an apple soup, based on a classic Hungarian summer dish. His grandmother used to make it. His childhood disgust with it was surpassed only by the sour cherry soup, its other sweet and cold soup sister. Coming out from school he used to long for proteins, like any kind of pork products, or carbs, like mashed potatoes. Or both! Apple soup was just a cruel gastronomic tease, leaving him wanting for more… more, but not of the same.
license: fal

![apple soup recipe]({attach}apple-soup-media/apple-soup-rares.png){.image-process-illustration--inline}
