title: Tu(r)ning into trees
author: Ils Huygens
summary: This text untangles some of the lines that have run through the Alchorisma research. It elaborates on the basis of the knowledge we gathered from meetings with trees that took place during excursions and field trips, and uses these meetings as a pathway to revisit some of Alchorisma’s leading references, concepts and methodologies. The text uses these meetings with trees to unravel practices of attunement, attention, embodied immersion and care for other beings, opening up new ways of engaging with trees, landscapes and other beings from a multispecies perspective.
license: fal
Date: 2020-01-03

# Tu(r)ning into&nbsp;trees:

# The multiplicity of becoming –&nbsp;root, trunk, branch, leaf


> Kinship
> Very slowly burning, the big forest tree
> stands in the slight hollow of the snow
> melted around it by the mild, long
> heat of its being and its will to be
> root, trunk, branch, leaf, and know
> earth dark, sun light, wind touch, bird song.
> Rootless and restless and warmblooded, we
> blaze in the flare that blinds us to that slow,
> tall, fraternal fire of life as strong
> now as in the seedling two centuries ago.
>
> — Ursula K. Le Guin[^1]

Alchorisma covers multiple threads of living in a multispecies world.
This text untangles some of the lines that have run through this
multilayered project. It touches on some of the ideas developed during
the two worksessions, leading up to the collection of prototypes, texts,
dreams and fictions presented throughout this online publication. The
lines gathered here try to cover some of the threads that guided us in
the alchemic meetings that occurred between the worlds of stones, trees,
ghosts and algorithms wielding between artistic practice, animist
knowledge and database technology.

The text elaborates on the knowledge we gathered from meetings with
trees that took place during excursions and field trips, and uses these
meetings as a pathway to revisit some of Alchorisma’s leading
references, concepts and methodologies. The text uses these meetings
with trees to unravel practices of attunement, attention, embodied
immersion and care for other beings. They offer possibilities to use
artistic practice and algorithmic technology as a means to open up new
ways of engaging with trees, landscapes and other beings from a
multispecies perspective.

## First meeting: An unadjusted Douglas fir

#### Location: Bokrijk, Genk, Castle Garden

![DouglasFir.jpg]({attach}Turning-into-Trees-media/image1.jpg){.image-process-illustration--inline}

*One of the most remarkable trees of the old English Garden around
Bokrijk Castle, located between Hasselt and Genk, is an old Douglas fir
with four stems. The English garden was created around 1900 together
with the castle. A typical feature of the English park style was to
create small groups of large, impressive (and often exotic) trees as a
way to show one’s social status. In Bokrijk we see small batches of
Douglas fir, as well as a majestic alignment of these firs marking the
wide alleys of the park. The largest Douglas fir in Belgium is found
here, although it is not even half the 100 metres it can reach in its
native habitat (mainly America’s West Coast and British Columbia). The
name of the species is derived from Douglas, the Englishman who first
introduced the species in Europe in 1827. Other exotic trees we
encounter in the park are the Japanese larch, Atlas cedar and the
Pacific red cedar, a majestic tree that can live up to a thousand years
(hence it’s nickname, giant arborvitae or tree of life). The park also
holds the largest collection in the region of the monumental mammoth
tree, of which two large specimens mark the entrance to the castle.*

This style of garden is in stark contrast to plantations of trees, where
straight stems are favoured to enhance productivity. When a tree grows
in multiple stems it decreases stability and complicates the energy and
water flows. In plantation forests meant for wood production, trees need
to grow high and fast. ‘Unadjusted trees’ like this Douglas fir with its
multiple stems would not be retained. However, for the joy of human
delight, abnormal growth patterns and tree deformations were accepted
and even encouraged in the English style, turning the trees into natural
‘follies’ that could inspire the visitor with feelings of awe and
wonder. Nature as curiosity.

The multistemmed Douglas fir is part of the vast idyllic park
surrounding the castle, fully constructed and man-made. Its origins are
based on heroic travels. For the sake of showing off one’s social
network and status, botanical curiosa like exotic tree species were
trades as gifts among the higher classes, often ending up in private
parks like this one. Paradise, however, requires continuous maintenance
and meticulous management. Most of the trees were not adapted to our
climate and that is why only a few of these remarkable exotic trees that
were brought back from faraway travels managed to survive. Most of those
that remain are found in private or botanical gardens. Apart from some
exceptions, most of these exotic species require an extremely devoted
gardener to keep them healthy and alive in the alien and often
inhospitable grounds in which they were relocated.

## Second meeting: a dead tree stump

#### Location: Bokrijk surrounding forest

![DeadTreeStump.jpg]({attach}Turning-into-Trees-media/image2.jpg){.image-process-illustration--inline}

*‘Death does not end the networked nature of trees. As they rot away,
dead logs, branches, and roots become focal points for thousands of
relationships.’*[^2]

Dead wood is covered in mosses, lichens and different types of fungi. It
is vibrant with life that at different moments of time cohabitate with
the dead material, enhancing or thriving on the complex processes of
organic decay. Small animals like rodents and birds use it to shelter,
whereas the hollows and crevices can also serve as drinking pools. The
moist areas allow for mosses to grow, creating fertile breeding grounds
where all kinds of insects and beetles lay their eggs to hatch. By
feeding on the wood, the fungi and the newly born insects produce fresh
and nutrient hummus that in turn enables growth and gives life to the
surrounding trees and plants.

*‘Our language does a poor job of recognizing this afterlife of trees.
Rot, decomposition, punk, duff, deadwood: these are slack words for so
vital a process. Rot is detonation of possibility. Decomposition is
renewed composition by living communities. Duff and punk are smelters
for new life. Deadwood is effervescent creativity, regenerating as its
“self” degenerates into the network.’*[^3]

As Peter Wohlleben so wonderfully describes it at the start of his
international bestseller *The Hidden Life of Trees,* recent studies
point to something even more astonishing: in a forest environment, a
dead tree stump can in some cases even literally overcome its own death.
Scientists have discovered that tree stumps can be kept alive by
surrounding trees. While the tree stump itself is unable to extract
water anymore, it can be provided a minimum of water by the trees around
it. This suggests that trees have interconnected root systems enabling
them to exchange nutrients with each other. A tree, even a dead one is
never a single tree, but part of a larger forest organism,
interconnecting with other trees as well as with many other beings.

More and more recent findings in biology suggest a shift from the
perception of trees as single individual entities towards the forest as
an entangled ecosystem or superorganism. As the example of the dead tree
stump suggests, roots of different trees are connected with each other,
allowing the exchange of water and minerals, and even communication. As
Wohlleben explains so well in his book, this communication mostly
happens with the aid of other species, most importantly types of
mycorrhizae. These are types of fungi that live symbiotically between
and within plant roots. Hidden underground, a rhizomatic world
interconnects the roots of individual trees with a vast network of
mycelium, which is the name of the underground threads of fungal
organisms. A single mycelium network can extend over kilometres,
interconnecting individual plants and trees into what has recently been
termed the ‘woodwide web’. It suggests that trees are sentient beings
but also that the forest acts as a social system where the older
stronger trees protect the weaker or younger ones.

The interrelation between the tree and the mycelium is a form of
symbiosis based on mutual benefit: while the fungal threads allow the
trees to exchange information and nutrients, the mycelium is ‘paid’ with
the sugar it feeds on. Slowly but steadily these new insights are
upending our view of trees, suggesting that symbiosis, far from being a
biological rarity, is in fact all around us. It is a far stretch from
the tree as a beautiful yet single entity, as it was placed in the
English garden park. From nature as curiosity to a new curiosity for
nature, we may need to start acknowledging that it is precisely our
Western and modernist concept of ‘nature’ itself that we need to do away
with.

## Third meeting: database ghost trees

#### Location: Bokrijk Arboretum database

![LabelledTree.jpg]({attach}Turning-into-Trees-media/image3.jpg){.image-process-illustration--inline}

*The manager of the park and surrounding forests of Bokrijk introduced
us to a huge database of all the trees in the park. In this database,
trees are coded as species, numbers, coordinates; they are labelled as
alive, in good, fair or poor condition, removed, unable to locate or
dead, in multiple sublevels where clarity and ambiguity conflate the
real tree and the data about the tree. Inputting and updating the
information in the database is an exhaustive and time-consuming task
that requires continuous action. Ghosts of dead trees linger. With the
meeting of the dead tree stump in mind, we wonder, when is a tree truly
gone?*

The story turns our attention to how numbers, graphs, calculations and
algorithms are used to describe and map the natural world today. The
concept of nature nowadays is fundamentally intertwined with scientific
observations and research encoded in database technology. And just as
scientific objectivity turned out to be an unattainable goal, technology
is conflated by those who create the database, those who manage it and
those who update it.

It is precisely thanks to the massive expansion of database technologies
and advances in computer modelling that the concept of the Anthropocene
made its way into the current debate on climate change.[^4]
Paradoxically, it is the elevated capacity of processing extensive
amounts of data, which led to the acknowledgment of the disastruous
impact of the anthropos on nature. We needed the concept of the
Anthropocene to understand how urgent it is to decentre the human and
move ahead with the ‘decolonization of nature’. According to cultural
theorists like Timothy Morton, Bruno Latour and T. J. Demos, it is the
modernist and dualistic view of ‘nature’ we need to get rid of. In
rethinking the Anthropocene further into concepts like the
plantationocene and the capitalocene, cultural theorists argue that the
human-nature divide is not only what caused our problems in the first
place, but is also what is keeping us from acting and dealing with the
current crisis. Their argument is that we need to do away with ‘nature’
as a concept since it keeps us adhering to a world view where humans are
placed ‘outside of’ and subsequently also ‘above’ nature.

Both human and natural sciences are increasingly viewing the world as an
all-encompassing system where stones, organisms, animals, plants,
bacteria, viruses and humans are part of an interconnected network.
Within the network, one can never escape or ignore any of the other
presences. If one species overrules the system, the effects of this
imbalance will haunt the earth and leak into the future like radioactive
waste. The current corona crisis, caused by a microscopic, tiny entity,
is just one more example of these rippling effects. If we want to
survive as a species and deflect the anthropos’ fate of turning into the
ghosts of the Anthropocene, we need to fundamentally change the way we
see the world.

With the aid of thinkers like Anna Tsing, Donna Haraway, Bruno Latour
and many others coming from diverse fields like biology (Lynn Margulis,
Margaret McFall-Ngai) or anthropology (Eduardo Kohn, Eduardo Viveiros de
Castro) – to name just a few – a new paradigm is emerging based on a
model of symbiosis and fundamental entanglement.[^5] In view of climate
change and its pending disasters, a multispecies way of apprehending the
world can offer a way out of negation, pure apocalyptic thinking or
*simple* paralysis.

## Fourth meeting: grafted walnut trees
#### Location: Vinay, Isère Valley, Le Grand Séchoir – Maison du Pays de la Noix

![GraftedTree.jpg]({attach}Turning-into-Trees-media/image4.jpg){: .front .image-process-front}


*Our second field trip took us to the Isère Valley and the adjacent
Vercors massif, located between Valence and Grenoble in France. As far
as the eye could see, we were surrounded by vast walnut tree
plantations. The walnut trees, we learned, are grafted. In this
technique, which is different from cross-pollination, two plants are
literally joined into one by creating a wound in one and inserting
another type of plant into it so the tissues can grow together. The
walnut trees are literally pieced together by man in order to enhance
economic viability, mixing the positive qualities of one species with
those of another.*

The technique of grafting walnut trees was developed and mastered in the
Vercors and is an important marker in the story of the famous Noix de
Grenoble, one of the first DOC-labelled fruits. The story of the walnuts
in the Isère Valley is interesting in its own right. Far from being an
original regional product, the walnut business here started out as the
result of a biological disease, the infamous *Grape phylloxera*, a type
of yellow sap-sucking insect that feeds on the roots and leaves of
grapevines and which started spreading through the more traditional
vineyards of the region in the mid 1850s. The massive impact of this
biological event made more and more farmers turn to walnut production.

The walnut tree in particular thrived in the region, shaded from strong
winds and extreme colds by the massif of the Vercors that is aligned
along the river, the ground scattered with pebbles forming an ideal type
of soil for the walnut whose roots dive deep into the earth. The
grafting technique of walnut trees was perfected by the mid nineteenth
century, increased production and industrialization tying in with the
opening of a new railway line from Grenoble to Valence, giving the Isère
access to important international markets and pushing even more farmers
to favour the walnut business.

The landscape of walnut trees is based on interrelated stories of
different species, of insects, diseases, bacteria, bifurcating with
regional geology and the rise of modern plantation technology,
industrial production and global capitalism. *‘Landscapes enact
more-than-human rhythms. To follow these rhythms, we need new histories
and descriptions, crossing the sciences and humanities.’*[^6] As Anna
Tsing so beautifully pointed out in her book *The Mushroom at the End of
the World*, the many interrelated encounters that form our contemporary
landscape, depend on soils, mushrooms, disease organisms and people
whose knotting together at specific points in time can lead to massive
changes in the landscape.

Once we lay down the dominant image of linear modernistic time and its
ideas of limitless growth and continued progress, we become susceptible
to different rhythms that lie hidden in the landscapes of today,
generating a new curiosity that allows us to follow ‘multiple
temporalities’ and opens us up to a ‘revitalizing of description and
imagination’. The famous Noix De Grenoble, today one of the region’s
most important economic agricultural products, tells a story of many
intertwining histories that gave rise to the plantations we see today,
making the region one of the world’s largest walnut exporters, yet at
the same time constantly struggling to ward off the plagues and pests
that haunt monoculture lands.

## Last meeting: the Lady of the Mountain

![]({attach}Turning-into-Trees-media/IMG_1668.jpeg){.image-process-illustration--inline}

Location: Forêt de Coulmes, Vercors

*From the place where we were staying we took a field trip to the park
of the Forêt de Coulmes, a natural forest high up in the mountainous
area around the Vercors at an altitude varying between 750 and 1500
metres. The main species in this dense and bucolic forest are beech
trees together with birch, pine, rowan, oak and ash. The dense shadowy
forest forms a sharp contrast to the open and strictly aligned walnut
plantations in the valley below. The forest is full of history evoked in
curious place names like le Pot de l'Aigle, Beauregard, Col de Pra
l'Étang and les Charbonniers. It is scattered with ruins of hammocks,
ancient water sources and old trails that sometimes end up in
mind-blowing panoramic vistas that uncover the little towns that lie
hidden inside the massif. Our guide, An Mertens, led us to this age-old
forest in a walk that acquainted us not only with the local species of
trees but also turned our attention to the Celtic values attributed to
them: the wisdom of the oak, the protective capacities of the hazelnut,
the strength and stability of the beech, the mystical capacities of the
beautiful rowan tree. The rowan, with its alluring red berries is also
called the Lady of the Mountain, because it is a species that thrives in
harsh mountainous areas like here in the Vercors. The Celts saw the tree
as a holy and highly spiritual tree, only to be approached or used by
those who were initiated to higher forms of knowledge, the druid or the
shaman. Its red berries have healing capacities but can also be
poisonous. The rowan tree can help humans fight against dark forces …*

The Celtic values of trees are based on the animist beliefs and
shamanistic practices of the Celts, whose worldview was based on the
forces of nature. In their world, non-human entities, animals, rivers,
mountains and trees were endowed with a soul. Looking back at these
ancient forms of knowledge, we learn that spiritual values were often
based on or led to material connections between tree and human, such as
types of usages of a tree’s wood, or link in with the nourishing or
medicinal capacities of its leaves, fruits, nuts or berries. By
acquainting ourselves with these spiritual perspectives, humans can
(re-)enter into meaningful rapports with more-than-human beings and
reacquaint ourselves with forms of knowledge that have been forgotten in
our modern age but are increasingly the subject of renewed interest
today, in art, science and even popular culture.

The animist tradition of the Celts is closely related to anthropologist
Eduardo Viveiros de Castro’s argument about how indigenous knowledge can
offer us insight into understanding the world as a ‘multiverse’. Ancient
forms of ‘cosmovisions’ conceive of the Earth, man, animals, rivers,
plants and stones as an indivisible interdependent relationship.
Anthropology and ethnography in particular are ahead in this
multispecies approach and the decolonization of Western modernist
knowledge systems. In his book *How Forests Think*, ethnographer Eduardo
Kohn immersed himself in the forest culture of the Runa people of Avila,
a village in Ecuador’s Upper Amazon. The lives of the Runa are
fundamentally intertwined with those of dogs, puma and the forest, but
just as much with their relations to nearby postcolonial trading cities.
*How Forests Think* argues that if we want to learn to understand and
embrace the multispecies worldview, we need not only to attune ourselves
to other beings, but also tune into the way these other beings see us
and learn to understand their modes of perception and their language,
even though it might not look like language to us. It is only when we
see the world as an ‘ecology of selves’, where the self dissolves into a
larger ecosystem, that we can move on to what Kohn refers to as an
‘anthropology beyond the human’.

*‘We’re all – trees, humans, insects, birds, bacteria – pluralities.
Life is embodied network. These living networks are not places of
omnibenevolent Oneness. Instead, they are where ecological and
evolutionary tensions between cooperation and conflict are negotiated
and resolved. These struggles often result not in the evolution of
stronger, more disconnected selves but in the dissolution of the self
into relationship.’*[^7]

Together with anthropology and ethnography, the contemporary art world
today is increasingly turning away from the white walls of the safe
gallery spaces and diving deeper into the messiness of the world. Field
trips, excursions and meetings with people from all kinds of different
domains are an aid in opening up our view, in exercising the ‘arts of
attunement’ and of becoming-other. Speaking with forest managers,
hunters, owners of tree plantations and biologists, or to local or
indigenous communities, spiritual healers and farmers helps us to
immerse ourselves in the strange worlds of ‘more-than-human beings’ with
which we share the world. This immersion in the multispecies
understanding of the world is all but a naive retreat into a pristine
and bygone idea of returning to the wild but takes into account the
history, the formation of landscape, the ferality of life today where
man-made interference and nature can no longer be set apart from each
other. We need to attune, learn and immerse ourselves in order to regain
the feeling of belonging to the Earth, which is the first step in
establishing new forms of caring.

Multiple projects in this publication dive further into these thoughts,
asking the question how we can enable and enhance the creative potential
of these techno-nature intertwinements, setting up new becomings of
roots, trunks, branches and leaves, between spirits of trees and
databased trees.

## References

- Ginet, J., ‘Contribution historique à l'étude de la greffe du noyer en
Dauphiné’,

- *Revue de Géographie Alpine*, no. 19-1, 1931, pp. 187-198,
<https://www.persee.fr/doc/rga_0035-1121_1931_num_19_1_4564> (accessed 3
March 2021)

- Haskell, D. G., *The Songs of Trees: Stories from Nature's Great
Connectors,* New York, Penguin Books, 2017.

- Kohn, E., *How Forests Think: Toward an Anthropology Beyond the Human,*
Berkeley, University of California Press, 2013.

- Morton, T., *Being Ecological*, London, Penguin Books, 2018.

- Press, C., ‘A tree stump that should be dead is still alive; here’s
why’, *Phys.org*, 7 July 2019,

- <https://phys.org/news/2019-07-tree-stump-dead-alive.html> (accessed 3
March 2021)

- Provinciaal Natuur Centrum, ‘Merkwaardige Bomen’,
<http://www.provinciaalnatuurcentrum.be/bomenabc>, (accessed 3 March 2021)

- Tsing, Anna L., *The Mushroom at the End of the World: On the
Possibility of Life in Capitalist Ruins*, Princeton, Princeton
University Press, 2015.

- Tsing, A. et al. (eds.), *Arts of Living on a Damaged Planet. Ghosts and
Monsters of the Anthropocene*, Minneapolis, University of Minnesota
Press, 2017.

- Wohlleben, P., *The Hidden Life of Trees: What They Feel, How They
Communicate*, London, HarperCollins Publishers, 2017.

[^1]: A. Tsing et al. (eds.), *Arts of Living on a Damaged Planet*, p.
    M18.

[^2]: D. G. Haskell, *The Songs of Trees: Stories from Nature's Great
    Connectors,* New York, Penguin Books, 2017, p. 84.

[^3]: Ibid., p. 97

[^4]: Geologists have proposed the Anthropocene as a new geological era,
    after the Pleistocene or Holocene, where new and purely man-made
    materials are present in geological layers (e.g. plastic or
    radioactive waste materials like trinitite).

[^5]: See also ['We are all Earth']({filename}./We-are-all-earth.md) by Karin Ulmer.

[^6]: A. Tsing et al. (eds.), *Arts of Living on a Damaged Planet*, p.
    G13.

[^7]: D. G. Haskell, *The Songs of Trees: Stories from Nature's Great
    Connectors,* p. viii.
