title: We are all Earth
author: Karin Ulmer
summary: Karin Ulmer has worked for more than 20 years with Brussels-based civil society organizations engaging in advocacy and lobbying EU institutions on policies related to sustainable food systems, agriculture and trade, land and seed rights. Karin is a member of the photo collective [tetebeche.eu](https://tetebeche.eu). ‘We are all Earth’ is written in her new capacity as independent consultant and will be part of her forthcoming photo-note-book in 2021 titled [homo situs]. As an advocate in rights related to sustainable food systems, agriculture and trade herself, Karin Ulmer invites the lecturer to pause on themes like the effects that humans have had on nature and activities with whom we could collaborate with nature’s functioning. These themes are established with theories of different authors, like Hannah Arendt, Anna Tsing, and Bruno Latour, to name a few. K. Ulmer activates us to become Earth lawyers for a world where both nature and humans can profit from each other.
license: fal
Date: 2020-01-02

# We are all Earth:

# Why we must all become Earth&nbsp;lawyers, enjoy the dance of&nbsp;freedom and embrace open-pollinated&nbsp;futures

## Rights of Nature: The Future We Want&nbsp;[^1]

In international political fora, rights of nature are introduced in a
patchy way, here and there. Reference is made to Pacha Mama, Mother
Earth, for example, in the constitution of Ecuador and in a universal
declaration in Bolivia. River rights were introduced in Ecuador,
Colombia, New Zealand and India as well as in Ethiopia, Nigeria and
Mexico, and in California and the Great Lakes region in the US. The
4<sup>th</sup> International Rights of Nature Tribunal in Bonn in 2017 looked at
Earth jurisdiction, working with Earth lawyers who seek to empower
Nature and thereby empower communities.[^2]

The recognition of the Rights of Nature originates mostly in the
struggle of indigenous peoples for their livelihoods. As custodians of
biodiversity, they understand the ecosystems they depend on. The
identity of indigenous peoples is built on enlarged boundaries,
recognizing the rights of plants, land, rivers and mountains. They
embody the knowledge of their ecosystems. Their struggles for the land
of their ancestors are clearsighted and foresighted, fighting over the
means of habitable soils, halting the decline of ecosystems, and
sustaining nature's reproductive rights.[^3]

Indigenous people know what it means to live on devastated land, on 'the
ruins of capitalism'. The international Earth Tribunals invite us to
listen to the suffering of those who 'know the world progress has left
us'.[^4]


## The killing of the custodians

Land environmental rights defenders are being threatened and killed in
ever higher numbers.[^5] While custodians of biodiversity and
open-pollinated futures[^6] suffer, the modern industrial machinery
continues to homogenize and annul the reproductive powers and fertility
of nature.

Open-pollinated plant varieties are genetically diverse and remain
reproducible on the other. They are bred *in situ* (in their natural
habitat) and are often protected by *sui generis* laws that allow for
open pollination by insects. By contrast, industrial seed companies
offer uniform varieties that are registered in national catalogues and
protected by intellectual property rights (plant variety protection, or
patents). Hybrid seeds are non-reproducible yet generate higher yields.
They are bred to comply with distinct, uniform, stable (DUS) criteria, a
precondition to be sold on the market.[^7]

The stakes are high. The EU seed market is valued at about 8 billion
euro. The EU is the largest seed exporter worldwide in a highly
competitive market.[^8]

## Enclosure

Anna Tsing (2015) calls the Anthropocene[^9] the
'plantation\[o\]pocene', a false universality. She narrates our relation
to nature back to the establishment of plantation agriculture (sugar,
tobacco, cotton) in the seventeenth and eighteenth century that led to
the establishment of slavery. Rather than waiting for seeds to germ and
for seedlings to grow, reproduction on plantations was 'reinvented': by
scaling the crafting of dwarf plants in the soil, by tossing fertilizer
and pesticides at it; to which in turn they react by panic growth. Tsing
characterizes this 'innovation' as plants reproduced under coerced
(slave) labour. In response, soil fungi transformed when fungicide use
in plantations lead to newly created virulent varieties and pests became
microbe-resistant. Tsing concludes that plantation farms have shaped our
modern relations between humans and plants and animals and nature, now
perceived as natural. By now we have been disciplined or colonized by
plantation farms.

## Radical analysis and local gravity fields

To revisit our relation to the rights of nature, radical analysis can
help. Radical comes from 'rac-ine', being rooted, related and belonging
to a 'site', a land. To be rooted is to be resilient, to be prepared, to
be neither corrupted nor hijacked, nor to be bought up. Radical analysis
can help to understand that open futures depend on adherence to nature,
to organic matter and genetic diversity.

![1-bubble chamber-ii.jpeg]({attach}We-are-all-earth-media/image2.jpg){: .front .image-process-front}
:   [^20]

According to science, physical connection to a local gravity field is a
given. Kuhlmann, a philosopher and physicist, says that without a body,
organic matter cannot form chemical compounds. Without a form, carbon
and organic matter cannot adhere and no biochemical connections can be
made. Kuhlmann believes that *the basic constituents of the world are
neither particles nor fields, but certain structures or bundles of
properties that cannot exist independently of body and form.*[^10]

Indigenous people know that without land, they are no more. They know
their livelihoods depend on the adherence to a body, an ecosystem where
there exists a structure or a bundle of specific properties of plant
soil microbiota interaction that continues to generate habitats for
different life forms. Jonathan Horowitz, a shaman teacher, reminds us
that *We are all indigenous to the Earth. We are Earth*.[^11]

## Critical Zones and Terrestrial site

Bruno Latour, a French philosopher of science, offers a radical analysis
of the history of modernity and its tragic disconnection from Earth.
Modernity has deprived nature of all its rights, whence we ventured
voluntarily and aggressively destroyed our habitable space, our
'terrestrial site' heading for the universe, for Mars.[^12]

*Critical Zone* is a recent exhibition curated by Bruno Latour. The
prologue to the catalogue fabulates: *You want me to land on Earth? Why?
– Because you are hanging in midair, headed for crash. – How is it down
there? – Pretty tense. – A war zone? – Close: a Critical Zone, a few
kilometers thick, where everything happens….* The Critical Zone is a
12 km stretch below and above ground where life is regenerated by way of
weathering, water cycles, carbon and nitrogen cycles, eutrophication,
soil biology, atmospheric air circulation, etc. Whether we will survive
in the Critical Zone and become part of its regenerative or destructive
processes depends on our politics and chosen sciences.[^13] As Tsing and
others argue, the impact of human-made industrial progress and its
plantation agriculture is the beginning of the Anthropocene, which has
proven deadly to life on Earth and its atmosphere.

The Critical Zone is the matter where structures or bundles of
properties occur. The Earth is the body to which we humans intrinsically
belong. Gravity is the form to which we adhere. We must all become Earth
lawyers.

## Endless variations of forms

Plants are essential matter to habitable zones on Earth. Plants dance
with their environment, adapting in step with it and modifying it by
cooling the air, changing atmospheric carbon dioxide concentration
providing oxygen, making soil and altering the relative abundance of
biotic components. Open pollination by insects, birds, wind and other
natural mechanisms is the basis of all life forms. Spontaneous open and
free sexual reproduction is what *allows plants for a wild dance of an
endless variation in form, music and the degree of complexity in their
relationship with animal*s (Ingrenouille 2006).[^14]

Thinking like a plant, Craig Holdrege (2013) suggests *we could become
aware of how we place – or better, how we plant our ideas and actions
in-situ, into concrete situations in life*.[^15]

![1-glykogen-ii.jpeg]({attach}We-are-all-earth-media/image1.jpg){: style="max-height: 75vh" }
:   [^19]

## Resisting the machine

Shoshana Zuboff (2019) has analysed how, unnoticed, the machine has
taken over. She demonstrates how Google, YouTube and co. are about to
conquer the idea of what is real. However, they are only dealing with
levels of simulation of matter. They are just uploading space, a street,
simulating a sensor-projected traffic jam that may not be real when
checking the location on site.[^16] Zuboff calls on us to *push aside
the algorithms that discipline and colonise our bodies,* our futures,
the land we are living on. Zuboff persuades us to join the struggle to
halt disruptive practices that foreclose the future we want. *What
happens when they come for my 'truth'* \[my body, my desires, my
indigenous land\] *uninvited and determined to march through my self,
taking the bits and pieces that can nourish their machines to reach
their objectives? Cornered in my self, there is no escape*. \[…\] *If
life is a wild horse, then the digital assistant is one more means by
which that horse is to be broken by renditions. Unruly life is brought
to heel, rendered as behavioural data and reimagined as a territory for
browsing, searching, knowing, and modifying.*[^17]

Cultivating radical analysis, invigorating the spirit of freedom and
resistance can help to free us from plantation\[o\]pocene and its new
disciplines and colonization.

## Unruly dance of freedom

Hannah Arendt reminds us that *all beginnings contain an 'element of
unique arbitrariness', related to natality as the accidental condition
of our birth. The meetings of our parents, our grandparents, and
progenitors are contingent or coincided events having no necessary
cause. That contingency is our price for being free, for being able to
experience freedom as beginning. The most important question is whether
or not our freedom pleases us, whether or not we are willing to pay its
price.*[^18]

Arendt's idea of new beginnings paves the way for new potential forms of
human agency, opposing the notion of rendering human beings passive or
superfluous. As demonstrated by Earth lawyers, we can start pleading the
case. We can become custodians of the rights of nature. We can peel off
the algorithm-sealing film that mediates our interactions with the
Other, the body, the Earth. We can embrace the open-pollinated futures
we want. We can adhere to the power of bonding and actualize our
capacity for freedom. We can become 'homo situs' and resist 'homo
imitates'.

[^1]: The title of the outcome document of the UN Conference on
    Sustainable Development, Rio de Janeiro, July 2012, see
    <https://www.un.org/ga/search/view_doc.asp?symbol=A/RES/66/288&Lang=E>

[^2]: *Fourth International (Earth) Tribunal for the Rights of Nature*,
    held at the COP23, November 2017, Bonn, see
    <https://www.earthjurist.org/new-blog/tag/COP23>
    *UN Resolution on Harmony with Nature*, December 2020, see
    <https://undocs.org/en/A/RES/75/220>

[^3]: See OHCHR Special Rapporteur on the Rights of Indigenous People.
    See EP Resolution on Violation of rights of indigenous peoples in
    the world, 3 July 2018, Strasbourg.
    See CLARA (October 2018), *Missing Pathways to 1.5 ºC: The role of
    the land sector in ambitious climate action. Climate ambition that
    safeguards land rights, biodiversity and food sovereignty.* Climate
    Land Ambition and Rights Alliance, see
    <https://static1.squarespace.com/static/5b22a4b170e802e32273e68c/t/5bc3cbf28165f51c6af2c7de/1539558397146/MissingPathwaysCLARAexecsumm\_2018.pdf>

[^4]: Tsing Lowenhaupt, Anna (2015), *The Mushroom at the End of the
    World*.

[^5]: See *Global Witness* (2020), Annual report reveals highest numbers of
    land and environmental rights defenders on record killed in a single
    year, with 212 people killed in 2019 for peacefully defending their
    homes and standing up to the destruction of nature, see
    <https://www.globalwitness.org/en/press-releases/global-witness-records-the-highest-number-of-land-and-environmental-activists-murdered-in-one-year-with-the-link-to-accelerating-climate-change-of-increasing-concern/>.

[^6]: Open-pollinated futures: term used in this essay; refers to the
    sexual reproduction of plants produced by seeds that have resulted
    from natural pollination of their parent plants. Open pollination
    occurs by insects, birds, wind, humans or other natural mechanisms
    resulting in more genetically diverse plants. By contrast, hybrid
    pollination is a type of controlled pollination in which the seeds
    come from a different species (i.e. heter-o-sis: modification,
    deviation, increased performance).

[^7]: Ulmer, Karin (2020), *Seed Markets for Agroecology*, published by
    actalliance.eu, see
    <https://actalliance.eu/wp-content/uploads/2021/02/seed-markets-for-agroecology-act-eu2020.pdf>.

[^8]: See [www.euroseeds.eu](http://www.euroseeds.eu/)

[^9]: Popular theory of an epoch starting with the Industrial
    Revolution. The National Geographic, Resource Library says: ‘The
    Anthropocene Epoch is an unofficial unit of geologic time, used to
    describe the most recent period in Earth's history when human
    activity started to have a significant impact on the planet's
    climate and ecosystem’, see
    <https://www.nationalgeographic.org/encyclopedia/anthropocene/>.

[^10]: Kuhlmann, Meinhard (2015) *Was ist real? Quantenfeldtheorie*.
    Article in Spektrum Wissenschaft 1/15, p. 16–23.

[^11]: See [www.shamanism.dk](http://www.shamanism.dk/)\
    Daphne Miller, M.D (2019), *Uncovering how microbes in the soil
    influence our health and our food*.

[^12]: Latour, Bruno (2021), *Où suis-je?*\
    Latour, Bruno (2017), *Où atterrir – comment s'orienter en
    politique?*
    Latour, Bruno (2015), *Face à Gaïa*.
    Anders, Günther (1966), *Wir Eichmannsöhne*. After the experience of
    the mass killings by the Nazis, Anders talks about the '
    Weltmaschine', a technical-totalitarian state towards which we are
    drifting: The world becomes a machine. The world as a machine.

[^13]: Latour, Bruno and Weibel, Peter (2020), *Critical Zones: The
    Science and Politics of Landing on Earth*, quote from cover of the
    catalogue. Exhibition at ZKM, Karlsruhe, Zentrum fűr Kunst und
    Medien, [www.zkm.de](http://www.zkm.de/) .

[^14]: Ingrenouille and Eddi (2006), *Plant Diversity and Evolution*,
    see page xiii-preface.

[^15]: Holdrege, Craig (2013), *Thinking like a Plant*, page 171ff.

[^16]: Zuboff, Shoshana (2019), *Surveillance Capitalism*.
    In February 2020, Simon Weckert, a Berlin-based artist, used 99
    phones to trick Google into launching a traffic jam alert. Google
    Maps diverted road users after mistaking cartload of phones for huge
    traffic cluster, see <https://www.theguardian.com/technology/2020/feb/03/berlin-artist-uses-99-phones-trick-google-maps-traffic-jam-alert>

[^17]: Zuboff, S. (2019: p. 268, 290, 444).

[^18]: Hannah Arendt (1906–1975), *Denken ohne Geländer*. Quoted in:
    Canovan, Margaret (1994) *Hannah Arendt: A reinterpretation of her
    political thoughts.*

[^19]: The power of bonding: see drawing/sketch of glycogen molecule;
    sometimes thousands of atoms fuse to form giant molecules that form
    branched chains – composed of hydrogen, carbon and oxygen atoms.

[^20]: Bundle of properties adhering to a body or form: see image of a
    quantum physical collision processes; depending on the mass and
    charge of the particles, their tracks in an external magnetic field
    are compressed to different degrees according to images of European
    Bubble Chamber-CERN; (taken from Kuhlmann article, see footnote 10).
