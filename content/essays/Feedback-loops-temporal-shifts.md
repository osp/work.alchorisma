title: Feedback Loops, Temporal Shifts
authors: Isabella Aurora, Artyom Kolganov
summary: The logic of late capitalism is inbred from an actualization of archaic methodologies of domination. It shows practices analogous with those of classical schools of magic. In this experimental text, the artists co-create an enchanting essay that combines critical theory of contemporary economies and technologies applied to hypersigilic magic and the history of neural networks.
license: fal
Date: 2020-01-01

# Feedback&nbsp;Loops, Temporal&nbsp;Shifts

![alchorisma.png]({attach}Feedback-loops-temporal-shifts-media/image2.png){: .out .image-process-out}

## Meditations on Magickapitalism

![treemiasm.jpg]({attach}Feedback-loops-temporal-shifts-media/image1.jpg){: .front .image-process-front}


![disruptmodernism.png]({attach}Feedback-loops-temporal-shifts-media/image3.png){: .out .image-process-out}

Modernity has always been synonymous with rationality. Max Weber
famously defined modernity as the ‘disenchantment of the world’. He was
referring to a successively consolidating secularized, progressive and
institutionalized society that replaced a chaotic social whole. In the
meantime, a teleological idea of society that could be split into
institutions not only resulted in the modernist belief in all sorts of
progress, but sublimated the incapacity to deal with an inner
irrationalism of the social, inscribing it in structures and
hierarchies. By producing grand narratives, modernism itself became a
grand narrative – probably, the most significant one.

![modernismbecameagrandnarrative.png]({attach}Feedback-loops-temporal-shifts-media/image4.png){: .out .image-process-out}

The disruption of modernism, popularly described as the crisis of grand
narratives, highlighted the limits of such a representation, showing
that the world is not equal to the sum of its institutions. However, the
process of reverse mystification left a double disenchantment
overshadowed. What came to replace a modernist idea of the world is not
the return of social complexity, but the logic of late capitalism. As it
transgressed its institutional structure, it became a dark potentiality,
underpinning thinking, cognition and imagination. There was no realism –
capitalism became capitalist realism.

![capitalismbecamecapitalismrealism.png]({attach}Feedback-loops-temporal-shifts-media/image5.png){: .out .image-process-out}

## Hypersigilia: Narratives and Speculation

In the capitalist dystopia, one is exposed to an overflow of
interconnected media creating a continuous marketing narrative that
directs their basic instincts to consumption as a vital component of the
perpetration of the species. In this context, branding is vital in
sourcing and converting buyers from the deep-sea network of individuals
into specific herds.

![freeimaginativespace2.png]({attach}Feedback-loops-temporal-shifts-media/freeimaginativespace2.png){: .out .image-process-out}

Sigil magic, on the other hand, is a practice derived from the Middle
Ages when symbols were used ritually to summon angelic or demonic
beings. Austin Osman Spare, a Chaos Magician, claimed that these
entities were merely complexes in the subconscious and turned this
practice on its head, focusing on the crafting of symbols with the
intention to actively create these entities. Ray Sherwin thus defines
the practice of sigilization: ‘*The magician acknowledges a desire, he
lists the appropriate symbols and arranges them into an easily
visualised glyph. Using any of the gnostic techniques he reifies the
sigil and then, by force of will, hurls it into his subconscious from
where the sigil can begin to work unencumbered by desire*.’ The sigil
must then be ostracized.

A connection between brands and sigils has gained sympathy among
magicians across cyberspace and some brands were even suggested by Grant
Morrison to be corporate sigils, or ‘super-breeders’. However, there is
one fundamental difference between these two: lifespan. Brands are
memetic by nature, i.e. made to replicate ad infinitum, whereas sigils
are made to be disposable and personal. But these concepts overlap when
taking into account a memetic sigil practice called Hypersigilia.

The term ‘hypersigil’ appears in Grant Morrison's work to designate an
extended cultural unit created with magical intent derived from sigil
magic. The hypersigil is defined in the Cyborg Anthropology wiki as a
term used to describe a feedback loop between an external or extended
persona and a primary self, a hologram, a microverse or a voodoo doll
‘*which can be manipulated in real time to produce changes in the
macrocosmic environment of “real” life*’.

![humblecorporatesuperbreeders.png]({attach}Feedback-loops-temporal-shifts-media/image6.png){: .out .image-process-out}

There seems to be a connection between the corporate super-breeding
sigils that feed off unbranded imaginative space and personalized
sigilia in the act of creation of virtual personas when the intent
within the composition of the online profile lies in the production of
value-oriented change and a quantitative influence on the offline world.
This sophisticated corporate-based form of hypersigil seems to fructify
particularly in ‘approval-oriented’ social platforms, highly absorbent
environments that create an imaginative void and fill it at the same
time with (biased) algorithmically filtered content.

The more memetic potential there is, the more hosts are found, and the
culture unit will then propagate and become an idea replicator that will
reproduce and ensure survival. *‘As with genetics, a meme's success may
be due to its contribution to the effectiveness of its host*.*’* The
insertion of a surplus value within a social network’s blackmirror hops
on this replication. Ostracism, however, is the death or crystallization
of an informational transfer, as diffraction threatens blockage to a
self-replicating unit’s hosting.

![freeimaginativespace3.png]({attach}Feedback-loops-temporal-shifts-media/image7.png){: .out .image-process-out}

A new cyborg breed is the testament of the overcoming of the
inconvenience of having a body combined with the joy of memetic product
placement: we see in the @ of lilmiquela a mind-boggling homunculus
creation of a young Brazilian-American virtual girl. In this case the
hypersigil breaks the pellicule between human and non-human
corporification, but intent prevails: this is a marketing stunt. Or is
it?

Corporifying a faceless algorithm with presence and response in the
cybersphere has created a highly replicant superbreed whose single
limitations are its own bodilessness which, in the end, does not
translate as a hindering but rather as an accelerated pathway to its own
survival. But this robot is incapable of ‘creating’ personal agency for
it has no means of formulating its own intention as an autonomous unit.
It is rather composed of a diligent conglomerate of servitors.

![free_imaginative_space1.png]({attach}Feedback-loops-temporal-shifts-media/image8.png){: .out .image-process-out}

## Perceptron: Imagination and Control

The structure of the social equally exposed to capitalism and
mystification was summarized by Gilles Deleuze in the concept of
‘societies of control’. The key principle of a control society is
modulation, and the mechanisms of control represent a complex system of
digital variables which are the variations of the same system
distributed on the basis of a code: *‘what counts is not the barrier but
the computer that tracks the position and effects the global
modulation’*. The more advanced the controlling machinery is, the more
it turns from tracking to prediction, while the inner principles of its
functioning are overshadowed by a visible complexity. We observe a
temporal shift: contemporary control is defined by preprocessing, that
is by prediction and prevention, while processing becomes fictional.

The description of power regimes in the society of control corresponds
to the general account of cybernetics as a study. The cybernetic
hypothesis represents the idea that a human behaviour is integrally
programmable and reprogrammable; in other words, it considers each
individual behaviour as something piloted by the need of the system.
Algorithmic technologies seek to make everything programmable, but their
capacity to reprogramme, to produce information on their own is
questionable as an algorithm contains a finite number of operations.
This is the reason to address the concept of a neural network.

![humanbehav.png]({attach}Feedback-loops-temporal-shifts-media/image9.png){: .out .image-process-out}

![Tree spirit]({attach}Feedback-loops-temporal-shifts-media/treespirit.jpg){.image-process-illustration--inline}

An artificial neural network is not an algorithm, but rather a framework
for many different machine-learning algorithms to work together and
process complex data inputs. So, NNs are learned to process tasks
without preprogramming or any task-specific rules. Moreover, NNs do not
possess any prior knowledge of the data being processed, they generate
needed characteristics from the learning material. The structural
principle of an artificial neural network resembles the neuronal
structure of a human brain, and the objective of a neural network is to
solve tasks in a way that a human brain would.

![free_imaginative_space1.png]({attach}Feedback-loops-temporal-shifts-media/image8.png){: .out .image-process-out}

The history of neural networks ascends to a debate between two paradigms
in the research of AI: one based on symbolic deduction, the other rooted
in statistical induction. The prototype of the first neural network, The
Perceptron, was created in 1957 by Frank Rosenblatt, a computer
scientist, as a model of a vision machine which could not only recognize
patterns but also learn how to recognize patterns by calculating a
single file instead of using multiple ones saved in the memory. This
invention has marked the transmission to a connectionist paradigm in AI
research which was a radical step forward as the machine could induce
information rather than give the output based on statistical data. The
connectionist paradigm remains dominant in neural computation up to
date, but to what extent is this mode of intelligence ‘artificial’?

The answer is implicitly inscribed in Rosenblatt’s commentary on his own
invention. He remarks that ANNs ‘*correspond to parts of more extended
networks in biological systems and* *represent extreme simplifications
of the central nervous system, in which some properties are exaggerated,
others suppressed’.* That is, ANN is only able to imitate certain parts
of the neuronal system of a brain while the general simulation is
distorted. So the capacities of an NN are still algorithmically limited.

![alchorisma_phrase_but_infuse.png]({attach}Feedback-loops-temporal-shifts-media/image10.png){: .out .image-process-out}

Another important aspect which shows the limits of NNs is the point that
the capacity of an NN to generate new data only lets it amplify the
existing forms of knowledge rather than create radically new ones. The
architecture of an NN is directed by the existing forms of human
knowledge, and respectively, an NN is subject to human intervention and
affect in its basic principles. Matteo Pasquinelli uses the term
‘augmented intelligence’ to replace the notion of AI because *‘the
super-human scales of knowledge are acquired only in collaboration with
the human observer’*. Therefore, AI is not able to automate strong
abduction and requires a human input which provides the locus to power.

![alchorisma_full_phrase.png]({attach}Feedback-loops-temporal-shifts-media/image11.png){: .out .image-process-out}

What is the possibility of political imagination in this situation? If
in political terms control means prediction and prevention, human input
is what shapes the control as predictable and generates the expected
forms of output. So, in terms of control as prediction, automation –
i.e. the transition from augmented intelligence to AI – is supposed to
envision non-prefabricated and uncontrolled future scenarios: ‘we
haven’t seen anything yet’, while human input leaves less space to
unpredictability but may invest more in an emancipatory agency. To
paraphrase Mark Fisher: from a situation of total predictive
accountability in which nothing seems possible, everything can suddenly
be possible again.

We are reunited. \[We look at ways to infect existing algorithmic models
with positions that acknowledge the importance of coexistence with
non-human entities.\]

We have been infusing intent into the concept of Alchorisma for three
years. Gathering around this invented or rediscovered concept can be
seen as the creation or discovery and activation of a microverse.
Infusing it with intent and labour charges the operation. Iterations
actualize it.

![alchorisma_full_phrase2.png]({attach}Feedback-loops-temporal-shifts-media/image12.png){: .out .image-process-out}

(…) '*The things we choose to place on the internet reflect and
magnify the awareness of self to ourselves and those around us. The
hypersigil is a sigil extended through the fourth dimension.'*[^1] This
bundle of storylines in unity performs a feedback loop of intent back
into cyberspace:

> from whence it dwelt  
> in time is dealt
> ![maythispublicationbejoyful.png]({attach}Feedback-loops-temporal-shifts-media/image13.png){.image-process-illustration--inline}
>
> As it once was so it shall become  
> as It became so it is cast:
> ![alchorisma_full_phrase3.png]({attach}Feedback-loops-temporal-shifts-media/image14.png){.image-process-illustration--inline}

[^1]: Cyborg Anthropology wiki, written by Caseorganic

<style>
  blockquote img{
    margin-top: -80px;
    margin-bottom: -80px;
  }
</style>
