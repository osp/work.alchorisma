Title: Spell Generator
Authors: Maeva Borg, Isabella Aurora
Summary: SpellGenerator is an automated hexing device coded with JavaScript. Developed by Maeva Borg, the script generates phrases containing new callings/rituals/spell-castings to call all entities (human or non-human). In a basic ritual/spell, there is a calling and also a demand, a wish. To create the script, a database was compiled of witching and magical terms separated into nouns, proper nouns and verbs. A second file was also made containing JavaScript libraries that enable the code to detect sentences or grammar forms (nltk, RiTa, tracery). Finally, the script was asked to read this database to generate spells. SpellGenerator was integrated in a performance by Maeva Borg and Isabella Aurora during the Alchorisma worksession at Z33. It is presented here as an online project generating ever-new forms of automated spells.
Sourcecode: https://gitlab.constantvzw.org/alchorisma/spell_generator

<div class="iframe-wrapper">
<iframe src="code/spell-generator/index.html" /></iframe>
<a class="vines-button" href="code/spell-generator/index.html" target="_blank" data-role="fullscreen">Spell Generator</a>
</div>
