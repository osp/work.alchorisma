Title: Attunement Generator
Authors: Malgorzata Zurada, Ils Huygens, An Mertens
Summary: This generator was created as a psychomagic tool. Based on a question for which you seek advice, it offers you different possible spiritual practices. This tool was created in the context of an art project as a hypothesis in a laboratory combining different paradigms and exploring possibilities. It is a first prototype. Feedback is welcome via this form.
sourcecode: https://gitlab.constantvzw.org/alchorisma/attunement_generator

<div class="iframe-wrapper">
<iframe src="https://alchorisma.constantvzw.org/attunement_generator/"></iframe>
<a class="vines-button" href="https://alchorisma.constantvzw.org/attunement_generator/" target="_blank" data-role="fullscreen">Attunement Generator</a>
</div>

Disclaimer: This is not medical advice. If you wish to use this tool on other humans, trees or more-than-humans, please ask for their permission first.

Thanks to: Annelies Clerix

The generator made use of the following sources:

-   Chakra Healing and Karmic Awareness, Keith Sherwood, Llewellyn Publications, US, 2005
-   Chamanisme Celtique: Ces arbres nos maîtres; Gilles Wurtz, Editions Trédaniel, Paris, FR, 2018
-   Futhark: A handbook of rune magic, Edred Thorsson, Weiser Books, San Francisco, US, 1984
-   All images are taken from Wikimedia Commons, <https://commons.wikimedia.org> and <https://www.pixabay.com>
