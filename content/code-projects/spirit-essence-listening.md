Title: Spirit essence listening session
Author: RYBN.ORG
Summary: Spirit essence listening session was made using a hybrid programme of Python and Pure Data involving a deep learning procedure that aims to analyse a series of images taken from a specific being – e.g. a tree, a forest, stone textures – in order to ‘extract’ the spirit essence of this specific element. The result is interpreted in sound layers. The process is based on the analysis of a convolutional network and involves the observation of uncanny patterns emerging in the training phase.
Sourcecode: https://gitlab.constantvzw.org/alchorisma/spirit_essence_listening_session


### IMG_20181203_104338
<audio controls>
  <source src="{attach}spirit-essence-listening-media/IMG_20181203_104338.opus" type="audio/ogg">
  <source src="{attach}spirit-essence-listening-media/IMG_20181203_104338.mp3" type="audio/mp3">
</audio>
![]({attach}spirit-essence-listening-media/capture_IMG_20181203_104338_resolution2000.jpg){.image-process-illustration--inline}

### IMG_20181203_104343_BURST1
<audio controls>
  <source src="{attach}spirit-essence-listening-media/IMG_20181203_104343_BURST1.opus" type="audio/ogg">
  <source src="{attach}spirit-essence-listening-media/IMG_20181203_104343_BURST1.mp3" type="audio/mp3">
</audio>
![]({attach}spirit-essence-listening-media/capture_IMG_20181203_104343_BURST1_resol2000.jpg){.image-process-illustration--inline}

<style>
  audio {
    width: 100%;
  }
  audio + img {
    max-width: 100%;
    width: 100%;
    margin-top: 0;
  }
</style>
