Title: Sounding stone ― an algolithic conversation
Author: Malgorzata Zurada
Summary: Sounding stone is a multiple-choice narrative that explores various ways of connecting with other-than-human entities. It is woven around an assumption that everything in existence possesses a unique frequency on which it operates most of the time. This vibrational signature carries the information about the entity and determines the way it starts resonating with others. The narrative branches into a maze of speculations about various methods for the interspecies and cross-dimensional data exchange. It draws on earlier research about frequencies and permutations initiated under the name ‘3-6-9’ by An Mertens, Mauro Ricchiuti and Malgorzata Zurada during the Alchorisma worksession at Z33 in Hasselt.

<div class="iframe-wrapper">
<iframe src="code/sounding-stone/index.html"></iframe>
<a class="vines-button" href="code/sounding-stone/index.html" target="_blank" data-role="fullscreen">sounding stone</a>
</div>
