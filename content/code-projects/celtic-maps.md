Title: Celtic map of trees
Author: Anne-Laure Buisson
Summary: ‘Celtic map of trees’ is a proposal to help one find a tree species carrying a specific spiritual value, as defined in Gilles Wurtz’s book, *Chamanisme Celtique: Ces arbres nos maîtres*. Besides offering guidance, this application also shows the limits of politics around publicly available tree databases.
Sourcecode: https://gitlab.constantvzw.org/alchorisma/celtic_map

<div class="iframe-wrapper">
<iframe src="code/celtic-maps/index.html"></iframe>
<a class="vines-button" href="code/celtic-maps/index.html" target="_blank" data-role="fullscreen">celtic maps</a>
</div>

Besides offering guidance, this application also shows the limits of politics around publicly available tree databases. In France, the [*Institut National de l’Information Géographique et Forestière*](https://inventaire-forestier.ign.fr/spip.php?rubrique90) produces and publishes the standard results of yearly inventory campaigns. The forest inventory follows a precise and well-defined protocol in the field to produce useful and relevant information for data production relative to French forests. A location is chosen, and a rectangle of forest around this point is visited and described in great detail: the number and condition of trees, but also the undergrowth, type of soil, etc.

In the attempt to open up this application to Belgium, no such overview database was found. The decision was made to focus on Brussels. The one more or less complete database was created by the [Plantations, fontaines et œuvres d'art department of Brussels Mobility](https://data.mobility.brussels/fr/info/trees/), which carried out an inventory of trees near all regional roads in Brussels. This database makes it possible to locate each tree in the region, but only if the tree is on regional land. This means that the trees are mainly located on the main roads in Brussels, along the major avenues and the ring road, for example. We don't know if these trees retain their powers when so close to traffic and so far from the forest. We suggest you go and talk to them.

For the *French* map, there are two ways to find a tree:

-   You can look for the plot with the highest concentration of the chosen species. This point is marked with a red circle on the map. In theory, if you go to this point, you will find an environment with a concentration of that species. The point also indicates the number of trees of that species counted on the plot.
-   You can look for the proportion of plots with at least one tree of this species for each French forest. This information is presented by a green background that is more or less intense depending on the proportion of plots containing at least one tree of this species in the area.

For the *Brussels* map, you can find the location of each tree for a given species, with its status:

-  lime green: living trees
-  grey: removed
-  orange: stump
-  red: dead trees

By clicking on the dot representing a tree, you can find out its species.
The source of the background of the maps can be found with Open Street Maps (Stamen Toner <https://stamen.com/open-source/>).

