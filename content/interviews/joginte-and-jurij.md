Title: Into the Forest (of Technology)
Author: Malgorzata Zurada
summary: A conversation with Jurij Dobriakov and Jogintė Bučinskaitė about technopaganism and related currents, (re)constructed pasts, and the current expressions of traditional knowledges on the Lithuanian contemporary art scene. The last time we met was during the 8th Inter-format Symposium on Rites and Terrabytes at the VAA Nida Art Colony in 2018, where Jogintė and Jurij were co-curators. That particular edition of the symposium had a research scope similar to the one of Alchorisma, and a focus on exploring the ‘practices of old’ in a digitally mediated post-contemporary world. Here Jogintė and Jurij talk about their views on the resurgence of pagan and shamanic motifs in contemporary culture in general and analyse why such themes have not only special prominence in Lithuania, but also a high level of complexity.
license: All photos were taken during the 8th Inter-format Symposium on Rites and Terrabytes, 20-24th June 2018 at Nida Art Colony.

# Into the Forest (of Technology)

A conversation with Jurij Dobriakov and Jogintė Bučinskaitė about
technopaganism and related currents, the (re)constructed pasts, and the
current expressions of traditional knowledges on the Lithuanian
contemporary art scene.

Jurij Dobriakov is a writer and curator based in Vilnius, Lithuania.
His research interests include the intersection between art and
technology, contemporary folklore, psychogeography and underground
cultures. Jurij has recently curated *Timestamps*, a retrospective of
Lithuanian new media art at the Lithuanian National Gallery of Art
(2021).

Jogintė Bučinskaitė works as an art critic and communication curator
in multiple projects of contemporary art, cinema and other fields of
culture. She graduated in Journalism and received her MA in Culture
Management and Cultural Policy. She is currently a PhD candidate in the
Representation of Contemporary Art at the Lithuanian Culture Research
Institute. Jogintė has also worked for the Lithuanian National
Television, edited publications and actively publishes various texts in
Lithuanian and international cultural media platforms.

 
![*Numinosum: Incubation* by Dovydas Korba at the 8th Inter-format Symposium on Rites and Terrabytes, VAA Nida Art Colony, Nida, 2018. Photo by Andrej Vasilenko ]({attach}joginte-and-jurij-media/1.jpg){.image-process-illustration--inline}
:  *Numinosum: Incubation* by Dovydas Korba at the 8th Inter-format Symposium on Rites and Terrabytes, VAA Nida Art Colony, Nida, 2018. Photo by Andrej Vasilenko 

**Malgorzata Zurada** Let's talk about technoshamanism, technopaganism,
technomagic(k) in the context of contemporary culture and of
contemporary art in particular. For the sake of clarity and conciseness
in our conversation, I will use the terms 'shamanism', 'paganism' and
'magic(k)' to talk about beliefs and practices that are rooted in
traditional lore and are connected, most often but not always, to local
contexts. I will use the terms 'technoshamanism', 'technopaganism' and
'technomagic(k)' to talk about practices that stem from traditional
knowledges but attempt to enhance, preserve or reimagine them through
the use of digital technologies. There are of course other ways to
understand these terms. Have you come across any? What would be your
definitions?

**Jurij Dobriakov** In Lithuania we see terms like ‘shamanism’ or
‘rituals’ evoked in the contemporary art scene increasingly often,
although they are probably used in relation to some generalized imagined
global spirituality rather than local tradition (for instance,
‘shamanism’ is not really applicable to Baltic pre-Christian religious
practices and must refer to something elsewhere). This interest is
mostly visible in various new materialist streams of art which have
evolved out of post-internet aesthetic and ideological sensibilities and
have recently turned towards what is commonly called ‘indigenous
knowledges’, ‘networks of care’ and so on. So in a sense it is a kind of
technopaganism inasmuch as digital technologies of production are
involved, but in the white cube reading it all seems to be very
metaphorical and neutralized, often pointing to practices but rarely
enacting them. Meanwhile, there are groups of people here involved in
linguistics, archaeology, ethnomusicology and mythology studies who are
trying to recreate or reinvent the ‘original’ Baltic pagan spiritual
tradition (they prefer to call it ‘the ancient Baltic faith’), and
although in many ways their practice is inevitably very modernized and
relies on digital means of communication, I doubt they would agree to
call it ‘technopaganism’ or anything similar. They want to make it look
like ‘the *real* thing’ as much as possible. There is little to no
communication between them and the contemporary art milieu. There is
also a lot of appropriation of traditional folk culture elements in the
heavily mediated popular culture and influencer domain, but that is a
different story.

**Jogintė Bučinskaitė** If we understand technoshamanism or
technopaganism as an attempt at reflecting on a historical trauma,
rebuilding the relics of traditions, or demonstrating a disillusionment
with the capitalist system - in the Lithuanian context, particularly in
the Lithuanian contemporary art scene, processes do not work like this.
I am convinced that we are under the influence of two forces. The first
is the entrenched myth of ‘Europe’s last pagans’, which seems to suggest
that we have a much stronger relationship with polytheistic beliefs and
nature. Among other things, all the conquests and occupations that
sought to undermine our traditions proved to be too weak against the
local dwellers’ determination to preserve their identity. If we are to
rely on these strong mythological and actually implemented narratives,
we could say that the link with the traditions has remained for the most
part unbroken – at least with those documented in the written sources.
However, for technopaganism to function here as a method of resistance
to capitalism today, we are still too much engaged in the process of
development and catching up with the West. Even if we think that over
the 30 years of independence we have formed a solid contemporary art
scene, winning Golden Lions and ensuring our artists’ international
visibility and acclaim – those are signs of integration into the global
art market, and conscious attempts at challenging or criticizing this
system with the help of traditional knowledge remain to be seen. It may
be that they will never come, simply because there will be no demand for
them. Perhaps that is why all the pagan rather than shamanic inclusions
in contemporary artworks Jurij has mentioned are often illustrative and
autoexotic, paradoxical as it may sound. We probably need a new, more
capacious term that would describe the approach of the artists working
here and employing such motifs in their work, which simply does not fit
in the definition of technopaganism due to its specificity.


![SALA (Audrius Šimkūnas) at the 8th Inter-format Symposium on Rites and Terrabytes, VAA Nida Art Colony, Nida, 2018. Photo by Andrej Vasilenko]({attach}joginte-and-jurij-media/2.jpg){.image-process-illustration--inline}
:  SALA (Audrius Šimkūnas) at the 8th Inter-format Symposium on Rites and Terrabytes, VAA Nida Art Colony, Nida, 2018. Photo by Andrej Vasilenko

**Malgorzata Zurada** I agree that such definitions may be imperfect at
best, especially when applied to contemporary practices that are more
often than not reinvented to some extent. Still, technoshamanism,
technopaganism, technomagic(k) are different terms that are often used
interchangeably to describe similar concepts and practices. Would you
even make a distinction between them, and if so, on what basis?

**Jurij Dobriakov** As mentioned, in the Lithuanian context they would
probably be understood quite differently. ‘Paganism’ would be the
closest to the actual local cultural heritage, which is indeed a big
thing here in terms of the country’s self-image and cultural narrative
(e.g. ‘the last pagans of Europe’). Meanwhile, ‘shamanism’ would
probably be understood most readily as referring to the spiritual
traditions of the Americas, Northern Scandinavia, Siberia, etc. – in
other words, as something ‘exotic’ and maybe even ‘New Age’ as opposed
to the ‘authentic’ local practices (as romantically constructed as they
often are). Meanwhile, ‘magic(k)’ would likely be associated first and
foremost with the particular Anglo-Saxon esoteric tradition, which has
some following in certain niche circles, but again not really related to
the ‘local’ animistic lore, where the emphasis on magic is not so heavy.
There is, of course, some intermixing and syncretism between these
spheres, much more evident in the art world or popular culture than in
the milieu of religion reconstructors, but the nuances are quite
pronounced.

**Malgorzata Zurada** How do the above concepts manifest themselves in
contemporary art or contemporary culture in general? Could you give
examples of art practices that you find noteworthy and are developed on
the basis of, say, ritual, divination, trance-states, etc.?

**Jurij Dobriakov** In recent contemporary art, there is a widespread
obsession with masks, amulet-like objects, ‘primal’ materials like wood
and fur, ‘traditional’ crafts like sourdough bread baking, fermentation,
or brewing. It is difficult to say how much actual research goes into it
and how much is speculative or purely imitational, but this sense of
‘ritualism’ is certainly there. There is also literal wording in the
spirit of ‘artists are shaman-like figures’. This has become very
evident in recent years. I would say, though, that a lot of it is still
very much aesthetic- and object-oriented rather than performative –
maybe that still has to develop, but so far I haven’t seen that many
actual rituals beyond the verbal in contemporary visual art. There is
more of that in the music scene perhaps. As for contemporary culture in
general, ‘folkish’ elements have been booming in design and branding in
the last five years or so, and this trend does not appear to be fading
yet.

![Sunset ritual/performance by Obelija at the 8th Inter-format Symposium on Rites and Terrabytes, VAA Nida Art Colony, Nida, 2018. Photo by Andrej Vasilenko ]({attach}joginte-and-jurij-media/3.jpg){.image-process-illustration--inline}
:  Sunset ritual/performance by Obelija at the 8th Inter-format Symposium on Rites and Terrabytes, VAA Nida Art Colony, Nida, 2018. Photo by Andrej Vasilenko 

**Jogintė Bučinskaitė** In addition, I would like to point out a few
more aesthetic and discursive trends evident in the contemporary art
scene. I have an impression that technoshamanism and technopaganism
manifest themselves here as an effort to imagine the Earth and the
humans after a total catastrophe. It's like an imagination exercise that
allows for creating anew the primal creatures, new species, communities
and their new rituals, and that is done with the help of already
existing examples of primitive, antique, or mythological art. You have
probably noticed the recent increase in the popularity of tribal
ornaments, images of devils and witches, clay, hair locks, chains, resin
and latex – such motifs are travelling from exhibition to exhibition. It
has become a standard language with certain safe gestures which enable
participation in the artistic communication processes. Surely, this
vocabulary will change after a while and will not necessarily be related
to shamanistic or pagan practices.

At the moment though, new visions of the old world are being created in
a pseudomythological way, when the aesthetic is cluttered with dirt,
chaos, and rudiments of unsustainable technologies, bearing witness to
the dreariest eschatological prophecies, while the discursive level
speaks of uncanny valleys, post-anthropocentrism, indigenous epistemes,
and the permanently updated political correctness as well as attempts to
come to terms with the existence of multiple ‘natures’. I find the Slavs
and Tatars collective to be the most productive in this discursive range
related to traditional knowledge, as they manage to capture the
paradoxes, similarities and differences of the various faiths and
traditions without exoticising the past or demonising the future.

![Traditional midsummer rites with spells and ritual foraging of herbs and flowers in the Valley of Silence near the Curonian Lagoon during the 8th Inter-format Symposium on Rites and Terrabytes, VAA Nida Art Colony, Nida, 2018. Photo by Andrej Vasilenko ]({attach}joginte-and-jurij-media/4.jpg){.image-process-illustration--inline}
:  Traditional midsummer rites with spells and ritual foraging of herbs and flowers in the Valley of Silence near the Curonian Lagoon during the 8th Inter-format Symposium on Rites and Terrabytes, VAA Nida Art Colony, Nida, 2018. Photo by Andrej Vasilenko 

**Malgorzata Zurada** Can you share what the Lithuanian scene looks
like regarding traditional knowledges and their current manifestations
as technoshamanic, technopagan or technomagic(k) arts, music, writing,
etc.?

**Jurij Dobriakov** I see these elements as most vibrant in the more or
less underground electronic music scene (drone, dark ambient,
post-industrial, etc.), where the intuitive ritualistic element has been
present for a few decades already. There is a whole, quite insular
micro-scene that actively infuses its sound with an evasive yet profound
sense of *genius loci* without resorting to actually quoting from
folklore. In other adjacent but distinct scenes like post-folk music,
the manifestations of traditional culture are more straightforward,
although often also more New Agey. Meanwhile, in the visual arts (except
for relatively ‘outsider’ artists closely connected to the community of
the ancient religion reconstructors), the preoccupation with
‘traditional’ themes seems to be based primarily on their specific
material qualities and association with some form of ‘otherness’ (a
long-standing currency in contemporary art in general), as well as with
temporary disillusionment with ‘Western values’, ‘Enlightenment’,
modernization and technofetishism of the previous decades. I think this
trend will mostly pass as soon as mainstream contemporary artists
discover new objects of mimicking and ‘othering’. Oddly enough, in
Lithuanian literature (other than the so-called ‘art writing’), these
themes do not have much presence.

**Jogintė Bučinskaitė** I find that the greater the emphasis on the
magical or ritual layer in artistic practices, the less art there is in
them, and vice versa. This is precisely why Lithuanian artists who
actively exploit these themes often fall into the trap of selling
uniqueness or esoteric, didactic, quasi-therapeutic practices relevant
to themselves or a narrow circle of people, often related to ancient
crafts: ceramics, yarn spinning, felting, brewing, or baking.
Occasionally these activities are accompanied by sophisticated concepts,
but we are always sceptical of artists’ attempts at appropriating
everyday practices by presenting them as ‘weird’ and distant from the
elitist art milieu. Yet most of these traditions are still alive and
practiced by the ‘common people’, thus such art projects lack deeper
reflection beyond the exploitative gesture.

Among the Lithuanian artists who are the most prominent explorers of
these themes or employ a kind of folkloric aesthetic, I would like to
single out the following ones. The practice of Žilvinas Landzbergas, who
represented Lithuania at the 57th Venice Biennale, has a very strong
element of nature-based magic and collective consciousness. His tendency
to use ornaments, motifs of wood, antlers, water, and even live birds
makes him an artist who probes the world through primitive means, trying
to bring us back to a magical fairytale. It is also important to mention
Anastasia Sosunova, who is increasingly visible in the international art
scene with her amalgamation of religious studies and present-day human
states and habits. Saulius Leonavičius meditates on contemporary rituals
and shamans, creating uniforms for them, setting up various kinds of
tents and baking acid pancakes. Indrė Šerpytytė makes abstract ornaments
based on the ancient Lithuanian woven sashes. Meanwhile, Juozas Laivys
has set up a cemetery for artworks in his homestead.

![*Vegan on Acid. THE HOTEL OF WORLD NATIONS* by Saulius Leonavičius, exhibition 2090 at Lokomotif, Lentvaris, 2019. Photo by Laurynas Skeisgiela]({attach}joginte-and-jurij-media/5.jpg){.image-process-illustration--inline}
:  *Vegan on Acid. THE HOTEL OF WORLD NATIONS* by Saulius Leonavičius, exhibition 2090 at Lokomotif, Lentvaris, 2019. Photo by Laurynas Skeisgiela

**Malgorzata Zurada** And if we were to mention particular artists
(musicians, writers) who actively use digital technology? How conscious
are they in their use of technology?

**Jurij Dobriakov** In the post-industrial music scene I mentioned, I
would distinguish artists like Sala, Girnu Giesmes, Oorchach, Skeldos
and Daina Dieva. I think they are quite conscious, but this
consciousness probably comes from some intuitive poetic and animistic
sensibility of the digital as an extension or transformation of the
analogue rather than in-depth theoretical reflection on the phenomenon
and philosophy of technology.

**Jogintė Bučinskaitė** All of the visual artists I have mentioned use
technology probably to the extent that a particular idea or the
expediency of producing a work demand at the time. Perhaps it is as
important as the ‘archaic technologies’ they seek to reflect on in their
projects.

**Malgorzata Zurada** Do you see any differences between Lithuania and
other parts of Europe when it comes to techno-shamanic/pagan currents in
art and music? Where does this distinctiveness stem from?

**Jurij Dobriakov** I see some difference in that in Lithuania there is
this popular perception or myth that we have a folkloric and spiritual
tradition that is somehow more ‘alive’ and ‘unbroken’ than in most other
places, particularly Western Europe, which is often seen as irreparably
tainted by Christianity, modernization, liberal values, and so on. So in
the case of Lithuania you could say that quite a big proportion of the
artists think that they are in fact continuing this ‘indigenous’
tradition, no matter how modern or syncretic the means they employ in
their practice. My feeling is that in some other parts of Europe there
is less emphasis on this kind of ‘authenticity’, and artists are more
comfortable with (re)invention or artifice as an inevitable element of
such approaches.

**Malgorzata Zurada** That's my observation as well. And how closely
are these artistic currents tied to contemporary folk religiosity and
pagan movements in Lithuania, for instance Romuva?

**Jurij Dobriakov** I don’t have any in-depth experience of Romuva[^1]
and similar reconstructionist communities, but from what I see, my
impression is that there is currently almost no communication between
them and the art world. For one thing, quite a few artists, particularly
from the music scene, used to go to Romuva’s summer youth camps, or even
sang and played in Kūlgrinda[^2], its central musical and ritual outlet.
But they seem to have outgrown this affiliation (though surely it must
have left some imprint on their later creative practice). Meanwhile, the
artists who are currently closely related to those reconstructionist
circles would probably not be considered part of the ‘in’ crowd of the
contemporary art scene that, despite all the recent ‘care’ discourse,
remains quite exclusive and self-conscious with regard to who belongs
and who does not. The ‘insiders’ of this field refer to and borrow from
folklore and ritual traditions via imagination or abstracted metaphor
rather than by going to researchers and enthusiasts seeking to
reconstruct ‘the real thing’. Maybe there are several reasons for that,
one being the strong association between those reconstructionist
practices and ethnic nationalism, another being the general preference
for inventive fiction over scientific or other kind of evidence in
today’s contemporary art.

**Jogintė Bučinskaitė** I have a pretty close relationship with the
Romuva community, and over the last 15 years I’ve seen how many
progressive peers of mine were raised by the Romuva summer camps. Most
of them today are active thinkers or researchers (historians,
philosophers, ethnologists, linguists). I also know a few directors and
photographers, but there are not many members of the Romuva community
who can be recognized as contemporary artists. Perhaps because the
contemporary art scene avoids literality and straightforward religious
connotations or discourses.

After all, Romuva itself can be considered as a creative project, which
required certain circumstances, knowledge and imagination to appear in
the late 70s.

**Malgorzata Zurada** To what extent are these traditions
(re)constructed, including by means of the new media?

**Jurij Dobriakov** In my opinion, the invented part is very big, and
I’m completely OK with that. Invention is inevitable when there is
limited information about ‘how things really were’. And the use of new
media is also very logical and very predictably prevalent if we think of
them as the new ‘vernacular’, something that works for us as ‘the tools
at hand’, like other means and materials worked for people in the past
(e.g., when metal replaced stone, perhaps it was perceived as a kind of
new medium). I see a problem in two cases: when people who definitely
engage in invention or construction to a certain extent (ancient faith
reconstructionists) try to disguise it by wrapping it in an aura of
historicity and unbroken continuity, and when other people who do not
lay any claim to ‘authenticity’ (contemporary artists) refer to folklore
and rituals in their practice utilizing new media, not out of some
genuine inner necessity or because it says and does something essential,
but merely because it fits the current trends. So I’d say the traditions
are inevitably constructed to a considerable extent in almost all the
forms in which today’s young and middle generations employ them, but the
important thing is how people deal with and acknowledge this. There are
(rare) cases where construction actually acquires some peculiar
authenticity precisely because it does not hide its use of new media and
does not imitate or directly quote from any kind of ‘true tradition’.
Rather, it channels some ‘here and now’ flow that is hard to pinpoint,
but has ritual quality based in the continuous present rather than in a
fantasized past.

**Jogintė Bučinskaitė** I agree with Jurij. Recently I’ve had plenty of
occasions to reflect on artists’ relationship with the rural, when they
move to work in the countryside or try to employ rural elements in their
practice. Hence, if we maintain that the countryside is, in a sense, a
repository of tradition, then it is intriguing to think about how
artists engage in applying, documenting, and pondering these old
knowledges.

One of the most interesting recent works is the film *Gustoniai in
Gustoniai* by Darius Žiūra. Visiting the same village over a period of
twenty years, the artist was capturing its inhabitants on video, taking
a one-minute-long portrait each time. To me it seems to be one of the
most consistent works dealing with a place and its change, which shows
the aging of not only the people but also the capturing technology
itself, while going to the same place and meeting the same people
becomes much more of a ritual than those we try to reconstruct.

One might say that Nida Art Colony captured the momentum of these
questions quite early with projects like* Climbing Invisible Structures.
Ritualized Disciplinary Practice*, in which the works pointed to a wide
range of ritualized practices, connecting performances executed by
people who lived ages ago, and whose activities can only be traced by
means of archaeology, to contemporary rituals. Last year the curator
Rado Ištok curated his exhibition *The Spectral Forest* here, which
referred to spectres, ghosts, and spirits traditionally residing in the
forest. One should also mention the 13th Baltic Triennial, entitled
*Give Up the Ghost*, in which the theme of newly constructed identities
was closely associated with the rejection of trauma, prejudices, and
outdated views. In other words, such mythological figures are
increasingly invoked as capacious metaphors for new rituals, which are
inevitably entangled with technology.

![Detail of the group show Timemaker at the Pamarys Gallery, Juodkrantė, 2020. Photo by Gintarė Grigėnaitė]({attach}joginte-and-jurij-media/6.jpg){.image-process-illustration--inline}
:  Detail of the group show Timemaker at the Pamarys Gallery, Juodkrantė, 2020. Photo by Gintarė Grigėnaitė

**Malgorzata Zurada** Technoshamanic or technopagan practices are
usually related – in one way or another – to what we can call 'root
cultures' or 'indigeneity'. How would you interpret indigeneity in the
context of contemporary culture?

**Jurij Dobriakov** It is a complicated question in the Lithuanian,
Eastern European context. Since much of Lithuanian history is marked by
some form of colonization-like experience (starting with the ‘last
pagans’ I mentioned), there is a tendency, particularly within the
romantic-nationalist milieu, to picture ethnic Lithuanian culture as
resiliently ‘indigenous’ (as opposed to Polish, Russian, Soviet,
currently even ‘Western’, ‘European’ or ‘global’ culture that has some
supposedly homogenizing and dominating power). This produces an
impression that Lithuanian culture (including its core traditional
values) was somehow ‘always here’, in this place, while other cultures
are perceived as migrant, colonizing, encroaching, placeless and alien.
Such a perception effectively excludes Lithuania from European, white,
colonial, hegemonic history, and equates it with other dominated,
colonized ethnic groups and cultures around the world. But a lot of
people outside Lithuania would surely contest this perception, because
globally ‘indigeneity’ has quite a different connotation and is usually
associated with non-white, non-European identities and narratives.
People within the art field predominantly subscribe to the latter
paradigm, which inevitably brings them into ideological conflict with
those who believe that Lithuania qualifies for that category of an
historically oppressed culture that is transcendentally entitled to its
‘native land’ other cultures attempted to occupy (hence the still
widespread wariness of the various kinds of otherness). I personally
would love to see and enact a notion of indigeneity that is not
deadlocked in this exclusive dichotomy – neither defined solely in terms
of the history of colonialism (though I understand its immense gravity)
nor connected to any kind of transcendental birthright, but instead
developed through very personal and immanent investment in the place one
finds oneself in, supported by the (re)constructed traditions that arise
out of a sense of connection, but are not insular or hostile.

**Jogintė Bučinskaitė** We follow the dynamic of this question in
different disciplines with great interest. While earlier it used to be
relevant to ethnologists, ethnographers, and archeologists engaged in
the search for, and substantiation of the ‘Lithuanian DNA’, currently
the topic of indigeneity is increasingly addressed by artists and
philosophers, whose focus transcends the boundaries of Lithuania. This
can be easily traced by looking at the thinkers quoted at the time. For
instance, one cannot but notice the current popularity of Eduardo
Viveiros de Castro and Déborah Danowski’s ideas, or Isabelle Stengers’
reflections on witchcraft and the neopagan Wicca movement. I think it is
one of the hottest topics at the moment, following the attempts to talk
about decolonization and deimperialization. I see the ongoing foray into
different cultures in contemporary art as an effort to resist the
dominant global art discourse. David Joselit writes about it in his
recent book *Heritage and Debt. Art in Globalization* (2020). It is
difficult to compare the different ‘root cultures’ or versions of
‘indigeneity’, because they were inevitably influenced by completely
different geopolitical circumstances, but it is crucial to understand
their diversity and peculiarities.

**Malgorzata Zurada** And what about nature? A symbiotic relation with
nature was at the core of pagan and shamanic practices, and so was a
non-linear perception of time or inter-dimensional communication. Would
you say the same applies to the shamanic/pagan practices of today in the
so called 'developed' world or not?

**Jurij Dobriakov** Nature is definitely very much fetishized in this
context, perhaps more than ever, because it has become a conscious
discourse rather than a natural habitat (if it ever was). Talking about
‘going back’ to nature is a value statement, and a very commodified one
at that. Again, there are different circles with differing perspectives
here, of course. The reconstructionist circles have adopted a very
romantic and essentialist understanding of nature, though their actual
lifestyles might be very urban. The contemporary art and philosophy
crowd, by contrast, emphasizes things like ‘natureculture’,
‘technoecologies’, the all-encompassing digital domain / the Internet as
‘second nature’, interspecies communication and the human as just one
part of the complex system of living and non-living things, though
recently it has also started flirting with such notions as animism,
(re)enchantment of the world, and so on.

**Jogintė Bučinskaitė** I think that the current fetishization of
nature is very much influenced by the volcanic eruption of the
Anthropocene discourse in contemporary art, the ecological crisis, even
Greta Thunberg. Curiously, the almost literal incorporation of nature in
works of art often translates into an even greater divide between the
civilized and the natural, few artists manage to avoid apocalyptic
nostalgia and find original ways of invoking nature instead. One of the
most thought-provoking Lithuanian authors in this regard is Aurelija
Maknytė, who co-curates her exhibitions almost in sync with nature,
finding intriguing parallels with cultural contexts instead of taming
it. Other artists also increasingly give prominence to natural materials
and integration of timeless motifs – Evgeny Antufiev is one such
example.

**Malgorzata Zurada** And what's the role of technology in all that?

**Jurij Dobriakov** In the latter case (the contemporary art and
philosophy crowd), technology is central, and one could even say that it
is perceived as an integral part of the expanded notion of nature.
Technology is employed by contemporary artists to contact, replicate,
construct or collaborate with nature. In the former (reconstructionist)
case, my guess is that there is a desire to create an impression of a
completely technology-free, primeval nature and to resist any
technological intervention in it.

**Malgorzata Zurada** In the context of Lithuania – or more broadly the
Baltic region – how are local traditions and beliefs related to the
political imagination? In what ways are they being used to justify or
create political narratives? I know it's a complex topic deserving a
whole other conversation, so let's limit ourselves to the sphere of
culture. Are there political implications for using traditional beliefs
in the field of art?

**Jurij Dobriakov** Within the contemporary art domain, traditional
beliefs seem to be almost completely depoliticized, or used in such a
way that strips them of any clear ethnopolitical and sometimes even
geographical associations. This itself is a political gesture, of
course. If we are speaking about the more ‘modernist’ forms of culture,
the motives for using traditional ethnic elements are usually either
purely aesthetic or emphatically romantic/nostalgic/’patriotic’, with
little if any critical deconstruction.

**Jogintė Bučinskaitė** It seems to me that ethnic symbolism in art can
easily serve as a litmus test for political views, particularly if one
carelessly manipulates solar signs (presented as Baltic) that inevitably
resemble the swastika. The same goes for the uncomfortable tradition of
Shrovetide celebrations in Lithuania, where people still dress up as
‘Jews’ or ‘Gypsies’. I do not mean to say that all traditions should be
censored, especially retrospectively, but they must be rethought if they
carry offensive, traumatic experiences with them.

**Malgorzata Zurada** You both were part of the curatorial team –
along with Andrew Gryf Paterson and Vytautas Michelkevičius – of the
8th Inter-format Symposium on Rites and Terrabytes held at
the Nida Art Colony in 2018. You gathered a diverse collection of
voices, works and thought currents there to reflect on the notions of
indigeneity and traditional knowledges in digital times. What are your
thoughts about the differences in how the participants approached the
topics of the symposium?

**Jurij Dobriakov** There was an obvious difference in how
representatives of the conditional ‘East’ and ‘West’ interpreted the
notion of indigeneity itself and who has the claim to it – I have
touched upon this problem already. Some of the participants even
emphatically reinforced this geopolitical and ideological split, which I
do not think was a productive strategy. Another major difference was
that some of the participants approached the topics and themes of the
symposium primarily discursively, while others (mostly artists)
contributed more tacit and intuitive knowledge and practice. In the end,
though I myself was part of the ‘theory’ camp, I found the latter to be
far more convincing and constructive. It established connections rather
than detected rifts.

![The group show Timemaker at the Pamarys Gallery, Juodkrantė, 2020. Photo by Gintarė Grigėnaitė]({attach}joginte-and-jurij-media/7.jpg){.image-process-illustration--inline}
:  The group show Timemaker at the Pamarys Gallery, Juodkrantė, 2020. Photo by Gintarė Grigėnaitė

**Malgorzata Zurada** Nearly three years later, have the approaches and
strategies you observed changed (globally and locally)?

**Jurij Dobriakov** At the time of the symposium, I would say this
interest in how traditional beliefs, folklore and rituals could gain new
relevance in present-day mediated culture was still relatively niche and
even rather marginal within the contemporary art field, at least here in
Lithuania. Now it has become mainstream, but at the same time it has
lost much of its critical currency, in my view. Especially because I am
not sure how many of the people professing their interest in it today
will still care for it in a couple of years.

**Malgorzata Zurada** I guess that's always the case when certain
trends gain momentum – and a new following. Time will tell.

**Jogintė Bučinskaitė** This experience also clearly demonstrated the
differences between us and the various participants – how uneven the
awareness of this topic is, and how multifaceted are the diverse
approaches that involve many different aspects of tolerance and equality
related to gender and race. I think it became an excellent panorama of
the widely varying attitudes that exist despite the fairly homogenous
globally-minded contemporary art field.

**Malgorzata Zurada** Are you aware of any other initiatives or
research projects similar to the symposium?

**Jurij Dobriakov** The most recent example is the announcement by the
Rupert centre for art, residencies and education, one of the most
prominent contemporary art institutions in Lithuania, of its 2021 theme:
‘Magic and Rituals’. It aims to employ these to ‘critically reflect on
sociopolitical enclosures’ and ‘foster rituals of care’. Here are a few
lines from their statement:

> With acknowledging the importance of rising questions and opening spaces
> for discussion, we also aim to provide answers. Answers that assure
> multiplicity of voices, encourage exploration and lauds playfulness.
> Every now and then abandoning the piercing gaze of a stringent
> researcher, we want to get our hands dirty and feel the body shivering.
> Increasing interest in magic and rituals opens possibilities to rebuild
> and reclaim worlds that have been neglected or wrecked. Feminist
> struggles, utopian techno imaginaries or animist ecology creates hazy
> pathways and promising alliances with defiant worlds of enchantment. It
> generates a potential to find ourselves in a situation where everything
> suddenly is permeated by magic. Where for a brief moment our senses are
> hacked and we are absorbed by the otherworldly.

This gives me a sense that their approach to the theme will be
performative and poetic rather than discursive and critical. But as they
promise a multiplicity of voices, I am curious to see who will feature
in their programme – for instance, will they invite someone from the
reconstructionist and romantic nationalist circles, the group that was
(perhaps deliberately) quite underrepresented in our symposium?

**Malgorzata Zurada** It seems such groups are rarely invited to the
contemporary art discourse. I would be interested as well to see how the
plurality of voices will be exercised in this case.

When you curate art that references traditional knowledges, what is
important for you? What do you look for, and do you have any specific
motivations?

**Jurij Dobriakov** My curatorial experience related to traditional
knowledge and its permutations is mostly limited to sound, particularly
the post-industrial, neofolk-inspired scene I mentioned: for instance,
putting together music compilations or moderating a group reflection on
the state of the scene. Within this field, I am looking first and
foremost for an evasive quality of being ‘chthonic’ without using any
direct explicit quotes from preserved actual folklore (this kind of
‘sampling’ tradition seems to me to be the most straightforward and
uninventive way of infusing artistic practice with a sense of place). My
motivation is to bring such oblique, understated practices into the
light without exoticizing or commodifying them, and instead highlighting
their value despite their possibly limited appeal. I like to position
them as ‘present-day’ folklore that becomes such without necessarily
trying to sound ‘folkloric’.

**Jogintė Bučinskaitė** I deal predominantly with thinking and writing,
thus I always feel drawn to original projects that reflect on these
themes in their own way. How they notice the idiosyncrasies of places
and people, the knowledge passed by word of mouth, and conceptually
invoke remoteness, old crafts, language, aesthetics and symbols not yet
rendered trite by the tourism sector, how they intertwine singular
contemporary experiences with inherited customs or transcultural archaic
motifs. I am interested in projects that undermine the seemingly
steadfast foundations of traditional culture, demonstrating that various
traditional practices can consist of minimal gestures, mundane daily
rituals and choreographies. I admire artists’ efforts to actually create
new systems of thinking and preserve a grain of the unknown beyond the
theoretical attempts at rationalizing everything.

![*From.Between.To* by Indrė Šerpytytė at the Vartai gallery, Vilnius, 2019. Photo by Laurynas Skeisgiela]({attach}joginte-and-jurij-media/8.jpg){.image-process-illustration--inline}
:  *From.Between.To* by Indrė Šerpytytė at the Vartai gallery, Vilnius, 2019. Photo by Laurynas Skeisgiela

**Malgorzata Zurada** To what extent does technology – or awareness of
technology – influence your curatorial strategies?

**Jurij Dobriakov** Since I have been interested in the intersection of
art and technology for most of my involvement in the cultural field, I
am inclined to look for instances of employing technology in creative
practice that combine intuition with self-consciousness and awareness of
the effects of technology that go beyond the merely aesthetic. I guess
that personally this technological aspect helps me to develop the
initial engagement with the work and then to see if it goes to some
territory that is in some way mystical or hard to explain but does
something to me almost on the subconscious level.

**Jogintė Bučinskaitė** I am interested in the forms of representation
of contemporary art that are inevitably related to communication and
information transmission technologies. It is always intriguing to think
about the intranslatability of certain artworks into a technology-based
language and, on the other hand, the successful transfer of all forms of
culture into all kinds of mediated formats.

**Malgorzata Zurada** And in what ways does technology influence the
thinking/awareness of artists working with the themes of traditional
knowledges?

**Jurij Dobriakov** I think at least some of them have developed a kind
of techno-mythology of their own. For example, Audrius Šimkūnas, the
founding member of the Sala project/band, systematically works with
field recordings in such a way that it becomes something more intricate
and charged with meanings than simply capturing sound via all kinds of
microphones. It starts to work indeed as a technologically augmented
form of divination, of ‘listening to the voice of Creation’ when done
with a particular consistent commitment and a certain ‘psychedelic’
perspective on the world and how technology expands and transforms the
experience of its manifestations.

**Malgorzata Zurada** That's exactly what I'm curious about.

**Jogintė Bučinskaitė** Speaking of the visual art field, I think that
artists who explore these themes try to avoid direct use of technology,
or at least it is not directly reflected in the works. They seem to give
preference to handicraft, thus trying to re-create the principle of work
itself, not just the idea part. Naturally, in video works the
technological imprint is inevitably visible, such as characteristic
‘dated’ graininess, or conversely, impeccably clean and pixelated image
with elements of augmented reality (e. g. in the video works of
Anastasia Sosunova). On the other hand, in Lithuania few artists
consistently employ computer technology in their work, and even if they
do that, their work usually follows thematic paths other than
traditional knowledge.

**Malgorzata Zurada** What I often observe is that in
techno-shamanic/pagan/magic(k)al practices, the technology – i.e. the
computer, the internet, the code, etc. – is used merely as a tool, a
medium to express traditional practices in a way that is more relevant
to our times. Sometimes the technology is simply seen as handier, and it
substitutes, say, a pen and paper or a musical instrument to create
sigils, mantras, rituals, etc. But there are also other instances where
technology is elevated and treated as having the equal ontological
status to that of its 'users'. Have you come across such artistic or
curatorial approaches?

**Jurij Dobriakov** Using technology like this simply out of
convenience or for the sake of its novel/advanced status is indeed
widespread. I enjoy it when artists use technology as something that
defines the world that we inhabit no less than trees, rivers, rocks and
so on, as something that has really acquired an extent of autonomous
existence already and can be equally ‘enchanted’, so to speak – meaning
that the ‘age’ of objects and phenomena is not the only possible measure
of their ‘magical’ quality. The above ritualized recording of the flow
of the world is perhaps an example of such an approach. When you think
of it, any technology of automated recording, be it a sound recorder
with a contact microphone or a repurposed CCTV camera (here another
artist, Vitalij Červiakov, comes to mind), has in itself an element of
magic, with all the accompanying mythology of making a cast of the
world, ‘stealing the soul’ of living beings, capturing ghosts, and so
on. I believe technology is capable of creating strange spectral worlds
through its very ambiguous relation to reality.

**Malgorzata Zurada** Obviously, we can find examples of people
benefitting from the techno-shamanic/pagan/magic(k)al practices. Can we
think of practices, or more specifically artistic practices, where
non-humans become beneficiaries (e.g. nature, ancestors)? Can art
contribute to making a change for other-than-human realms in current
times?

**Jurij Dobriakov** Again, this act of using technology to record
nature and other milieus, could we say that it ‘gives voice’ to
non-human entities, in a sense? Can it make them be heard and thereby
endow them with greater agency and prominence? I hope it can make people
care about their environment at least a bit more.

**Malgorzata Zurada** Hopefully so. I wonder, however. Are they
*really* given a voice, are they *really*
beneficiaries?

**Jogintė Bučinskaitė** I am sceptical of speaking for other entities
or trying to look at the world from their perspective. I think it is
always a speculative gesture that we feed through the prism of the human
consciousness in any case. It is difficult to tell whether art can
contribute to making a change for other-than-human realms when it is not
vital even to the majority of human inhabitants of the Earth. Being part
of the cultural field, we must be aware of our privileged position in
relation to not only the nonhuman entities, but also the marginalised
social groups. I would see it as a much greater achievement if art tried
to change the dominant perspective on such issues, and allowed us to see
the world not from the point of view of different entities, but from the
different positions of humans themselves in relation with their
environment.

**Malgorzata Zurada** Ok, so even if it's only a speculative
exercise: can techno-shamanic/pagan/magic(k)al art help assume or
cultivate other-than-human points of view?

**Jurij Dobriakov** I have no idea, to be honest. I am not sure such
points of view are at all attainable to us on a level that goes beyond
rhetoric or metaphor. But some forms of this kind of art can at least
make us more attentive to the world we find ourselves in and make us
recognize its vitality.

**Malgorzata Zurada** And to continue the notion of
*realness*. How often do you see techno-shamanic/pagan practices
– in the context of art – being 100 per cent real? And how often are
they treated metaphorically and practised purely for the sake of
aesthetics, politics, fabulation, etc.?

**Jurij Dobriakov** Yes, a lot of it is very metaphorical and
aesthetic, even fetishistic, sadly. On the other hand, if such practices
merely replicate, say, the rites performed by the members of Romuva,
then it is something other than art, or at least art that does go a long
way in terms of added value. I guess it has to be metaphorical to some
extent, but the sense of right proportion is key here. Very often, when
artists claim to replicate an authentic rite or ceremony (for instance,
build a temazcal in Vilnius), I wonder whether it doesn’t work as ‘art’
solely due to its perceived exoticism. I don’t find simple replication
or spatiotemporal transference of ritual practices to be enough. There
must be something else, some unexpected rupture.

**Malgorzata Zurada** Maybe we – as a global society – are detached
from nature (and our true selves), to such an extent that in order to
experience a ritual, or any form of transcendence, we need the mediation
of art/music/culture?

**Jurij Dobriakov** Actually, lately I’ve been feeling that art is
increasingly failing to do a proper job in this regard. By giving all
kinds of things special status and significance, it often tends to
achieve quite the opposite – making them banal, empty signifiers. In
contrast, things that are seemingly banal and routine, or things that
people do not do as ‘art’, can be much closer to rites and rituals and
therefore to the transcendental sphere. For example, walking with an
open mind, or just doing something small with great commitment and
enjoyment. This seems to be much more in line with nature, even though I
don’t think we can ever be fully ‘back’ to it.



**Malgorzata Zurada** Agreed. What I meant is that many people will not
be inclined to experience a real thing, say, a rite of some sort. They
will rather choose to go to a concert or a performance evoking the
ritualistic themes in a metaphorical way. It will feel more comfortable
and socially acceptable for them to remain an observer rather than
engage in something perceived as irrational and unscientific.

**Jogintė Bučinskaitė** I believe that art can become a pretext for
learning, recalling, or drawing attention to something. I have suddenly
realised that I forgot to mention another important exhibition in this
context that took place in Vilnius a few years ago – *Blood and Soil:
Dark Arts for Dark Times*, curated by Anders Kreuger and Julija Fomina.
It was a dense and simultaneously spacious show that expose the
extremely relevant dilemma between being grounded, living in direct
relationship with nature and its resources, inherited traditions and the
odd determination to follow them, and at the same time retaining the
cultural values and skills such as literacy, the power of ironic
observation or enjoying form.

In my view, an exhibition or another art form might not induce a
trancelike state or substitute an immediate ritual today, but it can
contextualise them and weave a peculiar mesh of meanings from different
elements.

![Still from *Gustoniai* in Gustoniai by Darius Žiūra, 2020]({attach}joginte-and-jurij-media/9.png){.image-process-illustration--inline}
:  Still from *Gustoniai* in Gustoniai by Darius Žiūra, 2020

**Malgorzata Zurada** Absolutely so. Do you think that such art
currents we're talking about here have transformative potential on other
than individual levels? If so, in what ways?

**Jurij Dobriakov** At least some of these currents help to build
collaborative communities, even if those communities are quite
individualistic and loosely connected. I guess they have this ‘tribal’
aspect in that they initiate or strengthen the bonds between people who
see and feel the world in a particular ‘transcendental’ way, who are
looking for microscopic escapes from the purely rational realm. Whether
such fluid and informal quasi-communities can activate broader
transformations is debatable, but maybe all of these micro-connections
added together do make the world a more hospitable place.

**Malgorzata Zurada** We already agreed that in recent years we've
seen a general rise in interest, creating, curating, and showing works
that reference various heterodox spiritualities, mystical currents,
witchcraft, indigenous beliefs, etc. It's visible also at the level of
the major shows like *documenta* or the *Venice Biennale*. In your view, what are the actual reasons for that other
than just changing trends?

**Jurij Dobriakov** The rise is simply massive. I would even say that
it is becoming a rare thing to see a contemporary art show without any
reference to those phenomena. It’s like a prerequisite now. My guess is
that, on the one hand, contemporary art is always in search of something
beyond itself that it could appropriate in one way or another, and
spiritualism, mysticism and ritualism just happened to be the next Other
it wanted to explore. On the other hand, contemporary art probably grew
tired of its own earlier fixation on the urban and the ‘progressive’,
turning to something attractively ‘backward’ – a similar process took
place in popular consumer culture with the whole ‘craft’ and ‘artisanal’
craze. Thirdly, for some reason, traditional culture has come to be
associated with psychological wellness, care, empathy and so on, perhaps
in parallel to the so-called ‘unlearning’ and ‘uncivilization’ currents
that seek to undermine the rationalist legacy of Enlightenment, and so
contemporary art incorporates it in its broader agenda of ‘resetting
modernity’ to a supposedly softer, more inclusive and sustainable
version of (nature)culture.

**Jogintė Bučinskaitė** I think it would make sense to mention the
special issue of the Lithuanian online art magazine Artnews.lt entitled
*Folklore Karaoke*, which we edited a couple of years ago. We included
there a Lithuanian translation of the American art critic Tess
Thackara’s article on the comeback of shamanistic practices in
contemporary art. Thackara claims that in the context of the
all-encompassing political, ecological, and economical crisis there is
an increasing longing for ‘primal’ communion, spiritual practices that
reconcile one with the environment, and a general sense of meaning,
miracle, and unity.

**Malgorzata Zurada** This definitely makes sense. I know some may
think such longing to be simply escapist, but what it could be instead
is something akin to an awakening of a spiritual survival instinct in a
world devoid of meaning.

To wrap up, would you like to share any of your upcoming projects, or
draw attention to any external sources (e.g. books, music, thought
currents, etc.) relevant for our conversation?

**Jurij Dobriakov** At the moment I am curating a retrospective of
Lithuanian new media art from the 1990s and 2000s. Looking back on it I
am surprised to see how much the perception of technology has changed
over this decade, having become so intertwined with animism and nature
today, whereas back then it seems to have been much more
technofetishistic and almost naively futuristic. Although I must
acknowledge that even in the mid 2000s in Lithuania there was already
this sensibility that drew parallels between the natural forest as a
Lithuanian favourite archetype of nature and the new ‘forest’ of
technology. Still, it was a very different approach compared to the
current technoshamanistic trend. Perhaps this shift deserves further
in-depth reflection.

**Malgorzata Zurada** Thank you for the conversation.

[^1]: <https://en.wikipedia.org/wiki/Romuva_(religion)>

[^2]: <https://en.wikipedia.org/wiki/Kūlgrinda_(band)>
