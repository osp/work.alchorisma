title: Frank Coppieters
author: An Mertens
summary: This interview took place in June 2018 in preparation to the worksession in Z33 in Hasselt. Frank Coppieters and his wife Kathy Melcher offer trainings in universal shamanism and Reiki initiations. Frank Coppieters Ph.D. is also the director of the [Living Light Spiritual Center](https://livinglightcenter.com) which he started in 1986 in Portland, Oregon. He is a native of Belgium where he taught graduate students full time at the University of Antwerp in Theory of Literature, History of Drama and Non-Verbal Communication. Frank works as an intuitive counselor, shaman and Reiki practitioner and always focuses on the empowerment of his clients and students. He helps people to access their inner teacher and to work creatively with the challenges that are happening in their lives.
license: fal
         Copyright: image of tree with Franck by Stephan Peleman

# Frank Coppieters

# Bois Le Comte, Villers-devant-Orval, June 2018

<!-- ![]({attach}Frank-Coppetiers-media/image2.png){.image-process-illustration--inline} -->

![]({attach}Frank-Coppetiers-media/image1.jpeg){: .front .image-process-front}

<section class="audio out">
<audio controls>
  <source src="{attach}Frank-Coppetiers-media/interview_frank_180622a.m4a" type="audio/mp3">
</audio>
</section>

## Futures

[0:39]

**An Mertens** How do you see the world in 50ys time? I ask this
question, especially because I'm interested in the perspective of the
shaman on the future.

[1:20]

**Frank Coppieters** From a shaman's point of view… There is a shaman
who often comes in our neighbourhood. He's very well known, Henk
Wesselman. He's an anthropologist. In one of his books I've read, it's
interesting. He has had shamanic visions, which put him on a path of a
future where the world as we know it has been destroyed, by probably
some nuclear disasters, maybe 300 years in the future. I thought, that's
really interesting. It is possible to have visions like that. That's
always been the basis of his work. I feel also in my nervous system that
there is a precarious aspect and many people feel that, of course. It is
not a given. The planet will go on for probably a few more billion
years, if the regular laws of physics hold, which I think they will. But
human life on the planet as we know it, may be extremely different. Say
already 50 years from now. It's possible, and that humanity is before
this momentous decision. How are we going to do this collectively? As
you know, there is already fighting around water. That's been going on
for a long time. The fight and the thinking about it started quite a
while ago. People who are really informed, they know what is coming.
Every single one of my masters would speak about it. Jóska \[Soós\][^1]
for example. He was often saying, because he was strongly connected to
the amphibians. They are very sensitive. When there is an amphibian that
is no longer able to live in the biosphere, you know that other species
are going to follow. And of course, the human beings are also species. I
was in New Delhi in December, it is hardly liveable. We were with
fourteen people who all had a shamanic interest, eight of us after one
day had some trouble breathing. In New Delhi, maybe 30% of the people
were wearing masks. That's an enormous amount of people. The situation
in that regard is really critical. I can easily, when I meditate,
envision a world where all of a sudden there is critical mass where we
see it, you know. Oh, why are we doing this? Why are we polluting our
own home? Animals don't do that. Why would humans think they can get
away with it? I think it will be a very difficult period of transition.
We will probably see the worst coming out. And also we will see the best
coming out. But it's not a given. I don't think anybody can know for
sure where it's going.

There is a whole thing about free will and all that. I wouldn't rule out
that there may be interventions from other galaxies, or that some
interventions are happening already. But I don't think that it will be
resolved for the humans in this way. Humans themselves need to learn and
see what the options are. I think there will be some sort of a respect
for that.

[7:15]

You know, I'm intrigued by the crop circles, like many people are. I
mean, there is some help with the channeling or divination. You can feel
there is, all the time when the heart is more open and the mind is
clear. During meditation, many people are getting clarity about the now
but also about the immediate future. I think some of that is really a
form of help. When I think about the Course in Miracles, you know.

[7:48]

\[The Course in Miracles\] is a channeled work. It was channeled in the
late 60s through a woman who was an atheist Jew and a professor of
psychology, a world renowned authority. It freaked her out, because she
was hearing a voice. Not necessarily a speaking voice but a voice that
she could hear in her heart. It started with ‘This is a course in
miracles, please take notes.’ She had visions prior to it. She ended up
writing this down for seven years. It is an amazing document, because
you can see there is, from the beginning to the end, a consistent
curriculum. There are lots of people who receive pieces of information,
but here you receive a whole course. It is also extremely beautiful at
times. Sometimes it came through in iambic pentameter, like in the works
of Shakespeare. It is very difficult to even do this if you were to try
to do it. And people who work with the Course really get to a new level
of consciousness.

That raises a lot of interesting philosophical or existential questions.
Where is this coming from? So, it is Jesus? But who is this Jesus who
gives this? Is that the same Jesus? You can’t get around phenomena like
that, or like the crop circles. When you really go into it, you see
there is something else that is present on this planet to help humanity
along.

And that is the one thing that connects shamans throughout the ages and
all over the planet, shamans have always felt that. It was and it is
their job to help to open the gateway for some people around them.
That’s been going on, as far as we know, forever.

[10:28]

Today, mostly when people talk about shamanism, I get as first question,
“What about ayahuasca?”. That has become the major question. Twenty
years ago, it was more like, how could you be a shaman if you’re not
born in a shamanic tradition. Initially it was mostly an anthropological
interest. It’s interesting that Mircea Eliade was the first one who
wrote a really scholarly work about it. From that work it became obvious
that this was a sacred practice. As he called it, techniques of ecstasy.
And that it was widespread. Prior to that, which often happens, for
example, when the Catholic missionaries went to Africa, they see this
culture, they think it is primitive. They don’t know anything about it
really. They certainly don’t get the depth of their religious practices.
Whereas the native people, also in North America, when the Europeans
come and they hear about Jesus, immediately they go, oh yes, I can see
this figure standing around you when you talk. Wow, what a powerful
shaman that must be. Because people live in those realities. They have
no trouble incorporating a figure like Jesus. Whereas the missionaries
are more coming from the mind, coming from dogma and ideology. They try
to convert somebody to their ideology. Shamans are not interested in
that at all. They just want to know, what can we do with this knowledge
to help the living, the mental and the spiritual conditions of the
people that we are responsible for.

## On ‘becoming’ a shaman

### Near-death experience


[12:53]

**An Mertens** How did you become a shaman?

**Frank Coppieters** I think in various steps, but I realize today –
and I have realized it for a while – that my near-death experience at 4
years old, was very instrumental. Maybe I was born as a shaman in a way,
with the destiny of a shaman. My mother would tell this very often. I
was born in a catholic hospital in Ghent. There were nuns there. They
would say, looking at me and holding me as a baby, he will end up in the
chambers – meaning in the ‘Senaat’. That was their thought about an
energy that they liked – I was apparently kind of ‘bubbly’ or
‘radiating’ or something. So they meant to say: ‘he will go far’. There
was apparently something present in that energetic field, strong enough
for my mum to really remember that and later talk about this a fair
amount.

When I was four, I had a near death experience. For about a day and a
half, I was in between this realm and another realm.

[14:12]

**An Mertens** Do you remember it?

![]({attach}Frank-Coppetiers-media/image10.jpeg){: style="float:right; max-width: 40%; margin: 0 calc(-1 * var(--pad)) var(--pad) var(--pad)" }

[14:15]

**Frank Coppieters** Yes, I do. Some of the memories became stronger
when I did rebirthing sessions. There was one memory that stands out. It
is the following. I must say I was very touched, because I could feel
how much my parents really wanted me to be here. I kind of knew, but not
to that extent. My older brother, who is five years older, has of course
much more distinct memories than I do. What stood out, was that the
awareness in me knew that I was not leaving Earth. It was a little
puzzling that grown-ups didn’t know that. It was not like a feeling of
superiority. It was more, why don’t they know that? Because I knew,
whatever this “I” was. And I do remember that I could feel the mother of
my mother. She had a devotion to Mary. She was not very much loved in
the family. She could be quite egotistical and domineering. But in that
perception I saw that what she was doing, was really helping me. I could
feel that. The anxiety of my parents, on a vibrational level, was not
helpful. It was touching me on the heart level, but it was not
practical. What my grandmother was doing, was practical. She knew what
she had to do. When I came back and it was clear that I was going to
live, I was a little different at kindergarten from the other boys. I
knew or I decided, I shouldn’t talk about this. I don’t think I had a
feeling that I needed to ask questions about this. But I just felt a
little bit different from the kids around me, having seen all that. Just
a little.

My other brother is three and a half years younger. Once he became maybe
five or six, I started sharing with him a bit. He is like my soulmate.
We were having from early on – I think I was 9 or 10 – very interesting
walks, the two of us, at the outskirts of Ghent where we lived. We
philosophized about the big questions. Why are we here? What is this
world about? Things like that. And this was also a secret. You can see
the seeking was starting.

### Carlos Castaneda

[17:46]

My reading of the book of Carlos Castaneda has been very instrumental. I
was fascinated, even though it has never been a branch of shamanism that
I would want to practise. There is something about it that I don’t like.
It is still connected with power, in the sense of, having power over.
But there are lots of interesting perspectives and insights and pearls
of wisdom in it. And I like very much the foolishness of Carlos
Castaneda, the anthropologist and then the wise teacher Don Juan who
always tricks him and tries to push him outside of his limited mind.

### Meeting the teacher

[18:41]

But then meeting my teacher, that was a very, very important event. I
could not have anticipated the consequences of that. It really grabbed
me when I saw him giving a demonstration. It was a recognition of
something. I think he always saw that I was a shaman. But he also knew
that I was not ready to see that, in the fullness of it. He was very
respectful, in the sense of not trying to… He allowed me to take my time
to grow into that. I think he saw my insecurity also, on many levels,
but also on that level, that I was not yet able to see it. When I look
back, I became a very serious apprentice. It took me about two hours and
a half to go from my home in Wilrijk to Brussels, where he was living
then, close to Schaarbeek. And two and a half hours back. But I was
there every week (laughs). And I always felt great, because typically I
felt quite disconnected. As a kid I think I was connected. But I found
the energies at my home also quite dense. My parents were sweet enough,
but they were anxious. We had this session about the war today in our
group… it was horrible, horrible, horrible.

### Dreams

[21:14]

Two dreams I remember from when I was young, that are very shamanic. One
is, I know the third world war is going to happen. People were talking
about it all the time. It was the period of cold war. And I know that
I’m totally incapable of killing another human being. I just cannot do
this. That’s where magical thinking comes in - I think I have found a
formula. I’m counting how many years it takes between the first WW and
the second. I must be quite young. Then I start counting from 1945 -
when will the third WW happen?, because it would be the same time
interval in my mind. Then I figure out how old will I be and will I have
to be a soldier? The anxiety increases. Then there is this thought which
I did not recognize until much later, I blame myself, why Frank did you
choose to be a man? If I had chosen to be a woman, I wouldn’t have to
kill anybody. And then at some point I fall asleep.

[22:42]

The other one, I find this even more interesting, in the dream a task is
given to me. The task is, what was there before time began? It is a
difficult question. So I’m starting to concentrate. I think it is a form
of meditation, in the dream. The closer I come, the more I break out in
a panic. At some point I start screaming, because I can’t hold the
question. Then, in real life, my father comes. He takes me to the
bathroom, he’s very gentle. He takes a ‘washandje’ (wash cloth), puts
some water on it and starts to put it on my face. After about twenty
minutes I calm down. I think this happened at least fifteen to twenty
times during a period of a year and a half. And of course, today, it is
my greatest joy during meditation. Time evaporates. But it is
interesting that already then, there was a preparation or a calling for
shamanism.

### A calling

[24:00]

And I have one more thing. At school I had catholic classes
(catechismus), and we went to church till I did my communion. In the
church the priest was talking about a calling. Apparently there were not
enough priests anymore. And I was so scared, because I thought, what if
I would be called. I thought, maybe I didn’t want to tell anybody,
because I didn’t want to become a priest. I kind of knew that. But I
also thought, if you have the calling, you have to do it… but if I don’t
tell anyone (laughs).

### Migrating

[15:13]

And then I think that immigrating to the US was part of the shamanic
process. I needed to go away, because the family and cultural
conditioning and my own self-consciousness was much too strong. So I
needed to be in a place where there was still a strong native vibration,
which is really the case; where I could start from scratch, and where
people were actually quite open on that level. I needed to go far away
from the parents, from academia, from all the conditioning.

### Out of body experiences

[26:07]

Another aspect is that I had a few out of body experiences, one of them
I put in my book. Just after I finished my Phd, I had a very strong
immediate out of body experience. As part of that experience, there were
two insights. One was on a personal level. My girlfriend at the time –
which I kind of knew but I didn’t want to confront that – we were not
going to stay together. We both knew that. It was painful, but that came
with such clarity. She was with me when I had that experience. The other
thing was more on a cosmic level. It was such a joke, that who I was,
was put in academia. I’m not a mind person at all. A good mind, but not
cerebral. I wasn’t doing drugs. There was just this big cosmic joke. It
was ok that I was there, but this was not my path. It was totally
obvious within the experience that this was not my path. There was a
knowing there, but there was no vision about what my path would be.
None. Absolutely none.

I could never ever forget that experience. It was so strong. In that
experience my psychic senses were totally open. Of course, I cannot know
for sure, but I felt the thoughts of all the people around me. There
were not that many people there.

**An Mertens** It happened in a church?

**Frank Coppieters** Yes, in Clermont-Ferrand. There seemed to be a lot
of things that led to this experience. Later I could put all those
things together. I was with my girlfriend and we couldn’t be together a
lot, because she was married. I was good friends with her husband. He
knew we were together. It was all friendly, part of the 60-70s. This was
the first time we had two weeks together, the two of us. And I was going
to go to the US for a year with a grant. I visited my best friends who
had just come back from the Osho ashram in Puna. I had never heard about
Osho then. It was 1977. I read one of his books on tantra. It made a
very deep impression. We said goodbye to my friends. I knew I was not
going to see them for a long time. We went to the festival of Avignon,
but we passed by Clermont-Ferrand. I had a bit of a headache. I take a
little aspirine (Troc), I go with my girlfriend in the church and I
started to meditate. I had been a meditator since I was 21. And
immediately, ‘pow’, really an explosion. It was a little bit
frightening, because it was completely out of control. It just happened.
But I’m watching. Then these revelations came. It lasted for a while.

[30:21]

**An Mertens** Can you describe what you saw?

**Frank Coppieters** It wasn’t so much a seeing. It was like the skin
was ripped off. I’m walking and perceiving in different dimension. But
people still look the same. It’s more on the level of psychic energy. I
know I can look through you, through whoever was with me. I knew from my
girlfriend that it was a little bit frightening to her. But at that
point I was very peaceful. My heart was very open.

Then we go out to a little café opposite. We sit on the terrace. Things
are at times extremely funny. I’ve never done LSD, but I guess it’s
similar. The waiter comes and he asks what we want. In that state I
couldn’t remember what ‘water’ was in French. And I go ‘ooooooooh’. And
then I realize that water is ‘eau’ \[ooooh\]. And that it is actually
what I was looking for. And then, all of a sudden, I get completely
frightened. I look around and I see that almost an accident happens. It
didn’t. But I was directed to it. It was a woman who was almost run
over. I knew it before it was going to happen. At that point it becomes
too much. I can’t handle this… I’m thinking I’m stuck in this dimension,
that it is going to go on forever. It’s just too much. I can’t eat. I’m
crying also. I feel a bit like a madman. I can see that if I look at
somebody now, they can see there is something going on with me.

Then we take the train from Clermont-Ferrand to Avignon. It was terrible
for me, as if I was going to explode. It was too much. I go from one
carriage to another. I remember there was one baby on the train. I would
look in the innocent eyes of the baby and I could breathe. I was also
totally aware of the circumstances and that I shouldn’t act too crazy.
After a minute or two I left (laughs).

That night I sweated like crazy. The next morning I was normal. I knew
that what had happened there, was an initiation of a sort. I knew that.
I shared it with my younger brother and the husband of my girlfriend.
That prepared me to start to open up to whatever my mission or task was
going to be.

### Chanting and integration

[33:58]

The shamanic part didn’t happen until I left the Osho community where
many things happened for me. I was again extremely open and vulnerable.
To give you an example, I would get out of the door where we were living
in Portland, I would walk, there would be a car and I felt that the car
was literally driving through me. I knew it was not a reality. But I had
to go back home, it was impossible to deal with this.

But gradually, that started to integrate. When I was chanting, listening
to the tapes of Jóska chanting, I could go from one realm to the other
realm. Like that. So I became addicted, you could say (laughs) in a good
way to chanting in my car. That was the only place I had. We were living
in a condo with neighbours, I couldn’t do it at home. That became my
training, to just chant. Even then, I had no idea that I was a shaman.
That came later.

[35:23]

Somebody invited me in Belgium to come and give a sound workshop. I
called it ‘Sacred Sound’. And the synchronicities you asked about, they
were happening all the time. Also with Kathy. And I started to know
people for whom synchronicities were also happening especially after I
took Reiki. That was the first time. And many other things. I started to
offer it to some people, and to my surprise, they loved it. Up until
then it had been my private practice. And Jóska had never said, Frank,
you should do this. For which I’m very grateful. Because if I would have
had a vision of where I would be teaching… noooooo. Not me.

[36:45]

Teaching in itself was always a natural gift for me. But going into the
unknown on behalf of other people, I didn’t feel that I was prepared for
that. That took a long time. And then shamanism became a regular thing.
In the US, when I started my practice, also because some people are
still connected to native practices, they recognized it in a way more
than I did. They would say, Frank, you’re doing shamanic work. And even
then. It is so strange how the cerebral mind can still take over. I
would say, no, I have not been trained. I truly believed that I had not
been trained, whereas of course, I had been trained. That split, you can
really jump from one part being in it to being outside of it. That
prepared me. Now, a big part of my work is trying to show people. Like
you, An, of course, you’re a shaman, whether you know it or not, you are
(laughs). And you do with this whatever you want, of course. At least
that is what we think. Because at some point, if you don’t do it, that
was certainly true for me; if I had not pursued it, I know I would not
be happy.

**An Mertens** That’s what they say.

**Frank Coppieters** Yes.

## Multidimensionality

![]({attach}Frank-Coppetiers-media/image4.jpeg){: .front .image-process-front}

**An Mertens** You were talking about going from one realm into the
other, which brings us to the other question. Can you describe that a
little bit more? How do you live it?

[38:42]

**Frank Coppieters** The great joy for a long time now is, when I pick
up the drum and I start chanting. It doesn’t take very long – especially
if I’m the only one who is doing it, because then I’m more in the role –
there is just like a different archetype maybe or a different center of
energy that takes over. My personality is still present, but truly, when
the sounds come, now I know they are part of what often comes through
me, it doesn’t sound like Frank, certainly not the personality of Frank.
I think that my training in theater - not so much as an actor, even if I
did a lot of improvisational exercises – has helped a lot to allow that.
To not censure anything, in the sense that I allow anything that is
coming out, just go with it and directing that energy. I’m not a very
visual person, but on the feeling level, I just know, here it is. It is
different from the more linear dimension, it has extra dimensions. And
sometimes, from within that, there is a strengthening that takes place,
or a filling up. Where I’m almost riding like on the back of a tiger,
you could say. I’m holding on. It is not that I’m ever not in control,
but there is no wanting to be in control. I’m still connected to outside
reality, very much actually. I’m aware of the time, for instance. I
could stop at any moment. There is a very intense listening going on. A
stream of impulses. It feels almost like the natural world, like you
watch a cat or a tree swaying in the wind. It is something like that.
Very organic. The technique is more on the level of breath control or
making sure my voice is open enough, so it is not being taxed. By
experience, it becomes also like a job, which I always wanted. That it
would be something normal, like a ‘schoenmaker’, a shoe maker who knows
how to make a shoe. Just doing it. It is not an ego thing at all. I
prepare. There is a preparation that I love to do, for bigger events
especially. The energy then comes beforehand, to strengthen and purify
the vehicle. Once it starts, there is no fear, which typically I would
have as a person. I don’t like to make mistakes, I don’t want to be late
for the train, whatever, all these things. I don’t seek it out, but on
occasion I have done events for 80 people. Most of them don’t know me
and I’m ok with it. Because I feel that whatever that connection is, it
doesn’t depend on the circumstances. It’s nice when it is in a place
like this, sure, but it can be anywhere. Except of course, I would never
ever offer it where it is not welcomed.

**An Mertens** How do you prepare?

**Frank Coppieters** Mostly through meditation. Exclusively through
meditation actually. Everybody is different. Jóska, strangely enough,
for his evening sessions he had in Brussels, I know he started in the
afternoon to chant; whereas I don’t want to tax my voice by the time
that I need it.


## On the materiality of spirits

<section class="audio out">
<audio controls>
  <source src="{attach}Frank-Coppetiers-media/interview_frank_180622b.m4a" type="audio/mp3">
</audio>
</section>

![]({attach}Frank-Coppetiers-media/image3.jpeg){.image-process-illustration--inline}

[0:12]

**Frank Coppieters** You ask how material spirits are, something like
that?

I think they are. Also thoughts have a material aspect to them, on a
certain level. For instance, I don’t know how much of Jung you know.
Jung has had experiences which he describes as poltergeists. When there
are certain tensions in a home, also emotional tensions, sometimes
something falls. There is a material aspect to that. I wouldn’t say it
is my specialty. In a way I’m happy about that. I would not necessarily
like to work with dubious spirits. I love to work with the energy of
saints. But on occasion it happens. For instance, there is this woman.
It is a whole story, I will try to keep it short. She has been sexually
abused by her mother, who is no longer alive. Of course, a long journey,
as these journeys are. She came to seek me out, because maybe six, seven
years, maybe more, after her mum had passed, her mum was back. At least
in her experience. I know she is a very sincere person. I’m still
working with her. That is a very typical thing to seek out a shaman for.
You cannot go to a doctor for this, even a psychiatrist is not
necessarily the right place. If you would know a shaman, you would go
for that option. I said, sure. I had immediately a rush of empathy. I
liked the person and had empathy, and also detachment. I’m not myself
possessed by spirits and don’t have a sexual abuse history. So, she lies
at some point on the table and I’m doing my shamanic work. All of a
sudden, it is heating up, heating up, uh, and I beg and order the spirit
of the mother to go. I feel it going to a specific corner of my room and
it goes out. That’s my experience.

Often I don’t really know what it going on. I’m just doing my work as
good as I can. But this one was very specific for me. So the client, in
her experience, she also felt something big leaving, in the shape of her
mother. She hadn’t been able to sleep well because of all that. I said,
well, it was a good session for me. Let’s see. That one session was
instrumental in her no longer being possessed by her mother. And of
course, there is no proof. Certainly not. But it is one of the few times
that I feel specifically a direction and ‘entity’ or a spirit that
leaves.

Even based on that one experience, I would say that spirits have a
material form. Of course, there are many books written about haunted
houses and all that stuff. It would be very strange if none of that were
based on any fact. It could happen.

But I also remember as a child going to Het Gravensteen in Ghent and
visiting the torture chambers and how horrible this was. Absolutely
horrible. I had some nightmares about that for a while. I went with
Kathy much later at some point. And now, there is a sound healer, I
think his name is Marcel Hebbelinck, who sits there with his harp. When
people have left the castle, he is playing music and I think he just
releases all the stuff around people. He’s aware of it. He has some
beautiful albums.

[6:10]

Jozef donated the big drum we have in the room. As a preparation for his
group before the initiations at the end of the second year, he had an
idea to go to some place, to Arduinna I believe. We ended up almost
accidentally, even though there are no such things, at the Maginot line
in France. We came at a place where there is a plaque hanging… A big
tragedy happened there for the French. A whole regiment of people from
North Africa, mostly Algerians, were gassed by German soldiers. There
was some kind of an explosion. It is a very dark spot. We were a bit
surprised that all of a sudden here we are, with our group of twelve. We
had our drums with us. So, we decided to do a ritual there. It was a
beautiful ritual. Very peaceful. With plenty of strong drumming. It took
about half an hour. All of us, we were absolutely elated. We were full
of joy. It was something that had happened, not just on the level of our
emotions. We couldn’t explain it. We felt stuff had left the Earth.
Sssssh.

That is something that shamans work with. Not just shamans, other
spiritual groups as well. Some groups go to Auschwitz to pray there.

So yes, I do think that spirits have a material presence that can be
felt, especially if you’re sensitive. Or if you’re in a bit of a trance
state.

## On local totems

[9:00]

**An Mertens** How important is a specific place for shamanism, related
to the local totems?

[9:28]

**Frank Coppieters** I think there are many circumstances where the
place can play a big role. I don’t think we would have been able to do
this work – and of course we never know for sure – I just go with my
heart. I feel that our little group did good work at the Maginot line,
but also that we were gifted with this opportunity. It was the perfect
way to prepare ourselves for initiation. It is a give and take and it is
amazing how that works in all realms. Within the shamanic work it is
very obvious. We needed to go there to do that work. I don’t think we
could do it from our room. Even though today I thought, yes, we can
connect with Gaza. We have a strong connection with Ton \[van der Kroon,
who was doing healing work there\]. There are these things that can
facilitate shamanic sessions, when there is some direct grounding in a
place where the work needs to be done, or when you are in a sacred
place. When I’m in my own office where I have been working for 25 years,
many presences are there. So many meditations have taken place there.
That is one of my power spots. I sit down, I use my special drum, that’s
perfect.

But also, when the intention is strong, it can take happen in any place
where the work needs to happen. At least in the way I work. Just once I
worked with shamans from Siberia who came to Oregon. There were three of
them. Two of them had never ever left their village. They had been
shamans their whole life. One was a younger person and she had traveled.
It was their first time in the US, their first time outside of their
village. When they were starting their work and calling in the spirits
they work with, they were so surprised because the spirits wouldn’t come
right away. I was thinking, how bizarre. Personally, I have never
experienced that. If I call upon Kwan Yin, it feels like Kwan Yin is
there. Their way of viewing that was, well, there are totems, there are
guides, they live in Siberia, in that village where we are. That’s where
they live. So they first have to get here. In a way that is the beauty.
Whatever you believe, however you’re trained, that also becomes a
certain reality. You cannot escape that. You can learn from that. For me
– I have been in airplanes so often – it becomes a natural thing.
Whatever the work is about, travels with me.

One of the first times I came to Belgium, I have a friend – she is a
homeopathic doctor and a clairvoyant – who said, Frank, there is a
native Indian with you. To tell you the truth, I had also felt that my
work here in Belgium was different from Portland. Here, especially,
people want this contact with the Native Americans. Whereas there, it is
more normal. They want more connections, let’s say, with Egypt. I also
have an Egyptian connection.

But when I’m here, it never fails. Lately it is more defined around
Sitting Bull, for instance. It’s not that I’m doing something, it just
installs itself in your field. Of course, if I can choose, which I often
can, I like to work in places that are protected, like here.

I also teach at the massage school \[Portland\] which is not an obvious
place, because there are other classes going on nearby and I have to
move rooms sometimes because there is too much noise and all that. But
the work that happens, is also on a very high level and perfect.
Certainly for the group of people that are there.

For instance, going to Bhutan… Our longest going group… Padmasambhava
announced himself in a morning meditation in the third year as the main
shamanic guide for that group. It took me by surprise. But it was clear
enough, strong enough, that I spoke what I heard, and then people were
curious. Who is Padmasambhava? I knew a little bit but not a whole lot.

**An Mertens** So he spoke through you?

**Frank Coppieters** Well, the guidance spoke through me. I’m not
really sure how that whole thing works. Is that my over-self? I don’t
really know. And I’m almost happy that I don’t know. So it doesn’t
become a new “mythology”.

Now when I was in Bhutan, I had no idea, Padmasambhava is in every
single place there. He is the shamanic guy from Bhutan. I had
transmission after transmission whenever I would visit a temple, almost
every day. He’s depicted with these big wide eyes and he has eight
different manifestations in the thangkas \[paintings\]. I could have
stayed for hours in every single temple, but we couldn’t. And that was
ok. I would just look at the thangkas and I would receive a
transmission. Shhhh. And that transmission I could not have received if
I had not come to Bhutan.

## On transmissions

![]({attach}Frank-Coppetiers-media/image7.jpeg){.image-process-illustration--inline}

[16:32]

**An Mertens** What do you call transmission?

[16:36]

**Frank Coppieters** It is almost as if – and I’ve had that in Egypt
also specifically, or in India for that matter – that the painting
\[thangka\] becomes alive. It really becomes alive. On the material
level there is like a shimmering. Shhhh. I love, let’s say, Van Gogh,
and many other paintings, but there is not that specific energetic
shamanic transmission. I would compare it more to the transmission of
Mother Meera or Amma, something that starts vibrating on a specific
frequency that is connected to consciousness. I feel that stays with me.
Later I can call it up if I want to. It becomes part of what is in my
‘medicine bag’.


[17:51]

And Jóska gave me once… When he was drumming for himself also, different
totem animals would come. They became associated with his drum. He
always used the same drum. Maybe he had two drums. Maybe one broke at
some point. When he would play his drum, after many years of doing that,
he had a bigger assortment of guides and totem animals. Some were, say,
‘de specht (woodpecker)’ or Krishna, or a bird called ‘de Jan van Gent’.
It was very specific. Or perfumes. He got very excited whenever there
was an addition to the spectrum. When he gave me his list, there were
already 47 different ones, and the list was from 1986. He passed away in
2008.

[18:57]

**An Mertens** It is more like a kind of energy that you then use
afterwards, than really concrete information? Or insight?

**Frank Coppieters** Yes. It is not information in the sense of
scientific information. Or even knowledge. But it is vibrational
information. That implants itself, I feel, into my energetic field.
Mostly in the heart, but also on the level of the bone structure. I
think it wants a little bit of density. It goes from the more immaterial
to the more material.

Shamanism is always between the dimensions, which is why it is fun to
initiate for instance at dawn or at dusk, at Spring equinox or Summer
equinox. Something is overlapping.

**An Mertens** On the intersections.

**Frank Coppieters** Yes. Where the linear kind of bends into what is
not so linear, where one dimension goes into another dimension. These
dimensions are just concepts. But still, science is also just concepts.
So it is one dimension that is nested into another dimension, into
another dimension, into another dimension….on and on. As far as we know,
there is no limit to that. It just goes on and on. When the mind becomes
more multidimensional, it is a very plastic mind. When somebody like
Einstein comes in, with a formidable capacity, that he can contain more
than anybody else on the planet, on that particular level, then a big
breakthrough is made. Sometimes it takes a hundred years for regular
science to catch up, to say, hey, he was right. And he’s just sitting
there, smoking his pipe or something. But he is holding it. He would
concentrate for hours and work it out.

For shamans it is something similar. It is a kind of thing that you
apply yourself, and of course you learn from the previous lineage of
shamans also. That is why this transmission often happens from person to
person.

**An Mertens** As bodily experiences.

**Frank Coppieters** Exactly. It is important not to deny the aspect of
the body. Sometimes in spirituality there is a split, let’s say, between
the body and what is not the body. In shamanism it is seen that the body
is relative, but that it is also the place to do the work. A shaman
typically is quite embodied. Prior to meeting Jóska I certainly had lost
big parts of me. Parts that were no longer being embodied. That was one
of his first lessons. He would say, if you want to go there (pointing to
the sky), go there (pointing down to the earth). It always took me a
while to figure out what he meant by that, which is often the way a
shaman works. You still have to do the work yourself.

<!-- ![]({attach}Frank-Coppetiers-media/image5.jpeg){.image-process-illustration--inline} -->

<!-- ![]({attach}Frank-Coppetiers-media/image6.jpeg){.image-process-illustration--inline} -->

<!-- ![]({attach}Frank-Coppetiers-media/image10.jpeg){.image-process-illustration--inline} -->

[^1]: Jóska Soós was a Hungarian shaman and painter. He migrated to
    Belgium during WWII and lived in Charleroi, Brussels and Antwerp:
    http://www.joskasoos.be
