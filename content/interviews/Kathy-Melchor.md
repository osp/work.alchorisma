title: Kathy Melcher
author: An Mertens
summary: This interview took place in June 2018 in preparation to the worksession in Z33 in Hasselt. Kathy Melcher and her husband Frank Coppieters offer trainings in universal shamanism and Reiki initiations. Kathy also works as a therapist from a Transpersonal perspective. The Transpersonal is a school of psychology that studies the self-transcendent or spiritual aspects of the human experience. Within that perspective she uses a number of tools from the disciplines of Interpersonal psychology, Jungian and Gestalt psychology, Cognitive Behavioral therapy, Dialectical Behavioral therapy, Universal Shamanism, Reiki and Meditation to help people gain insight into how to free themselves from the narrow confines of their symptoms and self definitions. <https://livinglightcenter.com>
license: fal

# Kathy Melcher

# Bois Le Comte, Villers-devant-Orval, June 2018

<section class="audio out">
<audio controls>
  <source src="{attach}Kathy-Melchor-media/interview_kathy_180621a.m4a" type="audio/mp3">
</audio>
</section>


<!-- ![]({attach}Kathy-Melchor-media/image2.png){.image-process-illustration--inline} -->

![]({attach}Kathy-Melchor-media/image3.jpeg){.image-process-illustration--inline}

<!-- ![]({attach}Kathy-Melchor-media/image4.jpeg){.image-process-illustration--inline} -->


[2:13]

**Kathy Melcher** Your questions make me think about this in a unified
way. Because normally the teaching from an experiential perspective is
as it comes. I find it interesting to try to think about it as a body of
work.

## Technology (before COVID-19)

[2:47]

One question you had, was, do you think technology plays a role? I've
been kind of struggling with that one. I'm not sure about how I would
answer that, except to say, for example, that technology makes things
available, like on Youtube. You can see everything, you can see every
shamanic piece of work and music that has ever been composed and
interviews. And all of that that we were never be able to see before.
Does it play a role? In a way it creates a very broad exposure of
something that used to be kept secret. It was only handed down from
generation to generation, from shaman to shaman trainee, as in Carlos
Castaneda. Maybe, if nothing else, it puts people in touch with what it
looks like. But it never probably gives what the internal experience is,
which I think matters more to people.


![]({attach}Kathy-Melchor-media/image5.jpeg){.image-process-illustration--inline}

## On the presence of spirits

[4:18]

On the presence of spirits.\
When I was a child I always saw things. I saw creatures and spirits and
all of that. I would say until I was about 11 years old. By then, I was
told that wasn't what you were supposed to look for. I spent a lot of
time as a child looking out the window and seeing things.\
I think that is why I love gardening so much, because you can be in
touch with those kinds of influences and not be thought crazy. You can
commune with the varying energies and feel good about it.

[5:20]

The most profound experience that I had, was when I stopped drinking
decades ago, I was very much aware of help from the other side. The way
that it came about, was really interesting. I decided to stop drinking
on the 17<sup>th</sup> December and Frank and I had never taken a honeymoon. We
decided during that time that we would go to the big island of Hawaii.
At the time, I had a friend who was training as a hypnotherapist. She
had come across in some of the groups that she had done, the work of a
Hawaiian healer, by the name of 'Papa Auwe. She said, if you're going to
Hawaii, you need to see 'Papa Auwe, he's really a healer. I thought,
great, ok. But Frank immediately picked up on it. He called 'Papa Auwe
and made an appointment. We drove all the way across the big island to
Hilo where 'Papa Auwe lived in a wooden house painted dark red. He was
about 85 years old. We came in the back door. He took us into a kind of
a sitting room. We were sitting on kitchen chairs. He started by asking
me about how long I had been drinking and what's going on now. I don't
remember everything he asked me nor do I remember everything that he
told me, but I remember his eyes and the power of his person.

Basically, his instruction to me was, you take these herbs and you put
them in tea, everyday at the same time and so on and so forth. He said:
it will not be easy. I said that I knew that. That was the end of that,
except to say that, that was the beginning of the support that I felt
for the change. I knew that it was going to happen and I knew that it
was going to last, even though I had my moments of craving. I knew that
this was once and for all.\
Years later, I bought a copy of Parabola - an esoteric publication in
US. This was some time after 'Papa Auwe died. There was an article on
'Papa Auwe and his life work. I discovered then that he was the great
Kahuna of all the Hawaiian islands. He was the great shaman, in charge
of the rituals at the City of Refuge. There I had first gone as part of
my pilgrimage on the island of Hawaii to be initiated in Reiki. I felt
that that was a huge influence in my life. Also the fact that three
years leading up to this point, I had been in the company of Native
Americans, because part of my work for this corporation, was to maintain
relationships with all the Native American tribes working with Native
American youth to help and encourage them in careers in technology. I
was meeting with elders, learning Native Americans' ways, hanging out
with all of the tribal stories, learning all of this. One day, I was
sitting at a lunch next to a Native American. He was a Cherokee. He
said, 'I never had anything in my life, until I quit drinking. Now I
have my dignity.'

That was like an arrow pierced my heart. Ah! That's what it is. And then
the story of 'Papa Auwe came.

[10:17]

**An Mertens** If you say that you felt the support, can you try to
visualise it?

**Kathy Melcher** No, it was a felt thing. I remember saying in
AA-meetings -- people often think you're crazy when you first sober up
-- that there is help from the other side.. 'I feel like there are
angels watching over us,' I said, 'that we're being taken care of if we
allow it.'

**An Mertens** Did you feel it physically?

**Kathy Melcher** Yes, I felt it as something around my head. I just
always felt a presence or presences. And I trusted that. Maybe I
graduated from when I was younger from seeing to feeling.

[11:26]

**An Mertens** When you were a kid, you could see them. Can you give an
example of what you saw?

**Kathy Melcher** No, because it is difficult to identify. I just
always felt at home. I didn't feel threatened. I didn't feel afraid. But
in the night, I'd tell my father that I saw something go down the hall,
he'd get up and go down the hall, he'd come back and say: no, nothing
there.

I could see entities, beings. Whether they were bodies, it was less
definable than that. Shapes, some colours. I'm not so sure that most
children don't see something like that. Or that they can see something
around people and read auras. I was able to do that at different times
in my life. I could see darkness in certain auras and light in others,
and wondering what that was. At the time I was not really understanding
that it was something. I had a vague sense that something was going on.\
It's not something that I rely on as a profession. I think all people
are capable of doing it and do read from one degree or another. I guess
it's just information.

I think the spirit world, the invisible world, is with us, and that the
veil between the visible and the invisible is very thin.

We think that death is a big door that is separating us, but I don't
think that is so. I think quantum physics is telling us, one minute
you're on one side and the next minute you're on the next, whoever you
are. Maybe.

[14:45]

**An Mertens** So, spirits are material?

**Kathy Melcher** Yeah, because I suppose that if you say the absolute
is the immaterial, then yes, I suppose there are gradations between what
is densely material and what is less densely material. Rocks and this
chair I'm sitting in, that's one kind of physical reality. And then,
what would you say to felt presences? They're not seen necessarily or
maybe they are seen but they're ephemeral. They're not solid.

**An Mertens** Like gas?

**Kathy Melcher** Yeah, or less than gas.

## On science, experience and synchronicity

<section class="audio out">
<audio controls class="out">
  <source src="{attach}Kathy-Melchor-media/interview_kathy_180621b.m4a" type="audio/mp3">
</audio>
</section>

[0:12]

**An Mertens** You're talking about quantum physics. How does that
relate, does it offer to you some kind of scientific explanation of
things that happen?

**Kathy Melcher** In the end, philosophy, science, it doesn't separate
science from experience. It is all one thing. The esoteric and the
mundane are the same. That's what I like, to see that holistic view of
what is. We've always known this, this story of the master appearing to
the student, like the story of Ramana Maharshi sitting at the foot of a
young seeker's bed for many years. Then the guy goes to India and there
the master is. So what is that about? All of those tales of the
miraculous. I think we make too much of separating science from
experience. Most people kind of know that. Otherwise it gets to be a
mental exercise, improving and disproving. It's not to say that there
shouldn't be some kind of higher reasoning involved in how we evaluate
our experience but I don't think it serves to separate anything from
anything. That's my take.

[2:30]

**Kathy Melcher** Ok, what you were saying about an enhanced awareness
of synchronicity, that also plays into that description of what I just
said of the intermingling, of how can this be that someone is in one
place and also in another place, or physically in one place and appears
in another place. I think that shamanism has always accepted everything.
It's an all inclusive practise. And there is nothing excluded, no
experience outside of shamanism that is not dealt with or included.

[3:27]

**An Mertens** Do you feel an enhanced awareness of synchronicity since
you started working as a shaman?

**Kathy Melcher** Yeah, that's more the natural than anything else. The
only reason that we're surprised is because we live in our heads all the
time. If you're living in experience where you're not overthinking it,
synchronicity is to everything, happening the way it should, unfolding
the way it should. It is not extraordinary. Because that is the way
reality is.

**An Mertens** In nature for example, the seasons and so on.

**Kathy Melcher** No, not like that. The fact that you think of
someone, and you go to the village to run an errand and then they're
there, even if they're from the other side of the world. I don't know if
you have had that experience. The first time I had it we went to Italy
and there we saw people we knew. My family were like, what are you doing
here. Oh, we just on the last moment decided to come and blablabla. Like
that.

I lived for four years without a telephone, without a television,
without anything. And, no kidding, everything that I needed, I got.
Everyone I needed to meet, I met.

[5:06]

**An Mertens** When you then meet these people in Italy, you feel the
synchronicity. Is it also then an invitation to work together or do
something together or exchange?

**Kathy Melcher** I think it's just pure play. As they say in Sanskrit:
'Lila'. It's the play of the universe. We come together, we recognize
each other, we enjoy, we share, and then we dance off somewhere else.
That's kind of how I see it.

And whether or not it means we're supposed to work together, that's kind
of heavy. \[Laughs\] I mean, if first something happens and we
participate in it, then I think it's pretty cool. \[Laughs more\]. When
you first hear of the new age thing, you know, when I was an early
sannyasin,, 'Ho, you're here too… oh wow…. \[laughs more\].'

[6:23]

**An Mertens** Or like the story Frank told us yesterday, about the
woman he gave a healing to and then you went to a concert and you came
across this woman? I ask the question, because for me often it is
tempting to put some reason or explanation to it. Or a meaning.

[6:51]

**Kathy Melcher** Well, it is its own meaning. You don't have to put
anything on it. It's just the joy of meeting that person again. And
although I did say to Frank, he said, that's the woman and I said,
'Yeah, he \[her husband\] doesn't look so good, he could use some help.'

[7:26]

**An Mertens** And it's enhanced since you started working as a shaman?

**Kathy Melcher** Maybe I ended up working as a shaman because those
were my predilections. That was my inclination, to experience things
that way. Because I didn't decide to be a shaman. That is absolutely not
the way it happened. I just happened to be married to one.

## On 'becoming' a shaman

![]({attach}Kathy-Melchor-media/image2.png){: style="float:right; max-width: 40%; margin: 0 calc(-1 * var(--pad)) var(--pad) var(--pad)" }

[8:04]

**Kathy Melcher** When we \[Frank & Kathy\] were first together, he
\[Frank\] said, I think I'm a shaman. I was like, hmmmm. I went to my
mother. I said, mum, he thinks he's a shaman. She said, oh yeah, I know
what that is. So my mother was happy, and I was just like, aaah. What an
idea.

**An Mertens** Did your mother explain it to you?

**Kathy Melcher** She didn't. Conversations with my mother were not
always complete. When she said she knew what it was, I wasn't afraid of
it. I was just kind of curious. And also somewhat burdened by it,
because I didn't know how to deal with it or incorporate it into my life
at that time. I mean, it was good that I had been to India and had all
these weird experiences and so forth. It enabled me to better meet that
challenge of having a house where all these things were happening. And
to this day when I meet my clients and Frank is doing his session, and
I'm doing just straight therapy with someone in my office, and the drums
start going, they feel it, they're having a session too.

I was initiated through marriage. And of course, I had encounters with
Jóska Soós[^1] and other Native American shamans and tons and tons of
sweat lodges. After a while, it becomes a lifestyle and you have friends
who are shamans.

[10:19]

**An Mertens** Do you believe everyone can be a shaman?

**Kathy Melcher** No, I think those who are called to be shamans, are
shamans. When they go through the initiatory process and they study with
someone, maybe there is the rare one who springs forth spontaneously, I
think that's a possibility too. But no, I don't think everyone wants to
be a shaman, because it can be a heavy duty. Not to say that it's not
fun, but you have people knocking on your door, day and night. Even in
the neighbourhood. Oh, there is a shaman down the street! Ding dong!
\[laughs\].

[12:51]

**An Mertens** You're collaborating. You're not serving him \[Frank\].

**Kathy Melcher** No.

**An Mertens** Which is very beautiful.

**Kathy Melcher** Yeah.

**An Mertens** And quite rare also, I think.

[13:03]

**Kathy Melcher** I think it is rare for people to be able to work
together in this fashion. There has to be an agreement that whatever
comes up, comes up. Whatever gets dealt with, gets dealt with. And that
the decisions, if any, are made in the moment and not beforehand. That's
difficult to live with sometimes, you know, because actually I'm a very
-- as I've gotten older -- I used to be very broad brush, let's have a
big perspective on things, but also, as I'm older, I require more
specificity. What do you mean when you say 'x', what are you trying to
communicate to me? In conversations I really want the full thing. After
all this time I finally learned that there are so many layers to all
this, that we don't talk about and that we take for granted. And
sometimes the work is not complete until it's fully understood. That's
my thing.

[14:25]

**An Mertens** When you say it is difficult to live, you also mean, on
a daily basis?

**Kathy Melcher** Yeah, also on a daily basis there has to be the
willingness to allow things to be as they are. Without interfering. It
is an impersonal view of one's personal experiences, or an impersonal
framework for ones personal experiences. And as long as one accepts the
way things are, unless there is some violation or there is something
that needs to be addressed, the way things are, flows very well and it's
good. The only time one gets distracted, is when there is a knot, a
personal knot. I find that very interesting. I came across some material
that I had forgotten, Ken Wilber talking about the chakras. He said,
yeah, chakras are real. What they are, are the knots that need to be
untied before people can have an experience of being one with the all.
And there are very specific places where people get stuck. Safety and
security. Lost in sex. Wherever the preoccupations are, the emotional,
bliss, self expression, vision, we can get stuck in these places. You
know it from rebirthing, for example, remember that? Breathwork --- the
whole rebirthing process, when people are breathing their way through
their various experiential layers. It's kind of neo-Reiki, based on
pranayama.. People come to plateaus and they want to get out there. They
want to hang out on the bliss plateau, on the heart plateau. But that's
not necessarily the end of it. We tend to get stuck. We get addicted to
certain places because they feel good. It may not be in our best
interest to remain there. We do that nevertheless.

## Multidimensionality


![]({attach}Kathy-Melchor-media/image1.jpeg){: style="float:right; max-width: 40%; margin: 0 calc(-1 * var(--pad)) var(--pad) var(--pad)" }

[17:46]

**An Mertens** Maybe we can address the question on the
multidimensionality?

[17:55]

**Kathy Melcher** If you're talking about existence, the typical
shamanic definition of the work, is that you have the lower world or the
unconscious, the middle world which is daily life, and the upper world
which is the lighter spirit world or superconscious. But I think that
there probably are many dimensions to reality. It's all reality. And I
think that we have trouble accepting the totality of reality. We try to
put it in categories and call levels. This is the amphibian level, Jóska
Soós used to say. He had a whole list. Mammalian level. Rocks and
crystals and so on. I'm fine if somebody wants to call those different
layers what they are. If people want to give those definitions of it,
fine, but it's all reality. It's all inclusive. I think that is what
universal shamanism tries to address. In it you say, I am that, instead
of, I'm not that. The Jungian archetype of the egg is the beginning, a
way to look at layers to the psychological, but he said that it tied in
to the ultimate reality. And that's the way to start making the enquiry.
What's happening on this level? And on this level? How do these meet?
What is the unifying force underlying all experience? What's hearing
these words? What's seeing this? What's feeling this? At that point,
people drop into peace.

The only reason people aren't happy, is because of the knots. As if
there is no birth, no death. We don't have to categorize experiences.

[21:23]

**An Mertens** Does it happen to you that you visualise these layers
and dimensions?

[21:35]

**Kathy Melcher** No. It's more of a felt… I don't have to visualise.
Whatever appears in the space, appears in the space, as an object. I'm
not concerned about the level per se. I kind of instinctively know, I
think many people do this, they vibrate at the level of the thing that
they're working with. If they want to consciously bring in a higher
level of vibration, then they do that. There is some skill to that. I
suppose that is what a shaman does. They raise the vibratory level of
the experience that is happening, so that there can be insight and
freedom. But I don't necessarily visualise.

I remember reading in the Book of Judas where is there is that gnostic
idea of the levels of heaven. The seventh heaven, you know that whole
idea. I think that's an ancient concept. It's a conceptual thing.

**An Mertens** It is the same as categorisation? A way to try to…

**Kathy Melcher** To explain. It's descriptive, a way to try to
describe various levels of experience.


## On tools and totems

![]({attach}Kathy-Melchor-media/image8.jpeg){: .front .image-process-front}

[24:03]

**An Mertens** If a person comes with a problem, that you so nicely
call an object, the tools of the shaman like drumming or…

**Kathy Melcher** Intention. I think the intention is maybe the
strongest tool. Everything else falls out from that. There are the
ritual objects. That was one thing that you didn't mention that I wrote
down. Ritual objects, those would be drum, a feather, sticks, flute,\…
Those are objects used in rituals. Crystals, stones, scents, touch, all
of that.

[25:08]

**An Mertens** The voice.

[25:18]

**Kathy Melcher** The voice, especially that. It's just a
consideration. You asked about local totems. I sat with Frank for a
while and I was like, what is that? He gave some kind of explanation and
finally I thought, local saints and power places.

One day Paul, a student that we have from many years ago, he always
comes back now and then, he bicycles all over the area. He said, yes,
there is a place we have to go and visit today. It's called the green
cathedral. So it's at Villécloye. We drive to Villécloye. And yes, there
is a cathedral of green trees and all these stations of the cross. Half
of them are devoted to Jesus and the other half are devoted to Mary. Or
the Mary's or the Mary energy. At the head, you have the Lady of
Lourdes. Behind that you have some Celtic symbol. At the front of this
whole entrance to the green cathedral is a source of water, of the
sweetest, purest water I have ever tasted. This water is supposed to
cure blindness. It's dedicated to St Ernelle. It's sitting in the middle
of nowhere. It was created as a place in the 1700s but it was reinforced
at WWI as a healing place.

[27:28]

**An Mertens** And the Celtic symbol.

**Kathy Melcher** Yeah, that's from way before. Most of these sites are
like that, like in the cathedral of Antwerp. There you have the weeping
Mary. Before that, there was a Celtic goddess there that wept. It got
co-opted with Christianity. That's often the case. There are secret
sites that speak to a certain resonance in us. That was a magic journey.
And then we came back to Avioth. It was a different experience then.

[28:23]

**An Mertens** Do you sometimes specifically work with the energy of
such a place?

**Kathy Melcher** Sometimes. We did work with the energy from WWI. The
Maginot Line and the bunkers where the 200 service men perished through
gassing. We went up to the site not knowing how we would do this. We
started with singing and ritual. We could feel it humming along. I think
this is an area that is right for a lot of work because of the buried
loss.

[29:14]

**An Mertens** That's more like a place to heal.

**Kathy Melcher** Yes, but it also offers healing. There is
reciprocity. Does the Earth need healing, really? Really? The Earth will
heal itself. But we offer it. It's a way of making harmony, a way of
making peace.

[19:42]

**An Mertens** So, for you the places where these symbols of what you
call religion…

**Kathy Melcher** Or power places, or local saints.

**An Mertens** The place you describe from WWI, do you also think it is
a sacred place?

**Kathy Melcher** Sure! Yes. Any place where people transition, can be
thought of as a sacred place.

**An Mertens** So, war zones are also sacred places.

**Kathy Melcher** Yeah.

**An Mertens** I never thought of it like that.

[30:37]

**Kathy Melcher** I mean crypts, chapels, goddess spots, rocks, moons,
carvings, oak trees, healing waters, sacred earth, Arduinna, St.
Ernelle,… stuff like that around here. But you know, in India, there
is always a sacred place. Every five minutes there is a sacred place. Or
in the Southwest of the US, there are tons and tons of that. I have a
friend, Joe, an old Druid, he calls them vortices.

[31:31]

**An Mertens** Can a place become a sacred place? Because if you talk
of vortices, there is some kind of predefinition in there?

[31:43]

**Kathy Melcher** Yes, I imagine that there are places that collect
energy. Why, I don't know, but people sense that. They'll sit in the
places where the energy is the best. Animals do.

[32:11]

**An Mertens** It helped me when someone told me that people go on
beach holidays and resorts, because beaches are sacred spaces.

[32:23]

**Kathy Melcher** Yeah, and that's where we encounter the whales, who
are the protectors of the planet. That's what all the aboriginal
cultures say. And the dolphins, playful and loving, unless you're a
shark and you attack them and they go 'whap' with their tale or with
their snout. That's the place of encounter with the other sacred
creatures. I think there is a reverence for that, for sure, and our
relationship to all of the creatures, while maintaining respectful
distance.

[33:11]

**Kathy Melcher** You know that Maori story of the whale rider. Did you
see that movie \[Whale Rider, 2002\]? It is a true story about how
typically the tribe would single out one who potentially could be chief
by their capacity to ride the whale. So they would befriend the whale
and the whale would agree to take them on their back and they would ride
the whale. Well, a young woman comes into the Maori tribe and she says
'I feel called to ride the whale' and they're like, no no no, not a
woman, no. She insists and goes out on her own and befriends a whale and
rides it. And then it is a known thing. And then she's chief. And she's
the daughter of a chief, but it's the fact that she's a woman.

## Trance

![]({attach}Kathy-Melchor-media/image6.jpeg){: .out .image-process-out}

[34:28]

**An Mertens** Maybe something about the trance?

[34:38]

**Kathy Melcher** It's a hypnotic state induced to promote vision,
insight and healing. In many ways that can happen. It can happen through
repetitive sound or repetitive vibration, it can happen through plant
induced hypnotic states. But we are generally speaking easily
hypnotized. Trance is probably a second nature to us, so we don't even
realize it. When we watch movies or anything that we do in a repetitive
ritualistic way induces trance. Driving on a highway! Listening to music
or background noise anywhere.

[35:44]

**An Mertens** It doesn't always include healing, does it, or vision?

[35:47]

**Kathy Melcher** No, it doesn't have to, but with trance generally it
is implied that there is a healing experience contained in it. That is
the precursor to a healing experience. Frank tells this story about
being invited to a village ritual in Bali, where the whole village
instantly goes into trance. They're so used to doing that. And then they
do this exercise with the knifes, where they put the knifes in and the
skin doesn't break. And then they pull it out. That in a way is for them
to have the experience that they are masters of their own experience.
Because they join with the all and they're protected somehow in this
exploration. And people in Bali are just healed by touch and presence.
And through art, also. It is an ecstatic experience and I like the
Mircea Eliade idea, of shamanism being an ecstatic experience. Coming
into oneness is an ecstatic experience.

[37:29]

**An Mertens** Is not everyone looking for that?

[37:32]

**Kathy Melcher** They are and don't realize it. It's like everyone
being in meditation all the time, but they don't realize it. We're all
perfect as we are but we don't realize it. It's about getting conscious,
I guess. Or allowing.

[38:13]

**An Mertens** For many people the ecstatic experience is found in
drugs, alcohol…

**Kathy Melcher** Yes, but that's one that doesn't last, right? And
perhaps ecstatic isn't the right word? Let's say an attunement, an
experience where you feel at one? So, I do my journeys without any
enhancement of ayahuasca or anything like that, partly because of my
agreement with myself not to use altering substances, altered state drug
induced. I go through these journeys without and that is also entirely
possible. I think what you talk about is the dopamine kind of thing. I
know ayahuasca is really popular right now and there is all this talk
about ketamine's use against depression and stuff like that. It sort of
reminds me of a paper I read when I was in graduate school studying to
be a therapist. LSD as a cure for alcoholism ---, this was the
experiment. I remember seeing this text just laughing. But I see now my
clients talking about microdosing of mushrooms, that it might be
helpful. For people who have been stuck for so many years on one of the
levels, for them, they just want to have a glimpse of what the potential
is. And I do think there are biochemical shortages in this life through
disrupting our nutrition processes or trauma. You can alter DNA in such
tiny ways. Like the twins, one goes up into outer space and comes back
and his DNA is changed from his brother's. It is so subtle. The theory
about schizophrenia is that it is like a summer virus or something that
alters the chemistry in pregnant women. It alters the DNA and then it is
passed on from generation to generation.

We are so sensitive. What are the restorative processes for that? I
think in Western medicine we're still looking for the relationship
between man and plant, man and mineral. It's a search.

[42:00]

**An Mertens** And other medicines?

**Kathy Melcher** It's the same… Ayurveda, Chinese medicine. It's the
search to re-establish somehow the whole connection between animal,
mineral, vegetable. The thing is it is all intelligent, it is all
cooperative.

[42:36]

I remember having a tooth ache. It was here. I thought, oh god, what am
I going to do, I'm in the middle of nowhere and I'm having a tooth ache.
I stepped outside of the door and I had no idea. Right beside the door
was 'kruidnagel' (clove). A remedy.

## Relationship to trees

[43:11]

**An Mertens** This brings us maybe to this question about the special
connection to trees, stones…

[43:21]

**Kathy Melcher** What to say? One of the most miraculous things I ever
saw, was when Frank and I were in Breitenbush Hot Springs, which is a
place where we teach and where Frank teaches. We were at a Reiki
conference and a Japanese researcher was recording the song of trees.
What she did is, there was a lot of construction going on at
Breitenbush, they moved the water as it were, they changed the course of
the river. It was shaking up the whole environment. So she recorded the
tree song while the earth moving equipment was going on, and then she
recorded the tree song after we were doing Reiki on the trees. It was
beautiful. She put the vibration, the readings, to musical notes.
Beautiful.

[44:26]

**An Mertens** What was the outcome?

[44:29]

**Kathy Melcher** With the Reiki of course, the song was more beautiful
and with the earth moving equipment it was chaotic. That's something
that you get when you put your hands on trees. Like my friend Joe, the
Druid, he can feel the sap going up and down in the tree. That's not
something I can do. But I can feel light in the tree, and I know, I have
trees on our property at home and sometimes I can sit, meditating, and I
can feel the wind moving the trees, coming up through the floor. There
is this whole network underground how trees communicate with each other.
I used to break apart the mushrooms that would come up, now I leave them
alone, because I know what they're doing. But it took me a while to
learn all this, to respect it.

We have a tree that was damaged when we enlarged our house to create
office space for Frank. The two people who were responsible for the
construction gave that tree Reiki on a critical moment, so that it
lived. It is an 80 year old maple tree. It has lived in what used to be
the woods.

[46:13]

We have 22 trees on the property, some large, some small, some
ornamental, some big pines. I don't do anything to disturb them anymore,
because I realize that they need to be there.

[46:42]

**An Mertens** How did she measure the vibrations?

**Kathy Melcher** She had these little clips and a box recording the
vibration. And there was a software program, I have no idea how it
worked. And then there was something else to translate that into Apple
music.

## Relationship to stones

[47:14]

**An Mertens** And the stones?

**Kathy Melcher** If you go to the Southwest of the US, you will see
what stones are really about. There are so many incredible, unbelievable
rock formations that give off energy. They tell a very deep and long
story about life on and underneath the surface of this place.

Where we live there is a lot of volcanic rock. It is not the same as
sedimentary rock that you might find in the Midwest or something. It is
relatively recent lava from 6000 years ago. So, we live in volcanic
country where any minute there could be something else, something
different.

[48:28]

I'm attracted to crystals. I have many crystals in my office, in our
home. People make fun of it sometimes.

[48:43]

**An Mertens** What is the attraction?

**Kathy Melcher** I started out with bear fetishes. Bear is my totem
animal. The first thing I ever bought was a very large bear fetish
carved by the New Mexico Zuni's. That was the first fetish I ever
bought, in rock, in green and blue turquoise, carnelian…

**An Mertens** Do you work with them?

**Kathy Melcher** Yes, I do, especially with the white North-West
healing spirit bear. And they're just all there on my wall and on my
desk in my office. They're just hanging out there. It's a bear cave
(laughs). It stands for strength and serenity.

I had my friend Lionel Little Eagle come long long ago when I first
built the office. He came and did a bear awakening ceremony in the
place. It was great. He smudged, it was thick. There was a thick smoke.

**An Mertens** You felt it afterwards?

**Kathy Melcher** Sure, it is still there. Anyone can feel it.

<!-- ![]({attach}Kathy-Melchor-media/image7.jpeg){.image-process-illustration--inline} -->

[^1]: Jóska Soós was a Hungarian shaman and painter. He migrated to
    Belgium during WWII and lived in Charleroi, Brussels and Antwerp:
    http://www.joskasoos.be
