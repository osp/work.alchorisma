Title: On Technoshamanism and Tecnoxamanismo
Author: Isabella Aurora
Summary: Fabiane M. Borges is interviewed as the articulator of the tecnoxamanismo and intergalactic commune networks. A psychologist and curator researcher (art/science), she holds a PhD in Clinical Psychology. The Tecnoxamanismo network has been articulating in Brazil since the early 2000’s and it has taken form as a festival in the years of 2014 and 2016. A Tecnoxamanism festival is a self-organized and self-funded cultural exchange of techniques ranging from applied programming and database creation to medicinal plants and healing methods, gathering a wide variety of participants such as artists, shamans, health practitioners, rural workers, software engineers and more. The festivals have been hosted in the south of Bahia in a cultural hotspot in Porto Seguro and in the Village Pará of the Pataxó indigenous ethnicity. <http://tecnoxamanismo.wordpress.com>, <http://sacieartscience.wordpress.com>
license: fal, by Rafael Frazão in 2016.

# On Technoshamanism and Tecnoxamanismo

# Decadence as language, anti-xawara and indigenous health

<section class="audio out">
<audio controls>
  <source src="{attach}fabiane-borges-media/audio_interview_FabianneBorges.m4a" type="audio/mp3">
</audio>
</section>

**Isabella Aurora** Good night Fabi, good day in Brazil. Thank you for
coming. I want to ask you some questions about the Technoshamanism
network to understand the current state of the movement a little better,
to get a better idea of what is happening in Brazil now, what this
junction of technology and shamanism is.

Technoshamanism is a broad term, which, if we look at it from a
conceptual perspective, can be seen as a school of thought. Having been
one of the first to use this term in Brazil and one of the researchers
of this concept, I would like to know what, for you, defines the basis
of a techno-shamanistic philosophy.

## I will start backwards, as I remember

![]({attach}fabiane-borges-media/image4.jpeg){.image-process-illustration--inline}

[1:37]

**Fabiane Borges** Hi Isabella! ‘Isabella Aurora’, that's great. I want
to thank Constant. I'll start backwards, as I remember.

(…) We tried to set up an NGO in Denmark. We tried to set it up in an
attempt to find a way to earn money to make the Pataxó health post, that
was the idea. We were trying to help the Pataxó[^1] people where we held
the second technoshaman festival to create a health post. They asked us
to do it and we agreed.

I wanted to create a group to help me with Frazão, you, Raisa, Marina
Morais and Carsten. We tried to set up this NGO, but it didn't work out
because we couldn't open a bank account and there was no NGO to receive
the money we were going to get in France (…) Anyway, the field got
confused, the project was scattered, we had a dispute. And this group
fell apart. The NGO in Denmark also disbanded.

(…) Then we managed to raise 30,000 reais through an anonymous donor
and we were very happy. We were going to have the third international
festival, which would have a different name because it was going to be
another type of meeting called ‘Mutirão[^2] of Indigenous Pataxó
Health’. In it we were going to have a kind of festival, where everyone
from the network would come and help the Pataxó to set up, in the form
of a mutirão, the health post, spending seven days with them.

## Then came the pandemic

[6:35]

… What happened was that the Pataxó had to do it alone because the
pandemic came, the village closed and they themselves were ordered not
to gather. So they could not even make the mutirão. So this money of
30,000 reais was also used to pay a group of professional masons to
build the house designed by Ariane Stolfi.

That money was diluted (because) we hadn’t thought of hiring these
masons, which makes it more expensive, and there was a pandemic and
everybody was in need of money, so not paying the group of masons would
have been unfeasible. Anyway, what we are doing now is negotiating with
this French NGO to see if they can finish the job.

[8:51]

It's a done deed

**Isabella Aurora** Is it working?

**Fabiane Borges** It's not working yet because there are still some
bureaucratic things to do, but the post is settled, it has a roof, it's
built, right? It’s a done deed. So it’s an achievement. (…)

What a pity it was not built before the pandemic because it would
certainly have been a place to help these (indigenous) families that
caught the coronavirus. They don't have (access to health), it's an
Indian village. They don't have an emergency room. They have to go
either to other villages or to Porto Seguro, which is about 70
kilometres away, on a dirt road.

I didn't answer anything you asked, right?

**Isabella Aurora** No. In fact, you gave me an overview of the latest
developments in the network, the last major action that took place last
year, the mutirão with the Pataxó people. To explain a little bit, the
Pataxó (…) today are very close to their mother village, which is
Barra Velha, and specifically the subgroup we are working with is the
Pataxó Pará Village.

But my question goes back more to the origins, to give us a background
to understand. What school of thought is this? Why is it called
techno-shamanism and what would be the basis of a techno-shamanic
philosophy?

## The beat was like a heartbeat

![]({attach}fabiane-borges-media/image2.jpeg){.image-process-illustration--inline}

[12:39]

**Fabiane Borges** … About TX, I think it went like this: the first time
the name came up was in England in the 1990s in rave culture. Then it
spread as a kind of counterculture, with shamanism and electronic
parties, an idea that the beat of electronic music was like the
heartbeat (…) That covers the idea of shamanism as an ancient religion
before all the damage done by monotheism and puts tech up front to show
that it is ancestral.

Here in Brazil we have a slightly different story, but it only happened
in the 2000s, which is the recovery of the word tecnoxamanismo because
of the culture hotspots, a project of Gilberto Gil in the Lula
government. The idea was to put free software to be applied in the
furthest corners of the country (…) within indigenous, quilombola and
riverain communities.

So this word technoshamanism started to emerge. It is the application of
a digital technology within a traditional or ancestral system of
existence. Then technoshamanism started to make a lot of sense and this
had international repercussions that updated the definitions of
technoshamanism that were being used in the past. But our footprint
wasn't music, it was technique, it was technology.

## Aesthetic decadence is a language

[22:00]

**Isabella Aurora** (…) from this constellation of experiences, what
practices or proposals have you seen carried out that you consider
techno-shamanic?

**Fabiane Borges** Techno-shamanism is a concept, as you said. It is a
conceptual world that works with the idea of shamanism, of technique and
practices seen as technology. So there are some indigenous people who I
consider to be techno-shamanists, like Anapuaka here in Brazil.

Anapuaka Tupinambá has developed a virtual reality project with a video
of pajés[^3] doing pajelança.[^4] You put these glasses on and it is as
if you are in the village. By making the indigenous elders part of it,
the legacy is left in VR terms. (…)

Of course, in terms of Brazil, it's always difficult to get funding for
these things. I personally have a taste for decadence, I like precarious
works because I know how difficult it is to do them. So it would be good
if more was invested in these works, like those of Anapuaka.

**Isabella Aurora** I find this question of aesthetic precariousness in
relation to the movement interesting, because precariousness is
something that scares people and causes rejection. We are used to a
finished visual and artistic language, completely exogenous, which I
feel comes a lot from places that have (…) a ‘well-defined’ and
sellable aesthetic experience. So I think the precariousness refers to
the DIY tools within technoshamanism, and these often include knowledge
and paths that are Other.

**Fabiane Borges** Yeah, I agree with you. There is this sublime thing,
super well finished, with a perfect varnish and everything fitting
together, but which is an oppressive aesthetic for it has no diversity.
I think it can be an aesthetic but it shouldn't oppress other
aesthetics.

If I were to call someone a techno-shaman, I would mention Germano Bruno
Afonso. (…)

He is an indigenous man from Mato Grosso do Sul. He is of Guarani origin
and he graduated in astronomy, took a master's degree and a doctorate in
France and is a professor of astronomy. He began to dedicate much of his
research to the skies of ancestral peoples, mainly from Brazil. He built
a mobile planetarium where you discover indigenous astronomy through
storytelling (archaeo-astronomy).

Then there are other international artists like Martin House and
Jonathan Kemp (…) They got together to give a workshop called
‘Decrystallization’ in London. They decrystallize a lot of computers and
at the end the gold components of the hardware are cleaned and
sanitized, and everyone drinks a sip of the glass with mined gold. On
top of that it makes a symbolic return of the rest of the gold to the
Earth. So it's the anti-xawara.

## (Dogs barking) the anti-xawara

![]({attach}fabiane-borges-media/image1.jpeg){.image-process-illustration--inline}

[33:32]

**Isabella Aurora** Why an anti-xawara?

**Fabiane Borges** Xawara is when the disease, the pandemic invades the
world. It comes out from inside the Earth. The dogs are barking, can you
see them?

**Isabella Aurora** Yes. But it's OK.

**Fabiane Borges** Extracting gold and precious metals from the Earth is
like taking the skeleton out of the Earth, like loosening the Earth.
From within these holes comes this spirit of the xawara, which is a
demolishing spirit that attacks the shamans' chests. When the shamans
stop singing, because they begin to die, the xapiris[^5] stop sustaining
the sky because they feed off the shamans' singing. And when they stop
singing, the sky collapses on the Earth. As the Earth has no structure,
it suffocates. It's a catastrophe. It destroys everything that is alive
in it. This is the legend of the xawara Yanomami, which is in the book
‘Falling Sky’ by Davi Kopenawa organized by Bruce Albert.[^6]

(…) And probably ‘Decrystallization’ is the reverse work. They talk
about putting entropy into the system, that we should go through a
process of high appropriation of technological knowledge but by
disinvesting in the ‘evolutionary’ construction and do that as a
deconstruction to go to a point, another place where other possibilities
would open up.

**Isabella Aurora** (…) Spinning in a spiral to try to connect us with
people back there makes me think of the concept of spiral time.

**Fabiane Borges** I had some ideas about technoshamanism also being an
experience of time as it is a conceptual world and it is always in
process. So people sometimes want to destroy the concept, and the
concept has to be elaborated, not destroyed. I don't really know, I
remember the idea of the spiral as an alternative to the idea of
circularity.

There are a lot of people who think that time is a circle that closes in
on itself. The idea with the spiral is one of constant movement and
process, like orbits. You don't arrive in the same place, you are always
going around in a certain temporal circularity but it is always a
movement. It has circularity but it takes you to other places – above,
below, further away …

## This hole we fell into

[42:23]

**Isabella Aurora** To think spatially this concept of time in relation
to technoshamanism (…)

**Fabiane Borges** We are working with the concept of ancestorfuturism.
We are aiming at the production of worlds and the production of
imaginaries exactly to sustain this hole we fell into – be it the
pandemic, be it Bolsonaro. And there is the rest (…)

Regarding the last technoshaman festival, we wanted to pass the ball. We
tried to do it in Mexico, we tried to do it in Ecuador. The people there
didn't take it because they tried to get the money, but they couldn't
and gave it back to us, and the ball is with us again. We will do the
festival without money – but not during the pandemic. During the
pandemic it had to stop.

**Isabella Aurora** It brought me back to this wish to think of
technoshamanism as a perception of the importance of world creation.
(…) Of how the indigenous cosmogonies created our world and created
their own worlds through many technologies, and to take this technique
as a rescue and a methodology to deal with catastrophe.(…)

**Fabiane Borges** I think in particular that technoshamanism has always
stood a bit on that edge. It is far more borderline than realist. All
the experiences that we had with technoshamanism took on a temporality
of Tupã, ayahuasca, dreams, fiction, imagination, at least in what we
did. (…) It's an internet network, really. That's what TX is.

(…) Techno-shamanism is already born within the context of madness,
because it is born out of ecstasy/ecstasy. With the psychedelics, with
the crazy DJs, it is already born from the context of contemporary
rituals. Updating the ancestral and the futuristic.

**Isabella Aurora** I think that the Brazilian view of techno-shamanism
comes up against the stifling that we experience as artists, as
Brazilians and cultural potentialists, which is an invisibility, a lack
of access to investment, of having to work on various other things to
put bread on the table every day. I think that the movement and what we
can make of it also has these loopings and parts of it are
disjointed.(…)

## The health of the peoples

[53:43]

**Fabiane Borges** In this context, now, what would be brilliant would
be for us to rescue videos and audios and make them available in a place
where people could access them.(…) We could be in a discussion on
health, perhaps at the cutting edge, together with the indigenous
people. Maybe our question would be what medicines are being proposed
now for the indigenous villages.

**Isabella Aurora** What are the future steps to take with this network,
with this concept, this movement? What about the relation of
technoshamanism to health?

**Fabiane Borges** There has been a migration. At the beginning,
technoshamanism was very much based on the free software culture and
when we started talking about it here in Brazil, around 2005, it came
within that context. Then, when things started to degrade and everything
started to convert to neoliberalism, the programmes were sold or
appropriated by the computer industry and the movement lost the
political force it had in the 1990s. All this made TX, when it started
to develop in our hands in 2010, change its profile (…) It started
migrating to a younger and more artistic crowd. Programmers,
technologists, computer scientists, computer makers sensitive to the
issue of aesthetics came along, but also performers, documentarists,
ecologists, agroforestry producers, and people involved in happenings,
family agriculture.

(…) Perhaps my real participation within all of this – not only as an
organizer, but as a creator as well – was to introduce the influence of
the clinic, of group psychology, of subjectivity, with the influence
also of schizoanalysis, of the experience with other cultures, of the
creation of new group dynamics.

(…) The health in which we would be entering now would be a broader
field. Thinking of indigenous health, the health of peoples, traditional
healing practices, medicine. The pandemic disrupted this process a lot,
but we are constantly with the village virtually.

**Isabella Aurora** But it brought urgency, you know.

**Fabiane Borges** After this business of the Pataxó health post, what I
want is to see it ready. The vision of the future, I don't know. Not to
give up before the health centre is ready. Everything is slow, far away,
with little access, but the Pataxó are organized and making this happen.

There are other people, artists, who have practices. You know some
people, don't you? Some artists?

## Immigrant spirits

[1:02:08]

**Isabella Aurora** I know some who have techno-magical practices. There
is a movement here in Europe, since I think the 2000s, a combination of
technology and traditional/archaic knowledge critically reimagined as
tools. (…) But there are many manifestations between art, magic,
witchcraft and technology.

And I also see that everyone who is a migrant here, well, we bring our
ghosts. We bring our will and our presence, the presence of our spirits.
So it is very funny if we think about the path of the spirits. First
there was a migration, a colonization, and a thousand syncretisms were
created of the gods that were born and carried in the boats and reborn
there (in the colonized countries). Now migration returns these gods but
re-imagined, and it makes them come back here where they find a
devastated land. So I feel that there is a spectral force here that is
very big and terrifying, but very veiled, silenced. That is why I think
that this migration and contact between the migrant and the place bring
a little bang, a little big bang that is also a feedback loop. (…) I
think that other iterations are going to take place with these gods that
are coming back.

**Fabiane Borges** The immigration of the spectres, right?

## It’s alive

![]({attach}fabiane-borges-media/image3.jpeg){.image-process-illustration--inline}

[1:06:05]

**Fabiane Borges** I want to thank you. I thought it was good for us to
have this conversation. (…) Of course, one hour of conversation is not
enough to know exactly what it's about, but I think we have already been
able to give some brushstrokes.

**Isabella Aurora** But it is a concept so alive and so organic that it
can only be touched as we speak. You can't make an A+B definition.

**Fabiane Borges** (…) People ask for something concrete, but like you
said, it is alive and organic, we touch it and it responds.

**Isabella Aurora** Thinking about the history of the movement, it also
makes no sense to talk about a cold concept without talking about people
and encounters. Because the encounter is already the movement.
Approximations, ‘disapproximations’, then it becomes clearer.

**Fabiane Borges** I usually call it a network, an internet network,
which is updated in real life whenever it happens. You never know who
you are reaching. But the pandemic has shown that the virtual world is
perhaps our greatest experience of the present for the upcoming years.

[^1]: The Pataxó are an indigenous people in Bahia, Brazil with a
    population of about 11,800.

[^2]: Mutirão: collective and free mobilization of individuals to
    perform a service that benefits a community.

[^3]: Pajé: Brazilian shaman.

[^4]: Pajelança: a generic term applied to the various manifestations of
    the shamanism of the indigenous peoples of Brazil.

[^5]: Xapiri is the sacred word the Yanomami people of Brazil &
    Venezuela use for 'spirit', the Yanomami shamans contact the Xapiri
    for guidance and to listen to the ancient wisdom of their ancestors.

[^6]: Davi <span style="font-variant:small-caps;">Kopenawa</span> and
    Bruce <span style="font-variant:small-caps;">Albert</span>, *La
    chute du ciel. Paroles d’un chaman yanomami*, preface by Jean
    Malaurie, Plon, ser. ‘Terre Humaine’, Paris, 2010
