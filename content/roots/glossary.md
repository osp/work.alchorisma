title: Glossary
authors: Ils Huygens, An Mertens
date: 2021-01-28
template: glossary

# Glossary

To create one set of algorithmic pathways within this publication a list of important words and their synonyms was made by the editors of ‘Alchorisma’. On this page that ‘list of lists’ is shown. The other set of algorithmic pathways is designed by an automatic search for similar 'bigrams' or similar sets of 2 words.