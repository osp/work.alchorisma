title: Participant biographies
authors: Z33 - House for Contemporary Art, Design & Architecture; Constant
date: 2021-01-29

# Biographies of the participants

**Anne Adé** studied history, got sidetracked along the way, and ended up (amongst others) as a researcher, sub editor, journalist and translator for a wide range of Belgian media outlets such as the public broadcaster VRT, newspapers and women’s magazines. When the open call for the Alchorisma-workshops was launched, she had just finished reading The Overstory, a powerful novel about trees and the destruction of forests, partly inspired by forest ecologist Suzanne Simard. How to inspire people to care more for the non-human entities on our planet (and beyond)? Can art be a gateway? Anne took part in Alchorisma as an outsider to find out, and is still trying to distill an answer to that question.

**Adva Zakai** was born in Israel and lives in Belgium. She combines her
background in dance and performance with the creation of participative,
educational and life practices. She is a part of the collectives Common
Wallet, School of Love and Co Post. She teaches, studies and tries to
never work alone and to never work too much.

**An Mertens** is a media artist, author, nature guide and spiritual
practitioner. She is a member of Constant, Association for Art and Media
based in Brussels. She develops a research-based art practice, focusing
on the use of computational techniques for literary creation. In 2019
she launched the artist name Anaïs Berck, a collective that stands for a
collaboration between humans, algorithms and trees and looks into the
specificities of human intelligence amidst artificial and vegetal
intelligences. She initiated and coordinates Algolit, a workgroup around
free code, text and literature.
<span class="personal-website"><a href="https://www.anaisberck.be/" target="_blank">www.anaisberck.be</a></span>

**Anne-Laure Buisson**, based in Brussels, Belgium, is a statistician /
datascientist who has worked with data since 2008. Fascinated by maps,
forests and the different ways memories populate places, she was happy
to contribute with a bit of Python code and some knowledge on walnut
trees.

**Annie Abrahams**, based in Montpellier, France, has an art practice
that meanders between research and performance. In her carefully
scripted art, she tends to reveal ordinary human behaviour and develops
what she calls an aesthetics of trust and attention. She is interested
in collaborative practices as a learning place for ‘being with’ and
considers behaviour as the aesthetic material of her art.
<span class="personal-website"><a href="https://bram.org/" target="_blank">bram.org</a></span>

**Arthur Gouillart** is a hybrid artist/designer/engineer based in
London. In 2018 he graduated from the Royal College of Art and Imperial
College in innovation design engineering. Working with interaction
technologies, he explores the intersection of meaningful emotions (Love,
Fear, Death) and complex systems (Technosystems, Ecosystems). He focuses
on making the unseen tangible through materialization and inverting
values hierarchies to open new perspectives. Research methodologies
include: Futurecasting, Speculative and Critical Design, Human-Centred
Design, Artistic research and Ethnographic user-research.
<span class="personal-website"><a href="https://www.arthurgouillart.com" target="_blank">www.arthurgouillart.com</a></span>

**Artyom Kolganov** is an independent theorist and writer based in
Moscow. He holds a BA in Philosophy from the Higher School of Economics
in Moscow and is a member of the AAA Research Group on Contemporary
Theory. His current research is dedicated to conceptual constructions of
the future under the conditions of algorithmic capitalism and control
society infrastructures.

**Axel Meunier** does design research, social practice, mapping and
workshop protocols. Currently a PhD candidate in design at Goldsmiths
College, he is an associate researcher at Sciences Po’s medialab and an
associate artist at the cultural co-operative CUESTA. His practice
consists mostly in creating live situations.
<span class="personal-website"><a href="https://axelmeunier.smvi.co" target="_blank">axelmeunier.smvi.co</a></span>

**Donatella Portoghese** graduated in foreign lLanguages and literature
with a master’s degree in cultural management with a focus on
museology. She has worked in various public and private cultural
institutions, ensuring artistic production, curatorship and financial
management. She has worked with artists from different fields, but with
a particular focus on media and audiovisual art. Since 2011 she has been
working with her colleagues on the management of Constant. She is
interested in collaborative processes of artistic production, the
creation of different and polyphonic narratives, the analysis and
dissection of cultural institutions such as libraries and museums, the
interaction between art and urban life.
<span class="personal-website"><a href="https://constantvzw.org/" target="_blank">constantvzw.org</a></span>

**Femke Snelting** develops research at the intersection of design,
feminisms and free software. In various constellations, she explores how
digital tools and practices might co-construct each other. Femke is a
member of Constant, Association for Art and Media based in Brussels and
collaborates as/in Possible Bodies, The Underground Division and The
Institute for Technology in The Public Interest.
<span class="personal-website"><a href="https://snelting.domainepublic.net/" target="_blank">snelting.domainepublic.net</a></span>

**Gaspard Bébié-Valérian** has been working, with his partner, as an
artist for over 15 years. Their joint practice focuses on social and
environmental issues such as energy, interspecies symbiosis and
cooperation for survival. They believe in the power of care to foster
more empathy and better understand our respective immediate ecosystems.
Through their collective work narratives, they entrust signals from
non-human entities to guide them down a more enlightened path towards
the future. For that purpose, their investigations include living
organisms as agents for change, invoking both a respectful and humble
position towards the complexity and ever-changing diversity of life.
<span class="personal-website"><a href="https://sgbv.net" target="_blank">sgbv.net</a></span>

**Ils Huygens** is curator of contemporary art and design at Z33 House
for Contemporary Art, Design & Architecture in Hasselt, where she has
curated exhibitions o the verge of art, science and society such as
Space Odyssey 2.0, The School of Time, Learning from Deep Time and solo
shows with Basim Magdy, Jasper Rigole and Kristof Vrancken
(forthcoming). She has co-curated two editions of Artefact Leuven: This
Rare Earth: Stories from Below and The Act of Magic. She is co-editor of
the book *Studio Time: Future Thinking in Art and Design*. Her recent
research has focused mostly on the politics of time and on exploring
multispecies interactions crossing art, nature and science.
<span class="personal-website"><a href="https://www.z33.be/" target="_blank">www.z33.be</a></span>

**Isabella Aurora** is a Brazilian artist currently living in Paris
whose body of work combines painting, performance and installation to
create a contemporary discussion re-imagining technology and alchemy as
augmented notions and instruments for transformation.
<span class="personal-website"><a href="http://isabellaaurora.fr" target="_blank">isabellaaurora.fr</a></span>

**Karin Ulmer** has worked for more than 20 years with Brussels-based
civil society organizations engaging in advocacy and lobbying towards EU
institutions on policies related to sustainable food systems,
agriculture and trade, land and seed rights. Karin is a member of the
photo collective tetebeche.eu. This article is written in her new
capacity as independent consultant and will be part of her forthcoming
photo-note-book in 2021 titled *\[homo situs\]*.
<span class="personal-website"><a href="https://www.tetebeche.eu/" target="_blank">www.tetebeche.eu</a></span>

**Maeva Borg** is currently a student at the Higher School of Art and
Design of Saint-Étienne, France. Her work focuses on the link between
machines and language, and how forms of ‘virtual spirits’ can emerge
from it. Her pratice involves programming and graphic design. She is
currently working on hybrid formats, such as a paper format and
additional digital ones. She also works on gender studies and archives
for her thesis, in order to find a way to make visible queer works in
the field of digital design.

**Malgorzata Zurada** is an interdisciplinary artist from Poland
currently based in Zurich (CH). She develops her works from esoteric
theories and rituals of past and present. The main areas of her research
are visual languages connected to heterodox belief systems and means of
coding the occult knowledge. She works mainly with site-specific
installations, digital image, text and sound.
<span class="personal-website"><a href="https://www.mzurada.com" target="_blank">www.mzurada.com</a></span>

**Mauro Ricchiuti** is a digital artist trained as an engineer with a
passion for technology and innovation. He loves the beauty of
mathematics and he tries to use it in creative ways in his works. Mauro
likes to use different technologies to create generative, interactive
and immersive experiences. He is currently a student of media
technology, doing his master’s thesis on computational creativity.
<span class="personal-website"><a href="https://www.behance.net/ricchiuti_4690" target="_blank">www.behance.net/ricchiuti_4690</a></span>

**Peter Westenberg** is a Brussels-based visual artist and cultural
instigator working in the field of community-based, technology-related
public art. His video work, walks, photography and collaborative
projects investigate urban iconography, vernacular architecture and
social routines. He prefers copyleft and free software. He is a core
member of Constant, Association for Art and Media, and is active as a
teacher and culture worker in the fields of art and design.
<span class="personal-website"><a href="https://www.videomagazijn.org" target="_blank">www.videomagazijn.org</a></span>

**Sumugan Sivanesan** is an anti-disciplinary artist, researcher and
writer based in Berlin. Often working collaboratively, his interests
span migrant histories and minority politics, activist media, artist
infrastructures and more-than-human rights. He is currently developing
‘fugitive radio’ to research anticolonial/antiracist media activism and
music in Northern Europe, in collaboration with Pixelache Helsinki. With
Tessa Zettel he co-founded the T. Rudzinskaitė Memorial Amateur
Lichenologists Society, an SF-storytelling and astrobiological social
movement that lives 86 years into the future.
<span class="personal-website"><a href="http://sivanesan.net" target="_blank">sivanesan.net</a></span>

**Tiina Prittinen** is a doctoral student at the University of Lapland
in Finland. Her research in the Faculty of Art and Design is based on
the interest in plant life, our response to undergoing ecological crisis
and visual presentations of pollution and key steps towards remediation.
She has a background in environmental activism and humanistic research
on art and environment. She includes the aesthetic experience in her
work, including the somatic response to our surroundings.

**Vesna Manojlovic** is a hacker, a mother, an expat from ex-Yugoslavia
living in Amsterdam since 1999; an intersectional feminist; a computer
science engineer turned teacher, artist, lecturer, organizer & community
builder. At RIPE NCC she takes part in Technical Internet Governance and
Networking Infrastructure fora and advocates for fundamental topics and
activities such as: mental health, diversity & inclusion, ethics &
fairness, empowerment, non-violent communication, sustainability,
ecological justice. She is also promoting Free/Libre & Open Source and
hackers ethics values & practices within activist and human-rights and
humanities communities.
<span class="personal-website"><a href="https://becha.home.xs4all.nl/" target="_blank">becha.home.xs4all.nl</a></span>

**Wendy Van Wynsberghe** is an artist, tinkerer, member of Floss Arts
Lab, Constant vzw, sound & field recorder, fauna adept, part of the
lively and chaotic Brussels realm, fascinated by protocol in all shapes
and sizes, avid free software user, pro open licences such as the Free
arts license for artwork, frankenscript coder, physical computing
aficionado, decentralization enthusiast, dabbling in embroidery, crochet
& knitting (with or without eTextiles).
<span class="personal-website"><a href="http://www.wvanw.space" target="_blank">www.wvanw.space</a></span>

**Elodie Mugrefya** is a member of Constant, Association for Art and
Media based in Brussels. She’s interested in the many things that form,
feed and codify what we call knowledge (mostly cultural) and its
transmission. She loves experimenting with writing as a generative
gesture to think about oppressive systems and our relationships to them.
<span class="personal-website"><a href="https://constantvzw.org/" target="_blank">constantvzw.org</a></span>

**Rares Craiut** is a Romanian Brussels-based artist-researcher
developing food performances centred on affect, collaboration and
factual information. His food performances are concerned with the
increasingly good knowledge we have about what is good for human and
environmental health and, at the same time, the huge gap between our
knowledge and policy, eating habits and urban food culture, precisely
because these are tightly constrained by the interests of giant
corporations.
<span class="personal-website"><a href="https://www.performingfood.com" target="_blank">www.performingfood.com</a></span>

**RYBN.ORG** is an artist collective created in 1999 and based in Paris.
RYBN.ORG leads extra-disciplinary investigations within the realms of
high-frequency economics and information technologies, writing
kabbalistic algorithms, inserting suicidal trading machines into the
financial markets, perverting neural networks during their training
phase or hunting ghosts in the noise of data traffic.
<span class="personal-website"><a href="http://rybn.org/" target="_blank">rybn.org</a></span>

**FoAM** (Maja Kuzmanovic, Nik Gaffney) is a transdisciplinary network
which entangles art, science, nature and everyday life. Maja Kuzmanovic
and Nik Gaffney operate as a nomadic cell within the network to explore
futurecrafting as a way of re-enchanting the present. They cultivate
circumstances for conviviality and collaboration, resonant with animist
approaches to time, attunement and interdependence. Their works and ways
of working aim to stimulate collective imagination and form solidarity
through speculative propositions, immersive experiences and
contemplative practices suitable for techno-materialist cultures.
<span class="personal-website"><a href="https://fo.am/" target="_blank">fo.am</a></span>

**Erik De Wilde, Ingrid&nbsp;Baisier, Karin&nbsp;Van&nbsp;Ginneken, Linda&nbsp;Verplancke,
Marleen&nbsp;Verschueren, Martina&nbsp;Gylsen and Nele&nbsp;De&nbsp;Man** are a group of
local shamans who organized a shamanic evening as the opening of the
worksession in Z33 in Hasselt. As a group they followed the shamanic
training and were initiated by Frank Coppieters and Kathy Melcher.

**Z33** is a groundbreaking arts institution in Hasselt that works at
the intersection of art, design and architecture. It is a safe haven on
the periphery with a magnificent new building and a refreshingly diverse
programme. It is a rugged playground for artistic research, with its
sights set on the world, its feet in the mud, and a heart that beats for
talent development, (un)learning, the enjoyment of research and
scintillating collaborations.
<span class="personal-website"><a href="https://www.z33.be/" target="_blank">www.z33.be</a></span>

**Constant** is a non-profit, artist-run organization based in Brussels
since 1997 and active in the fields of art, media and technology.
Constant develops, investigates and experiments. Constant starts out
from feminisms, copyleft, Free/Libre + Open Source Software. Constant
loves collective digital artistic practices. Constant organizes
transdisciplinary worksessions. Constant creates installations,
publications and exchanges. Constant collaborates with artists,
activists, programmers, academics, designers. Constant is active
archives, poetic algorithms, body and software, books with an attitude,
counter cartographies, extitutional networks, performative protocols,
etc.
<span class="personal-website"><a href="https://constantvzw.org/" target="_blank">constantvzw.org</a></span>


<style>
  strong{
    font-weight: normal;
    background: whitesmoke;
    color: black;
    padding: 0.2em 0.4em;
    border-radius: 0.5em 0;
    /* height: calc(var(--em-space) * 2); */
    display: inline;
  }
  span.personal-website{
    display: block;
  }
  span.personal-website::before{
    content: "→ ";
  }
</style>
