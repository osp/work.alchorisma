title: About Alchorisma
authors: Z33 - House for Contemporary Art, Design & Architecture; Constant
date: 2021-01-31

# About Alchorisma

<section class="side-to-side" markdown="1">
![people on computer at desks]({attach}worksessions-gallery-media/PWFU8418.jpeg){.image-process-illustration--inline}
![two tied trees supporting each others]({attach}about-alchorisma-media/DSC03518.JPG){.image-process-illustration--inline}
</section>

## Context

Alchorisma is interested in the possibilities of infecting,
contaminating, transforming the algorithmic by other
non-exclusively-human visions of the world. Alchorisma proposes to be a
common ground from which algorithms can start to overcome the strictly
optimizing concepts.

Algorithms have made their way into contemporary society through
mathematics and computer science, as sets of rules, behaviours or
methods underlying software operations. They have become closely
associated with a problem-solving engineering mindset, and more recently
with market-driven optimisation, proprietary technologies and morally
troubling decision making. However, algorithms extend beyond the
man-made world. They are inherent in all matter. Organic and inorganic,
microscopic and macroscopic. They can describe generative processes of
living things, from the branching of trees and division of cells, to the
flows of crowds and fluids.

We rarely see algorithms themselves, as they are embodied in source
code, laws of physics or DNA. Yet we're surrounded by their effects,
including the appearance of grown and built structures, the development
of individuals and organisations, or algorithmically customised
newsfeeds. Tacit and unseen, algorithms often guide our actions. They
influence the processes and behaviours of human societies, economies and
legal systems. The influence flows in both directions; while algorithms
influence us, we are capable of influencing algorithms too. We can exert
our agency and change their behaviours and directions. Changing the
algorithms of a legal code, for example, can allow rivers, forests and
mountains to gain legal rights to be protected from human exploitation
(e.g. the recognition of the Whanganui river as a 'person' in New
Zealand or Rights of Nature in Ecuador).

## Premisses

We began approaching non-anthropocentric algorithms by acquainting
ourselves with the perspectives of trees, stones and spirits, in the
site-specific context of the former beguinage of Z33 and beyond. Each of
these entities functioned as an entry-point into a panpsychic worldview:
a proposition that all matter possesses an elementary aliveness to which
we can relate. Alchorisma aimed to become a collective process of
collaboration with the workshop participants, trees, stones and spirits,
as envoys or archetypes pointing to changing relationships between
humans and the rest of the world.

<section class="side-to-side" markdown="1">
![dividing branches of a grafted tree]({attach}about-alchorisma-media/DSC03528.JPG){.image-process-illustration--inline}
![people dancing]({attach}worksessions-gallery-media/PWFU8869.jpeg){.image-process-illustration--inline}
</section>

We asked ourselves what patterns, behaviours and transformations we
can discover in dialogue with stones, trees and spirits? Which
algorithms underlie their properties, processes and needs? How can
we translate these algorithms into human-readable forms? In what way can
these algorithmic models become (dis)functional? What can they teach us
about life-affirming relationships with not-only-human entities?

After the exploration of species-specific algorithms during the
first days, our intention was to collectively compose an algorithmic
structure in which these beyond-human models can be combined into a
generative ecosystem of sorts; an interconnecting tissue or algorithmic
social space. A place where we could collaborate with spirits, stones,
trees or any other entities that would join the collective process
throughout the week. During the worksession we stretched our imagination
to jointly develop prototypes, models and concepts of alchorisms --
charismatic algorithms. All documentation of the first worksession
in Hasselt can be found here:
[https://constantvzw.org/wefts/alchorisma.en.html](https://constantvzw.org/wefts/alchorisma.en.html).

## Credits

Alchorisma was developed in December 2018 by
[Constant](https://constantvzw.org/) in collaboration
with [FoAM](https://fo.am/),
[RYNB.ORG](http://www.rybn.org/) and [Z33, House
for Contemporary Art, Design and Architecture](https://www.z33.be/) in Hasselt. In August 2020 a second
worksession took place in the beautiful family house of Anne-Laure
Buisson, one of the participants, in Beaulieu, Vercors, France.

With the support of: Vlaamse Overheid, Vlaamse Gemeenschapscommissie, Fédération Wallonie-Bruxelles Arts Numériques, City of Hasselt, City of Genk, Domein Bokrijk & Agentschap voor Natuur & Bos.

With the participation of: Adva Zakai, An Mertens, Anne Adé,
Anne-Laure Buisson, Annie Abrahams, Arthur Gouillart, Artyom Kolganov,
Axel Meunier, Donatella Portoghese, Erik De Wilde, Femke Snelting,
Gaspard Bébié-Valérian, Ils Huygens, Ingrid Baisier, Isabella Aurora,
Jack Boyer, Karin Ulmer, Karin Van Ginneken, Kevin Bartoli, Linda
Verplancke, Maeva Borg, Maja Kuzmanovic, Malgorzata Zurada, Marika
Dermineur, Marleen Verschueren, Martina Gylsen, Mauro Ricchiuti, Nele De
Man, Nik Gaffney, Peter Westenberg, Roberto Simone, Sumugan Sivanesan,
Tiina Prittinen, Vesna Manojlovic, Wendy Van Wynsberghe, Elodie
Mugrefya, Rares Craiut.

A big thank you to: Michel and Marie-Andrée Buisson,
Toon Van Daele, Jef Van Meulder, Hans Nickmans, Kathy Melcher, Frank
Coppieters, Michèle Meesen.

Publication design: [Open Source Publishing](http://osp.kitchen/)

Proofreading texts: Patrick Lennon

![a father and a rock]({attach}about-alchorisma-media/DSC03583.JPG){.image-process-illustration--inline}
