title: Alchorisma Reader
authors: Z33 - House for Contemporary Art, Design & Architecture; Constant
date: 2021-01-27

# Alchorisma Reader

This reader was compiled in preparation of the first worksession by the
Alchorisma participants. It acts as a guide to further references,
books, quotes and articles by the many authors and thinkers that have
accompanied us throughout the Alchorisma research. The reader is divided
into different chapters: General, Technology, Spirits, Stones and Trees.

<div class="iframe-wrapper">
  <iframe src="{attach}pdf/reader_all.pdf"></iframe>
  <a class="vines-button" href="{attach}pdf/reader_all.pdf" target="_blank" data-role="fullscreen">reader</a>
</div>
